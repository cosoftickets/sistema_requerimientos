var Adjuntos={
    guardarNuevoAdjunto: function (form, btn) {
        var formulario = $(form).serialize();
        alert(formulario);
        btn.disabled = true;
        var error = false;
        var msg_error = '';
        if (error) {
            
        } else {
            var id_ticket=form.ticket.value
            $.post(BASE_URI + 'index.php/Solicitudes/guardarNuevoAdjunto', {data: formulario}, function (response){
                $.ajax({
                      url: BASE_URI+'index.php/DetalleSolicitud/listarAdjuntos/'+id_ticket,
                      context: document.body
                      }).done(function(data) {
                        $("#adjuntos").html(data);                                      
                    });

            }, 'json').fail(function () {
                xModal.danger("Error en sistema. Intente nuevamente",function(){
                    btn.disabled = false;
                });
            });
        }
    },

    eliminarAdjunto: function (iden) {
        var id_ticket=$('#ticket').val()
        var archivo=iden
        var param={
            'archivo':archivo,
        }
        $.ajax({
            url:BASE_URI+'index.php/DetalleSolicitud/eliminarAdjunto/'+archivo,
            type:'POST',
            data:param,
            beforeSend:function(){},
            success:function(obj){
                alertify.success("Adjunto eliminado exitosamente")
                 $.ajax({
                      url: BASE_URI+'index.php/DetalleSolicitud/listarAdjuntos/'+id_ticket,
                      context: document.body
                    }).done(function(data) {
                        $("#adjuntos").html(data);                                      
                    });
            }
        })
    }
}