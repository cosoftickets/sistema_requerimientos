var Solicitudes = {

    guardarNuevaSolicitud: function (form, btn) {
        /*
         realizar validacion formulario
         */
        btn.disabled = true;

        var error = false;
        var msg_error = '';
        if (form.nombre.value == "") {
            msg_error += '- Debe Ingresar el Asunto<br/>';
            error = true;
        }
        if (form.fc_fecha_entrega.value == "") {
            msg_error += '- Debe Seleccioner una Fecha límite<br/>';
            error = true;
        }
        if (form.id_prioridad.value == 0) {
            msg_error += '- Debe Seleccionar una Prioridad<br/>';
            error = true;
        }
        if (form.cd_id_usuario.value == 0) {
            msg_error += '- Debe Asignar un Responsable';
            error = true;
        }


        if (error) {
            xModal.danger(msg_error,function(){
                btn.disabled = false;
            });

        } else {
            var formulario = $(form).serialize();
            //alert(formulario);
            $.post(BASE_URI + 'index.php/Solicitudes/guardarNuevaSolicitud', {data: formulario}, function (response) {
                //alert(response);
                if (response.estado == true) {
                    xModal.success(response.mensaje,function(){
                        location.href = BASE_URI + 'index.php/Solicitudes/Nuevo';
                    });

                } else {
                    xModal.danger(response.mensaje,function(){
                        btn.disabled = false;
                    });

                }
            }, 'json').fail(function () {
                xModal.danger("Error en sistema. Intente nuevamente",function(){
                    btn.disabled = false;
                });

            });
        }

    },

    

    /*ActualizarDetalleFinalizado: function (template, visador_true, id, perfil) {
        var idTicket = id;
        var perfilUsr = perfil;
        if(perfilUsr ==1){
            var comentario = $ ('#gl_detalle_finalizado').val();
        }else{
            var comentario = 1;
        }
        $.post(BASE_URI + 'index.php/Solicitudes/ActualizarDetalleFinalizado', {idTicket: idTicket, comentario: comentario}, function (response) {
        }, 'html');
    },

    ActualizarHoras: function (template, visador_true, id, perfil) {
        var idTicket = id;
        var perfilUsr = perfil;
        if(perfilUsr ==1){
            var estimadas = $ ('#horas_estimadas').val();
            var utilizadas = $ ('#horas_utilizadas').val();
        }else{
            var comentario = 1;
        }
        $.post(BASE_URI + 'index.php/Solicitudes/ActualizarHoras', {idTicket: idTicket, estimadas: estimadas,utilizadas:utilizadas}, function (response) {
        }, 'html');
    },*/

    finalizar: function (solicitud, btn) {
        btn.disabled = true;
        $.post(BASE_URI + 'index.php/Solicitudes/finalizarTicket', {solicitud: solicitud}, function (response) {
            if (response.estado == true) {
                xModal.success(response.mensaje,function(){
                    Solicitudes.cargarGrillaAsignados();
                    xModal.closeAll();
                });

            } else {
                xModal.danger(response.mensaje,function(){
                    btn.disabled = false;
                });

            }
        }, 'json').fail(function () {
            xModal.danger("Error en sistema. Intente nuevamente",function(){
                btn.disabled = false;
            });
        });
    },

    iniciar: function (solicitud, btn) {
        btn.disabled = true;
        $.post(BASE_URI + 'index.php/Solicitudes/iniciarTicket', {solicitud: solicitud}, function (response) {
            if (response.estado == true) {
                xModal.success(response.mensaje,function(){
                    Solicitudes.cargarGrillaAsignados();
                    xModal.closeAll();
                });

            } else {
                xModal.danger(response.mensaje,function(){
                    btn.disabled = false;
                });

            }
        }, 'json').fail(function () {
            xModal.danger("Error en sistema. Intente nuevamente",function(){
                btn.disabled = false;
            });
        });
    },

    barraProcessCheck: function (form, check, id, cantidad_tareas){
        var idProyecto = $("input[id='id_solicitud']").val();
        var id_tarea = id;
        var count = 0;
        var cantidadTareas = cantidad_tareas;
        var porcentaje = 0;
        $.post(BASE_URI + 'index.php/Solicitudes/barraProcessCheck',{id_tarea: id_tarea, idProyecto: idProyecto }, function (response) {

            $("#barraProcesCheck").html(response);
            total = $("input[id='check']:checked").length;

            if(cantidadTareas){
                var totalMarcado = total;
                porcentaje = (totalMarcado *100) / cantidadTareas;
                if(porcentaje == 100){
                    $("#btn_finalizado").prop( "disabled", false );
                }else{
                    $("#btn_finalizado").prop( "disabled", true );
                }
            }else{
                porcentaje = 0;
            }

            $("#barra_progreso").animate({width: porcentaje+"%"}, {queue: false,duration: 1500});
            $("#porcentaje").html(Math.round(porcentaje)+"%");
            //$("#barra_progreso").width(porcentaje+"%")
        }, 'html');
         
    },


    cargarGrillaTareas: function (template, visador_true) {
        var idProyecto = $("input[id='id_solicitud']").val();
        var cantidad   = $("input[id='cantidad_tareas']").val();
        $.post(BASE_URI + 'index.php/Solicitudes/cargarGrillaTareas', {idProyecto: idProyecto, cantidad: cantidad}, function (response) {
            if (template === undefined) {
                $("#lista_tareas").html(response);
            } else {
                $("iframe").contents().find("div#lista_tareas").html(response);
            }
        }, 'html');
    },
    
    cargarDatosRegularizacion: function (template, visador_true) {
        
        $.post(BASE_URI + 'index.php/Solicitudes/cargarDatosRegularizacion', {}, function (response) {
            if (template === undefined) {
                $("#lista_datos").html(response);
                $('#btn_regularizar').trigger('click');
            } else {
                $("iframe").contents().find("div#lista_datos").html(response);
                $('#btn_regularizar').trigger('click');
            }
        }, 'html');
    },

    recargarTablaReportes: function (template, visador_true){
        var filtroFecha = $( "#filtroFecha option:selected" ).val();
        var mes = meses(filtroFecha);
        $.post(BASE_URI + 'index.php/Reportes/recargarTablaReportes', {filtroFecha: filtroFecha}, function (response) {
        $("#tablaReportes").html(response);
        $("#tablaPrincipal").dataTable({
                "pageLength": 10,
                "aaSorting": [],
                "language": {
                    "url": url_base + "static/js/plugins/DataTables-1.10.5/lang/es.json"
                },
                "fnDrawCallback": function (oSettings) {
                    $(this).fadeIn("slow");
                },
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Exportar a Excel',
                        filename: 'Reporte COSOF - '+mes,
                        exportOptions: {
                            modifier: {
                                page: 'all'
                            }
                        }
                    }
                ]
            });
        }, 'html');
    },

    meses: function($filtro){
        var enero   = "Enero"
        var febrero = "Febrero"
        var marzo   = "Marzo"
        var abril   = "Abril"
        var mayo    = "Mayo";
        var junio   = "Junio";
        var julio   = "Julio";
        var agosto  = "Agosto";
        var septiembre  = "Septiembre";
        var octubre = "Octubre";
        var noviembre  = "Noviembre";
        var diciembre   = "Diciembre";
        if($filtro == 01){
            return enero
        }
        if($filtro == 02){
            return febrero
        }
        if($filtro == 03){
            return marzo
        }
        if($filtro == 04){
            return abril
        }
        if($filtro == 05){
            return mayo
        }
        if($filtro == 06){
            return junio
        }
        if($filtro == 07){
            return julio
        }
        if($filtro == 08){
            return agosto
        }
        if($filtro == 09){
            return septiembre
        }
        if($filtro == 10){
            return octubre
        }
        if($filtro == 11){
            return noviembre
        }
        if($filtro == 12){
            return diciembre
        }

    },

    ActualizarCamposSolicitudes: function (template, visador_true, id, perfil) {
            var idTicket = id;
            var fechaEntrega = $("input[id='fecha_entrega']").val();
            var comentario = $('#gl_comentario').val();
            var detalle    = $('#gl_detalle_finalizado').val();
            var horasEstimadas  = $('#horas_estimadas').val();
            var horasUtilizadas = $('#horas_utilizadas').val();
            var fechaFinalizar  = $("#fecha_termino").val();
            if(fechaFinalizar == ""){
                fechaFinalizar = "00/00/0000";
            }

           

        $.post(BASE_URI + 'index.php/Solicitudes/ActualizarCamposSolicitudes', {idTicket: idTicket, fechaEntrega: fechaEntrega, fechaFinalizar: fechaFinalizar, comentario: comentario, detalle: detalle, horasEstimadas: horasEstimadas, horasUtilizadas: horasUtilizadas}, function (response) {
            /* $('#fecha_entrega').on('changeDate', function(ev){
                $(this).datepicker('hide');
            });*/

            if(response.estado == true){
                 
                 alertify.success("Campo modificado exitosamente");
            }else{
                 //alertify.success("Adjunto eliminado exitosamente")
            }

        }, 'json');

    },


    CargarGrafico: function (id) {
        var idUser = id;
        $.post(BASE_URI + 'index.php/Estadisticas/CargarGrafico', {idUser: id}, function (response) {
             
             $("#contenedor_grafico").html(response);

        }, 'html');

    },

    cargarCentrosResponsabilidad: function (subsecretaria, combo) {
        $.post(BASE_URI + 'index.php/Documento/centrosResponsabilidad', {subsecretaria: subsecretaria}, function (response) {
            var total = response.length;
            var options = '';
            for (var i = 0; i < total; i++) {
                options += '<option value="' + response[i].codigo + '">' + response[i].nombre + '</option>';
            }
            $("#" + combo).html(options).trigger('change');
        }, 'json');
    },


    initAutocompleteRutEmisor: function () {
        $("input.typeahead").typeahead({
            onSelect: function (item) {
                $("#nombre_emisor").val(item.value);
            },
            ajax: {
                url: BASE_URI + 'index.php/Documento/getEmisores',
                timeout: 250,
                displayField: "rut",
                valueField: 'nombre',
                triggerLength: 1,
                method: "post",
                loadingClass: "loading-circle",
                preDispatch: function (query) {
                    //showLoadingMask(true);
                    return {
                        search: query
                    }
                },
                preProcess: function (data) {
                    //showLoadingMask(false);
                    if (data.success === false) {
                        // Hide the list, there was some error
                        $("#nombre_emisor").val('');
                        return false;
                    }
                    // We good!
                    return data.listado;
                }
            }
        });
    },


    cargarGrillaAsignados: function () {
        $.post(BASE_URI + 'index.php/Solicitudes/grillaAsignados', function (response) {
            $("#contenedor-grilla-asignados").html(response);
            $("#tablaPrincipal").dataTable({
                "pageLength": 10,
                "aaSorting": [],
                "language": {
                    "url": url_base + "static/js/plugins/DataTables-1.10.5/lang/es.json"
                },
                "fnDrawCallback": function (oSettings) {
                    $(this).fadeIn("slow");
                },
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Exportar a Excel',
                        filename: 'Grilla',
                        exportOptions: {
                            modifier: {
                                page: 'all'
                            }
                        }
                    }
                ]
            });
        }, 'html');
    },


    cargarGrillaRevision: function () {
        $.post(BASE_URI + 'index.php/Documento/grillaRevision', function (response) {
            $("#contenedor-grilla-revision").html(response);
            $("#tablaPrincipal").dataTable({
                "pageLength": 10,
                "aaSorting": [],
                "language": {
                    "url": url_base + "static/js/plugins/DataTables-1.10.5/lang/es.json"
                },
                "fnDrawCallback": function (oSettings) {
                    $(this).fadeIn("slow");
                },
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Exportar a Excel',
                        filename: 'Grilla',
                        exportOptions: {
                            modifier: {
                                page: 'all'
                            }
                        }
                    }
                ]
            });
        }, 'html');
    },


    cargarVisadores: function (cr, combo) {
        $.post(BASE_URI + 'index.php/Documento/cargarVisadores', {centro: cr}, function (response) {
            var total = response.length;
            var options = '';
            var selected = '';
            if (total == 1) {
                selected = ' selected ';
            }
            for (var i = 0; i < total; i++) {
                options += '<option value="' + response[i].id + '" ' + selected + '>' + response[i].nombre + '</option>';
            }
            $("#" + combo).html(options);
        }, 'json');
    },


    validarSoloNumeros: function (input) {
        var num = input.value.replace(/\./g, '');
        if (!isNaN(num)) {
            num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
            num = num.split('').reverse().join('').replace(/^[\.]/, '');
            input.value = num;
            $("#mensaje_monto").html('');
        }

        else {
            $("#mensaje_monto").html('Sólo se permiten números');
            input.value = input.value.replace(/[^\d\.]*/g, '');
        }
    },


    subirArchivo: function (form) {
        $(form).submit(function () {
            $.post(BASE_URI + 'index.php/Solicitudes/cargarGrillaAdjuntos', function (response) {
                $("#lista_adjuntos").html(response);
            }, 'html');
        });
    },

    subirDatos: function (form) {
        $(form).submit(function () {
            $.post(BASE_URI + 'index.php/Solicitudes/cargarGrillaAdjuntos', function (response) {
                $("#lista_adjuntos").html(response);
            }, 'html');
        });
    },


    cargarGrillaAdjuntos: function (template, visador_true) {
        var visador = 0;
        if (visador_true !== undefined) {
            visador = visador_true;
        }
        $.post(BASE_URI + 'index.php/Solicitudes/cargarGrillaAdjuntos', {visador: visador}, function (response) {
            if (template === undefined) {
                $("#lista_adjuntos").html(response);
            } else {
                $("iframe").contents().find("div#lista_adjuntos").html(response);

            }

        }, 'html');
    },

    /*cargarAdjuntos: function (template, visador_true) {
        var visador = 0;
        if (visador_true !== undefined) {
            visador = visador_true;
        }
        $.post(BASE_URI + 'index.php/DetalleSolicitud/detalleSolicitud', {visador: visador}, function (response) {
            if (template === undefined) {
                $("#lista_adjuntos").html(response);
            } else {
                $("iframe").contents().find("div#lista_adjuntos").html(response);

            }

        }, 'html');
    },*/

    borrarAdjunto: function (indice, template) {
        var visador = 0;
        if (template !== undefined) {
            visador = 1;
        }
        $.post(BASE_URI + 'index.php/Solicitudes/borrarAdjunto', {indice: indice, visador: visador}, function (response) {
            if (template === undefined) {
                $("#lista_adjuntos").html(response);
            } else {
                $("iframe").contents().find("div#lista_adjuntos").html(response);

            }
        }, 'html');
    },

    borrarTarea: function (indice, id_proyecto, template) {
        var idSolicitud = id_proyecto;
        $.post(BASE_URI + 'index.php/Solicitudes/borrarTarea', {indice: indice, idSolicitud: idSolicitud}, function (response) {
            if (template === undefined) {
                $("#lista_tareas").html(response);
            } else {
                $("iframe").contents().find("div#lista_tareas").html(response);

            }
        }, 'html');
    },

    borrarTareaBySession: function (indice, template) {
        alert(indice);
        $.post(BASE_URI + 'index.php/Solicitudes/borrarTareaBySession', {indice: indice}, function (response) {
            if (template === undefined) {
                $("#lista_tareas").html(response);
            } else {
                $("iframe").contents().find("div#lista_tareas").html(response);

            }
        }, 'html');
    },



    mostrarFolioSigfe: function () {
        $("#contenedor-folio-sigfe").show();
    },


    devengar: function (form, solicitud) {
        if (form.numero_folio.value == "" || isNaN(form.numero_folio.value)) {
            xModal.danger('Debe ingresar el número de folio correspondiente');
        } else {
            $.post(BASE_URI + 'index.php/Documento/devengarSolicitud', {
                numero: form.numero_folio.value,
                solicitud: solicitud
            }, function (response) {
                if (response.estado == true) {
                    xModal.success(response.mensaje,function(){
                        Documento.cargarGrillaRevision();
                        xModal.closeAll();
                    });

                }else{
                    xModal.danger(response.mensaje);
                }
            }, 'json').fail(function () {
                xModal.danger('Error en Sistema. Intente nuevamente');
            });
        }
    },


    devolverProveedor: function (solicitud) {
        $.post(BASE_URI + 'index.php/Documento/devolverProveedor', {solicitud: solicitud}, function (response) {
            if (response.estado == true) {
                xModal.success(response.mensaje,function(){
                    Documento.cargarGrillaRevision();
                    xModal.closeAll();
                });

            }else{
                xModal.danger(response.mensaje);
            }
        }, 'json').fail(function () {
            xModal.danger('Error en Sistema. Intente nuevamente');
        });
    },


    filtrarDocumentos: function (form) {
        var dias = form.numero_dias.value;
        if (dias === undefined || dias == "") {
            xModal.danger('Debe ingresar un valor para realizar el filtro');
        } else {
            var condicion = form.condicion.value;
            $.post(BASE_URI + 'index.php/Documento/filtrarDocumentos', {
                dias: dias,
                condicion: condicion
            }, function (response) {
                $("#contenedor-grilla-asignados").html(response);
                $("#tablaPrincipal").dataTable({
                    "pageLength": 10,
                    "aaSorting": [],
                    "language": {
                        "url": url_base + "static/js/plugins/DataTables-1.10.5/lang/es.json"
                    },
                    "fnDrawCallback": function (oSettings) {
                        $(this).fadeIn("slow");
                    },
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            text: 'Exportar a Excel',
                            filename: 'Grilla',
                            exportOptions: {
                                modifier: {
                                    page: 'all'
                                }
                            }
                        }
                    ]
                });
            }, 'html');
        }
    },


    cambiarVisador : function(visador,doc){
        $.post(BASE_URI + 'index.php/Documento/cambiarVisador',{visador:visador,doc:doc},function(response){
            if(response.estado == true){
                Documento.cargarGrillaTodos();
                xModal.closeAll();
            }else{
                xModal.danger(response.mensaje);
            }
        },'json').fail(function(){
            xModal.danger('Error en sistema. Intente nuevamente');
        });
    },


    cargarGrillaTodos: function () {
        $.post(BASE_URI + 'index.php/Documento/grillaTodos', function (response) {
            $("#contenedor-grilla-asignados").html(response);
            $("#tablaPrincipal").dataTable({
                "pageLength": 10,
                "aaSorting": [],
                "language": {
                    "url": url_base + "static/js/plugins/DataTables-1.10.5/lang/es.json"
                },
                "fnDrawCallback": function (oSettings) {
                    $(this).fadeIn("slow");
                },
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Exportar a Excel',
                        filename: 'Grilla',
                        exportOptions: {
                            modifier: {
                                page: 'all'
                            }
                        }
                    }
                ]
            });
        }, 'html');
    },



// BC


    guardarNuevaBoleta: function (form, btn) {
        /*
         realizar validacion formulario
         */
        btn.disabled = true;

        var error = false;
        var msg_error = '';
        if (form.subsecretaria_boleta.value == 0) {
            msg_error += '- Debe seleccionar Subsecretaría<br/>';
            error = true;
        }
        if (form.tipo_boleta.value == 0) {
            msg_error += '- Debe seleccionar Tipo de boleta<br/>';
            error = true;
        }
        if (form.numero_boleta.value == "") {
            msg_error += '- Debe ingresar Número de la boleta<br/>';
            error = true;
        }
        if (form.rut_emisor.value == "") {
            msg_error += '- Debe ingresar Rut Emisor\n';
            error = true;
        }
        
        if (form.fecha_oficina_boleta.value == "") {
            msg_error += '- Debe seleccionar Fecha Ingreso Oficina de Partes<br/>';
            error = true;
        }
        if (form.nombre_emisor.value == "") {
            msg_error += '- Debe ingresar Nombre de Emisor<br/>';
            error = true;
        }
       

        if (error) {
            xModal.danger(msg_error,function(){
                btn.disabled = false;
            });

        } else {
            
            //return 0
            var formulario = $(form).serialize();
            //alert(BASE_URI);
            $.post(BASE_URI + 'index.php/Documento/guardarNuevaBoleta', {data: formulario}, function (response) {
                form.guardarBoleta.disabled=false
                //document.getElementById("divDetalle").style.display = "block";
                xModal.success("Boleta guardada correctamente",function(){
                    btn.disabled = false;
                    //xModal.open('index.php/Documento/detalleBoleta','Detalle de boleta',50,'adjuntar',true,280);
                });
                form.rut_emisor_boleta.disabled=true
                form.subsecretaria_boleta.disabled=true
                form.tipo_boleta.disabled=true
                form.numero_boleta.disabled=true
                //xModal.open('index.php/Documento/detalleBoleta','Detalle de boleta',50,'adjuntar',true,280);

                /*if (response.estado == true) {
                    xModal.success(response.mensaje,function(){
                        //location.href = BASE_URI + 'index.php/Documento/boletas';
                    });

                } else {
                    xModal.danger(response.mensaje,function(){
                        btn.disabled = false;
                    });

                }*/
            }, 'json').fail(function () {
                xModal.danger("Error en sistema. Intente nuevamente",function(){
                    btn.disabled = false;
                   //alert("hasta aca")
                });

            });
        }

    },

    guardarDetalle:function  (form, btn) {
        /*
         realizar validacion formulario
         */
       
        btn.disabled = true;

        var error = false;
        var msg_error = '';
        if (form.codigo.value == 0) {
            msg_error += '- Debe ingresar código del producto<br/>';
            error = true;
        }
        if (form.glosa.value == 0) {
            msg_error += '- Debe ingresar la glosa<br/>';
            error = true;
        }
        if (form.cantidad.value == "") {
            msg_error += '- Debe ingresar la cantidad<br/>';
            error = true;
        }
        if (form.precio.value == "") {
            msg_error += '- Debe ingresar el precio\n';
            error = true;
        }
        //alert(BASE_URI)
        //return 0
        if (error) {
            xModal.danger(msg_error,function(){
                btn.disabled = false;
            });
        } else {
            
            //return 0
            var formulario = $(form).serialize();
            //alert(BASE_URI);
            $.post(BASE_URI + 'index.php/Documento/guardarDetalleBoleta', {data: formulario}, function (response) {
                //form.guardarBoleta.disabled=false
                //document.getElementById("divDetalle").style.display = "block";
                xModal.success("Detalle guardado correctamente",function(){
                    btn.disabled = false;
                });
                /*form.rut_emisor_boleta.disabled=true
                form.subsecretaria_boleta.disabled=true
                form.tipo_boleta.disabled=true
                form.numero_boleta.disabled=true*/

                /*if (response.estado == true) {
                    xModal.success(response.mensaje,function(){
                        //location.href = BASE_URI + 'index.php/Documento/boletas';
                    });

                } else {
                    xModal.danger(response.mensaje,function(){
                        btn.disabled = false;
                    });

                }*/
            }, 'json').fail(function () {
                xModal.danger("Error en sistema. Intente nuevamente",function(){
                    btn.disabled = false;
                   //alert("hasta aca")
                });

            });
        }
    }

   
}