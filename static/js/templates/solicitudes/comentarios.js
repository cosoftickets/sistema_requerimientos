var Comentario = {
    guardarComentario: function (form, btn) {
        /*
         realizar validacion formulario
         */
        btn.disabled = true;

        var error = false;
        var msg_error = '';
        
        if (error) {
            xModal.danger(msg_error,function(){
                btn.disabled = false;
            });

        } else {
            var id_ticket=form.ticket.value
            var formulario = $(form).serialize();
            //alert(formulario);
            $.post(BASE_URI + 'index.php/Solicitudes/guardarNuevoComentario', {data: formulario}, function (response) {
                //alert(response);
                if (response.estado == true) {
                    Comentario.deshabilitar();
                    btn.disabled = false;
                    alertify.success("Comentario guardado correctamente")
                    $.ajax({
                      url: BASE_URI+'index.php/DetalleSolicitud/listarComentarios/'+id_ticket,
                      context: document.body
                    }).done(function(data) {
                        $("#comments").html(data);                                      
                    });

                } else {
                    xModal.danger(response.mensaje,function(){
                        btn.disabled = false;
                    });

                }
            }, 'json').fail(function () {
                xModal.danger("Error en sistema. Intente nuevamente",function(){
                    btn.disabled = false;
                });

            });
        }

    },
    deshabilitar(){
       $('#seccionComentario').fadeOut()
       $('#nuevo_comentario').val('') 
    },

     cargarGrillaComentarios: function (template, visador_true) {
        var visador = 0;
        if (visador_true !== undefined) {
            visador = visador_true;
        }
        $.post(BASE_URI + 'index.php/Solicitudes/cargarGrillaComentarios', {visador: visador}, function (response) {
            if (template === undefined) {
                $("#comments").html(response);
            } else {
                $("iframe").contents().find("div#comments").html(response);

            }

        }, 'html');
    },
}