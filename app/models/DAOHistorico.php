<?php

class DAOHistorico extends Model {

    protected $_tabla = "historico_tickets";

    function __construct()
    {
        parent::__construct();
    }


   public function queryBusquedaHistorico(){
      $query="select t.cd_id_usuario,(select(concat(nombres,' ',apellidos))from usuario where id=t.cd_id_usuario)as asignado,ht.cd_id_usuario,cd_id_tarea,id_ticket,id_historial,nombre,e.gl_descripcion as estado,p.gl_descripcion as prioridad,gl_comentario,gl_evento_historial,gl_nombre_tarea,
        concat(nombres,' ',apellidos)as usuario,date_format(fc_fecha_movimiento,'%d-%m-%Y %H:%i:%s') as fecha,
        gl_descripcion_tarea,tr.nr_estado
        from historico_tickets  ht
        left join tipo_estado e on id_estado=cd_id_estado
        left join usuario  on id=cd_id_usuario 
        left join tickets t on id_ticket=cd_id_solicitud
        left join prioridad p on p.id=cd_id_prioridad 
        left join tareas tr on id_tarea=cd_id_tarea";
        $consulta = $this->db->getQuery($query,array());
          if ($consulta->numRows > 0) {
              return $consulta->rows;
          } else {
              return null;
          }                  
    }

    public function queryBusquedaHistoricoUser($parametros){
        $query = $this->db->select("gl_nombre_proyecto as cd_id_proyecto,gl_comentario,id_historial,nombre as cd_id_solicitud,gl_descripcion as cd_id_estado,concat(nombres,' ',apellidos) as cd_id_usuario,date_format(fc_fecha_movimiento,'%d-%m-%Y')as fc_fecha_movimiento,gl_evento_historial
          from historico_tickets left join usuario on id=cd_id_usuario
          join tipo_estado on id_estado=cd_id_estado
          join tickets on id_ticket=cd_id_solicitud
          join proyecto on id_proyecto=cd_id_proyecto
          where cd_id_usuario=?");
                 
        if(!empty($parametros["gl_comentario"])){
            $query->whereAND("t.gl_comentario" , "%" . $parametros["gl_comentario"] . "%", "LIKE");
        }

        if(!empty($parametros["cd_id_proyecto"])){
            $query->whereAND("t.cd_id_proyecto" , "%" . $parametros["cd_id_proyecto"] . "%", "LIKE");
        }

        if(!empty($parametros["cd_id_solicitud"])){
            $query->whereAND("t.cd_id_solicitud" , "%" . $parametros["cd_id_solicitud"] . "%", "LIKE");
        }

        if(!empty($parametros["cd_id_estado"])){
            $query->whereAND("t.cd_id_estado" , "%" . $parametros["cd_id_estado"] . "%", "LIKE");
        }

        if(!empty($parametros["cd_id_usuario"])){
            $query->whereAND("t.cd_id_usuario" , "%" . $parametros["cd_id_usuario"] . "%", "LIKE");
        }

        if(!empty($parametros["fc_fecha_movimiento"])){
            $query->whereAND("t.fc_fecha_movimiento" , "%" . $parametros["fecha"] . "%", "LIKE");
        }

        if(!empty($parametros["gl_evento_historial"])){
            $query->whereAND("t.gl_evento_historial" , "%" . $parametros["gl_evento_historial"] . "%", "LIKE");
        }

                             
        fb($query->query());
        return $query;
    }

     public function insHistorico($datos){
      extract($datos);
      $query = "insert into historico_tickets values(null,?,?,?,?,?,?)";
      $parametros = array(//son lols nombres de los campos de texto de la vista
          $cd_id_solicitud,
          $cd_id_estado,
          $cd_id_usuario,
          $fc_fecha_creacion,
          $gl_evento_historial,
          $cd_id_tarea,
      );

    

      if ($this->db->execQuery($query, $parametros)) {
           return $this->db->lastInsertId();

       } else {
           return null;
       }
     }
}