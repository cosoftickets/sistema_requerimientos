<?php

class DAOComuna extends Model{

    /**
     *
     * @var string 
     */
    protected $_tabla = "comuna_chile";
    
    /**
     *
     * @var boolean 
     */
    protected $_transaccional = false;
    
    /**
     * 
     */
    function __construct(){
            parent::__construct();
    }
        
}
