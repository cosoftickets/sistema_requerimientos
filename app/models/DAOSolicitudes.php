<?php

class DAOSolicitudes extends Model
{
    /**
     * Constructor
     */

    protected $_tabla = "tickets";


    function __construct()
    {
        parent::__construct();
    }

    public function getPrioridad(){
        $query = $this->db->select("id, gl_descripcion")->from("prioridad");
        $resultado = $query->getResult();
        if($resultado->numRows>0){
            return $resultado->rows;
        }else{
            return NULL;
        }
    }

    public function getListaTrabajadores(){
        $query = $this->db->select("id, rut, nombres, apellidos")->from("usuario");
        $resultado = $query->getResult();
        if($resultado->numRows>0){
            return $resultado->rows;
        }else{
            return NULL;
        }
    }

    public function getListaProyectos(){
        $query = $this->db->select("id_proyecto, gl_nombre_proyecto")->from("proyecto");
        $resultado = $query->getResult();
        if($resultado->numRows>0){
            return $resultado->rows;
        }else{
            return NULL;
        }
    }

    public function getEstadoSolicitud($id_solicitud){
        $query = "select cd_id_estado FROM tickets WHERE id_ticket =". $id_solicitud;
        $consulta = $this->db->getQuery($query,array($id_solicitud));

        if ($consulta->numRows > 0) {
            return $consulta->rows->row_0;
        } else {
            return null;
        }
    }

    public function insSolicitud($datos){
        extract($datos);
        $query = "insert into tickets values(null,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $parametros = array(//son lols nombres de los campos de texto de la vista
            $nombre,
            $gl_comentario,
            $fc_fecha_creacion,
            $fc_fecha_termino,
            $fc_fecha_entrega,
            $cd_id_estado,
            $id_prioridad,
            $cd_id_usuario,
            $fc_fecha_diferencia,
            $id_proyecto,
            $gl_detalle_finalizado,
            $nr_horas_estimadas,
            $nr_horas_utilizadas,
        );

        if ($this->db->execQuery($query, $parametros)) {
            return $this->db->lastInsertId();

        } else {
            return null;
        }
    }

    
    public function queryBusquedaTicket($parametros){
        $query = $this->db->select("t.*, p.gl_nombre_proyecto")
                          ->from($this->_tabla . " t")
                          ->join("proyecto p", "p.id_proyecto = t.cd_id_proyecto");
        fb($query->query());

        return $query;
    }

    public function getSolicitudById($id_solicitud){
        $query = "select nr_horas_estimadas,nr_horas_utilizadas, t.gl_detalle_finalizado, t.id_ticket as id_solicitud, t.nombre as asunto, t.gl_comentario as comentario, t.cd_id_estado as id_estado, t.cd_id_prioridad, t.fc_fecha_creacion, t.fc_plazo as fecha_entrega, t.fc_fecha_termino, t.nr_fecha_diferencia, e.gl_descripcion as desc_estado, u.id as id_usuario, u.nombres as nombre, u.apellidos as apellido, u.id_perfil, p.id, p.gl_descripcion as desc_prioridad, t.cd_id_proyecto as id_proyecto, pro.gl_nombre_proyecto as nombre_proyecto
                from tickets t, tipo_estado e, usuario u, prioridad p , proyecto pro
                where t.id_ticket = $id_solicitud and t.cd_id_estado = e.id_estado and t.cd_id_usuario = u.id and t.cd_id_prioridad = p.id and t.cd_id_proyecto = pro.id_proyecto
                group by t.id_ticket";
        $consulta = $this->db->getQuery($query,array($id_solicitud));

        if ($consulta->numRows > 0) {
            return $consulta->rows->row_0;
        } else {
            return null;
        }
    }  

    public function getBadge($id_usuario){
        $query = "select COUNT(*) as badge from tickets where cd_id_usuario = $id_usuario and cd_id_estado = 1";
        $consulta = $this->db->getQuery($query,array($id_solicitud));

        if ($consulta->numRows > 0) {
            return $consulta->rows->row_0;
        } else {
            return null;
        }
    }  

 public function getSolicitudesAsignadas($id_usuario = null, $estado = null){

        $query = "select t.nombre, t.id_ticket as id_solicitud,  COUNT( ta.id_tarea ) AS cantidadTareas, SUM(ta.nr_estado) as sumaEstados, t.nombre as asunto, t.cd_id_estado as id_estado, t.cd_id_prioridad, t.fc_fecha_creacion, date_format(t.fc_plazo,'%d-%m-%Y') as fecha_entrega, t.gl_comentario, e.gl_descripcion as desc_estado, p.id, p.gl_descripcion as desc_prioridad, t.cd_id_proyecto as id_proyecto, pro.gl_nombre_proyecto as nombre_proyecto, ta.nr_estado
            FROM tickets As t 
            LEFT JOIN tareas as ta     ON (t.id_ticket = ta.cd_id_ticket and ta.nr_estado !=2)
            INNER JOIN tipo_estado as e ON (t.cd_id_estado = e.id_estado)
            INNER JOIN prioridad as p   ON (t.cd_id_prioridad = p.id)
            INNER JOIN proyecto as pro  ON (t.cd_id_proyecto = pro.id_proyecto )
            WHERE t.cd_id_usuario = $id_usuario
            GROUP By t.id_ticket";

        $resultado = $this->db->getQuery($query);
        if ($resultado->numRows > 0) {

            $arrSalida = array();
            $i=0;
            foreach ($resultado->rows as $itm) {
                $arrSalida[] = $itm;
            }
            return $arrSalida;
        } else {
            return NULL;
        }
    }

     public function getTareasAsignadas($id_usuario = null, $estado = null){

        $query = "select ti.id_ticket, ta.id_tarea from tareas ta, tickets ti where ta.cd_id_ticket = ti.id_ticket and ti.cd_id_usuario = $id_usuario Order By ti.id_ticket Asc";

        $resultado = $this->db->getQuery($query);
        if ($resultado->numRows > 0) {

            $arrSalida = array();
            $i=0;
            foreach ($resultado->rows as $itm) {
                $arrSalida[] = $itm;
            }
            return $arrSalida;
        } else {
            return NULL;
        }
    }

    public function todasSolicitudes(){

        $query = "select t.nombre,t.id_ticket as id_solicitud,  COUNT( ta.id_tarea ) AS cantidadTareas, SUM(ta.nr_estado) as sumaEstados, t.nombre as asunto, t.cd_id_estado as id_estado, t.cd_id_prioridad, t.fc_fecha_creacion, date_format(t.fc_plazo,'%d-%m-%Y') as fecha_entrega, t.fc_fecha_termino,  t.gl_comentario, e.gl_descripcion as desc_estado, p.id, p.gl_descripcion as desc_prioridad, t.gl_detalle_finalizado, t.nr_horas_utilizadas, t.cd_id_proyecto as id_proyecto, pro.gl_nombre_proyecto as nombre_proyecto, ta.nr_estado
            FROM tickets As t 
            LEFT JOIN tareas as ta     ON (t.id_ticket = ta.cd_id_ticket and ta.nr_estado !=2)
            INNER JOIN tipo_estado as e ON (t.cd_id_estado = e.id_estado)
            INNER JOIN prioridad as p   ON (t.cd_id_prioridad = p.id)
            INNER JOIN proyecto as pro  ON (t.cd_id_proyecto = pro.id_proyecto )
            GROUP By t.id_ticket";

        $resultado = $this->db->getQuery($query);
        if ($resultado->numRows > 0) {

            $arrSalida = array();
            $i=0;
            foreach ($resultado->rows as $itm) {
                $arrSalida[] = $itm;
            }
            return $arrSalida;
        } else {
            return NULL;
        }
    }

    public function Reportes($fecha1, $fecha2){

        $query = "select t.nombre,t.id_ticket as id_solicitud,  COUNT( ta.id_tarea ) AS cantidadTareas, SUM(ta.nr_estado) as sumaEstados, t.nombre as asunto, t.cd_id_estado as id_estado, t.cd_id_prioridad, t.fc_fecha_creacion, date_format(t.fc_plazo,'%d-%m-%Y') as fecha_entrega, t.fc_fecha_termino,  t.gl_comentario, e.gl_descripcion as desc_estado, p.id, p.gl_descripcion as desc_prioridad, t.gl_detalle_finalizado, t.nr_horas_utilizadas, t.cd_id_proyecto as id_proyecto, pro.gl_nombre_proyecto as nombre_proyecto, ta.nr_estado, u.nombres, u.apellidos
            FROM tickets As t 
            LEFT JOIN tareas as ta      ON (t.id_ticket = ta.cd_id_ticket and ta.nr_estado !=2)
            INNER JOIN tipo_estado as e ON (t.cd_id_estado = e.id_estado)
            INNER JOIN prioridad as p   ON (t.cd_id_prioridad = p.id)
            INNER JOIN proyecto as pro  ON (t.cd_id_proyecto = pro.id_proyecto )
            INNER JOIN usuario as u     ON (t.cd_id_usuario= u.id )
            WHERE t.fc_fecha_termino between '$fecha1' and '$fecha2'
            GROUP By t.id_ticket";


        $resultado = $this->db->getQuery($query);
        if ($resultado->numRows > 0) {

            $arrSalida = array();
            $i=0;
            foreach ($resultado->rows as $itm) {
                $arrSalida[] = $itm;
            }
            return $arrSalida;
        } else {
            return NULL;
        }
    }

    public function ticketPorUsuario($idUsuario){
        $query="select t.cd_id_usuario,(select(concat(nombres,' ',apellidos))from usuario where id=t.cd_id_usuario)as asignado, ht.cd_id_usuario, cd_id_tarea,id_ticket, id_historial, nombre, e.gl_descripcion as estado,p.gl_descripcion as prioridad,gl_comentario,gl_evento_historial, gl_nombre_tarea, concat(nombres,' ',apellidos)as usuario, date_format(fc_fecha_movimiento,'%d-%m-%Y %H:%i:%s') as fecha, gl_descripcion_tarea,tr.nr_estado
            from historico_tickets  ht
            left join tipo_estado e on id_estado=cd_id_estado
            left join usuario  on id=cd_id_usuario 
            left join tickets t on id_ticket=cd_id_solicitud
            left join prioridad p on p.id=cd_id_prioridad 
            left join tareas tr on id_tarea=cd_id_tarea
                where t.cd_id_usuario='$idUsuario'";
        $resultado=$this->db->getQuery($query);
        if($resultado->numRows>0){
            return $resultado->rows;
        }
        else{
            return NULL;
        }
    }

    public function insComentario($datos){
            extract($datos);
            $query = "insert into maestro_comentario values(null,?,?,?)";
            $parametros = array(//son lols nombres de los campos de texto de la vista
                $ticket,
                $nuevo_comentario,
                $id_usuario,
                //$estado,
                
            );

            if ($this->db->execQuery($query, $parametros)) {
                return $this->db->lastInsertId();

            } else {
                return null;
            }
        }

    public function estByProyectoAsignado(){
        $query   =  "select * , COUNT( u.id ) AS cant_proyectos,
                    (SELECT COUNT(cd_id_estado) FROM tickets WHERE cd_id_estado = 7 and u.id = cd_id_usuario) as cant_terminados ,
                    (SELECT COUNT(id_ticket) FROM tickets WHERE fc_fecha_termino <= fc_plazo and cd_id_estado = 7 and u.id = cd_id_usuario) as pro_atiempo,
                    (SELECT COUNT(id_ticket) FROM tickets WHERE fc_fecha_termino > fc_plazo and cd_id_estado = 7 and u.id = cd_id_usuario) as pro_atrasados
                    FROM  `usuario` u
                    INNER JOIN tickets t
                    WHERE u.id = t.cd_id_usuario
                    GROUP BY u.id";

        $resultado = $this->db->getQuery($query);
        if ($resultado->numRows > 0) {

            $arrSalida = array();
            $i=0;
            foreach ($resultado->rows as $itm) {
                $arrSalida[] = $itm;
            }
            return $arrSalida;
        } else {
            return NULL;
        }
    }

    public function estProyectoAsignadoByUser($id_usuario){
        $query   =  "select * , COUNT( u.id ) AS cant_proyectos,
                    (SELECT COUNT(cd_id_estado) FROM tickets WHERE cd_id_estado = 7 and cd_id_usuario = $id_usuario) as cant_terminados ,
                    (SELECT COUNT(id_ticket) FROM tickets WHERE fc_fecha_termino <= fc_plazo and cd_id_estado = 7 and cd_id_usuario = $id_usuario) as pro_atiempo,
                    (SELECT COUNT(id_ticket) FROM tickets WHERE fc_fecha_termino > fc_plazo and cd_id_estado = 7 and cd_id_usuario = $id_usuario) as  pro_atrasados
                    FROM  `usuario` u
                    INNER JOIN tickets t
                    WHERE u.id = $id_usuario and t.cd_id_usuario = $id_usuario";

        $consulta = $this->db->getQuery($query,array($id_usuario));

        if ($consulta->numRows > 0) {
            return $consulta->rows->row_0;
        } else {
            return null;
        }
    }
}

?>