<?php

class DAORegion extends Model{

    /**
     *
     * @var string 
     */
    protected $_tabla = "region";
    
    /**
     *
     * @var boolean 
     */
    protected $_transaccional = false;
    
    /**
     * 
     */
    function __construct(){
            parent::__construct();
    }
        
}
