<?php
	class DAODetalleSolicitud extends Model{

		//protected $_tabla = "historico_tickets";

		function __construct(){
        	parent::__construct();
    	}

	    public function getDetalleSolicitudById($cd_id_solicitud){
	        $query ="select t.cd_id_usuario,(select(concat(nombres,' ',apellidos))from usuario where id=t.cd_id_usuario)as asignado,ht.cd_id_usuario,cd_id_tarea,id_ticket,id_historial,nombre,e.gl_descripcion as estado,p.gl_descripcion as prioridad,gl_comentario,gl_evento_historial,gl_nombre_tarea,
		        concat(nombres,' ',apellidos)as usuario,date_format(fc_fecha_movimiento,'%d-%m-%Y %H:%i:%s') as fecha,
		        gl_descripcion_tarea,tr.nr_estado
		        from historico_tickets  ht
		        left join tipo_estado e on id_estado=cd_id_estado
		        left join usuario  on id=cd_id_usuario 
		        left join tickets t on id_ticket=cd_id_solicitud
		        left join prioridad p on p.id=cd_id_prioridad 
		        left join tareas tr on id_tarea=cd_id_tarea
							where cd_id_solicitud=?";
	        $consulta = $this->db->getQuery($query,array($cd_id_solicitud));

	        if ($consulta->numRows > 0) {
	            return $consulta->rows;
	        } else {
	            return null;
	        }
	    }  

	     public function getTareaById($cd_id_ticket){
	        $query = "select id_tarea,gl_nombre_tarea,gl_descripcion_tarea,nr_estado,cd_id_ticket  
			         from tareas
			         where cd_id_ticket=?
			         and nr_estado<>2";
	        $consulta = $this->db->getQuery($query,array($cd_id_ticket));

	        if ($consulta->numRows > 0) {
	            return $consulta->rows;
	        } else {
	            return null;
	        }
	    } 

	    public function getDatosTicket($id_ticket){
	        $query = "select nr_fecha_diferencia,nr_horas_estimadas,nr_horas_utilizadas,gl_detalle_finalizado,gl_comentario,nr_fecha_diferencia, id_ticket,nombre,t.gl_comentario,date_format(t.fc_fecha_creacion,'%d-%m-%Y')as fc_fecha_creacion,concat(nombres,' ',apellidos)as cd_id_usuario,te.gl_descripcion,date_format(fc_plazo,'%d-%m-%Y')as fc_plazo,date_format(fc_fecha_termino,'%d-%m-%Y')as fc_fecha_termino,gl_nombre_proyecto,datediff(fc_plazo,now())as DiasAtraso,(select count(*) from tareas where cd_id_ticket=$id_ticket and nr_estado<>2)as TotalTareas,(select count(*) from tareas where cd_id_ticket=$id_ticket and nr_estado=1)as TareasTerminadas,cd_id_estado,
				(select count(*) from tareas where cd_id_ticket=$id_ticket and nr_estado=0)as TareasPendientes
	        	from tickets t 
	        	left join usuario on id=cd_id_usuario 
	        	left join tipo_estado te on id_estado=cd_id_estado 
	        	left join proyecto on id_proyecto=cd_id_proyecto 
	        	where id_ticket=$id_ticket";
	        $consulta = $this->db->getQuery($query,array($id_ticket));

	        if ($consulta->numRows > 0) {
	            return $consulta->rows;
	        } else {
	            return null;
	        }
	    } 

	    public function getAdjuntosTicket($id_ticket){
	        $query = "select cd_solicitud_fk_archivo,id_archivo,gl_ruta_archivo,gl_nombre_archivo,gl_comentario,date_format(fc_fecha_archivo,'%d-%m-%Y')as fc_fecha_archivo
	        	from archivos 
	        	where cd_solicitud_fk_archivo=?";
	        $consulta = $this->db->getQuery($query,array($id_ticket));

	        if ($consulta->numRows > 0) {
	            return $consulta->rows;
	        } else {
	            return null;
	        }
	    } 

	    public function getComentario($idTicket){
	        $query="select id_comentario,cd_id_ticket,gl_comentario,concat(nombres,' ',apellidos)as usuario
	                from maestro_comentario
	                left join usuario on id=id_usuario
	                where cd_id_ticket=?";
	        $resultado=$this->db->getQuery($query,array($idTicket));
	        if($resultado->numRows>0){
	            return $resultado->rows;
	        }
	        else{
	            return NULL;
	        }
	    } 

	    public function eliminarAdjunto($idArchivo){
	    	$query="delete from archivos where id_archivo=$idArchivo";
	    	 $resultado=$this->db->getQuery($query,array($idArchivo));
	        if($resultado->numRows>0){
	            return $resultado->rows;
	        }
	        else{
	            return NULL;
	        }
	    }
	}

?>
