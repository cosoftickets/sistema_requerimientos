<?php

class Estadisticas extends Controller
{

    /**
     *
     * @var DAOUsuarios
     */
    protected $_DAOSolicitudes;
    protected $_DAOTareas;
    protected $campo_id = "id_estadistica";

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->_DAOUsuarios    = $this->load->model("DAOUsuarios");
        $this->_DAOAdjuntos    = $this->load->model("DAOAdjuntos");
        $this->_DAOSolicitudes = $this->load->model('DAOSolicitudes');
        $this->_DAOHistorico   = $this->load->model('DAOHistorico');
        $this->_DAOTareas      = $this->load->model('DAOTareas');
        //$this->smarty->addPluginsDir(APP_PATH . "views/templates/mantenedor_avanzados/grilla_historico_user/plugins/");
    }

    /**
     * Index
     */
    public function index()
    {

        $data = $this->_DAOSolicitudes->estByProyectoAsignado();
        //print_r($data[0]->id);die();
        /*  $data = array(
                'Cristian' => $data[0]->cant_proyectos,
                'Jose' => $data[1]->cant_proyectos,  
                'Domingo' => $data[2]->cant_proyectos,
            );*/
        $this->smarty->assign("data", $data);  
        $this->load->javascript(STATIC_FILES.'js/templates/solicitudes/solicitudes.js');
        $this->_display('Solicitudes/Nuevo/estadisticas.tpl');

        $this->load->javascript(STATIC_FILES.'js/templates/solicitudes/solicitudes.js');
        
    }
        public function CargarGrafico(){
        $id_usuario = $_POST["idUser"];
        $data = $this->_DAOSolicitudes->estProyectoAsignadoByUser($id_usuario);
        //print_r($data);die();
        //print_r($data);die();
        $data = array(
                //'asignados'  => $data->cant_proyectos,
                //'terminados' => $data->cant_terminados,  
                'atrasados'  => $data->pro_atrasados,
                'atiempo'    => $data->pro_atiempo,
        );
        $this->load->javascript(STATIC_FILES.'js/plugins/Chart.js/Chart.js');
        $this->load->javascript(STATIC_FILES.'js/templates/home/home.js');
        $this->load->javascript('EstadisticasUser.grafico1('.json_encode($data).');');

        $template = $this->smarty->fetch("Solicitudes/Nuevo/grafico.tpl");
        echo $template;
    }
}
