<?php
	class Historico extends Controller{
		function __construct(){
			parent::__construct();
			$this->_DAOHistorico = $this->load->model("DAOHistorico");
			$this->smarty->addPluginsDir(APP_PATH . "views/templates/mantenedor_avanzados/grilla_historico/plugins/");
		}

		public function index(){

		}

		public function historico(){
			$_DAOHistorico = $this->load->model('DAOHistorico');
        	$historico = $_DAOHistorico->queryBusquedaHistorico();
        	//print_r($usuarios); die();
        	$this->smarty->assign('historico', $historico);
			$this->_display('avanzados/historico.tpl');
		}
	}
?>