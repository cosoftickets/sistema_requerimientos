<?php
	class DetalleSolicitud extends Controller{
		function __construct(){
			parent::__construct();
			$this->_DAODetalleSolicitud = $this->load->model("DAODetalleSolicitud");
			$this->_DAOSolicitudes = $this->load->model("DAOSolicitudes");
			//$this->smarty->addPluginsDir(APP_PATH . "views/templates/mantenedor_avanzados/detalle_solicitud/plugins/");

		}

		public function index(){
		}

		public function detalleSolicitud(){
			
			//$this->_addJavascript(STATIC_FILES.'js/templates/estados/form.js');
	        $DAODetalleSolicitud = $this->_DAODetalleSolicitud;
	        $parametros = $this->request->getParametros();
	        $param=$this->request->getParametros();
	        $parameters=$this->request->getParametros();
	        $adjuntos=$this->request->getParametros();
	        $comentario=$this->request->getParametros();
	        $this->smarty->assign("nuevo", false);
	        
	        $detalle = $DAODetalleSolicitud->getDetalleSolicitudById($parametros[0]);
	        $tareas=$DAODetalleSolicitud->getTareaById($param[0]);
	        $parameters=$DAODetalleSolicitud->getDatosTicket($parameters[0]);
	        $comentario=$DAODetalleSolicitud->getComentario($comentario[0]);
	        //print_r($parameters);die();
	        $adjuntos=$DAODetalleSolicitud->getAdjuntosTicket($adjuntos[0]);
  	        if(!is_null($detalle)){
	        	$this->smarty->assign("detalle", $detalle);
	        	$this->smarty->assign("tareas", $tareas);
	        	$this->smarty->assign("parameters", $parameters);
	        	$this->smarty->assign("adjuntos", $adjuntos);
	        	$this->smarty->assign('comentarios',$comentario);
	            $this->smarty->display('avanzados/detalle.tpl');
	            $this->load->javascript(STATIC_FILES . 'template/plugins/datepicker/bootstrap-datepicker.js');
	            $this->load->javascript(STATIC_FILES . 'template/plugins/datepicker/locales/bootstrap-datepicker.es.js');
	            $this->load->javascript('$(".datepicker").datepicker();');
	            $this->load->javascript(STATIC_FILES . 'js/templates/solicitudes/comentarios.js');
	            $this->load->javascript(STATIC_FILES . 'js/templates/solicitudes/adjuntos.js');
	            $this->load->javascript(STATIC_FILES . 'template/plugins/alertify/alertify.js');
	        } 
	        else {
	            throw new Exception("El historial que está buscando no existe");
	        }
	    }

	    public function listarComentarios(){
	    	$parametros = $this->request->getParametros();
	    	$comentario=$this->_DAODetalleSolicitud->getComentario($parametros[0]);
	    	//print_r($comentario);die();
	    	$this->smarty->assign('comentarios',$comentario);
			$this->smarty->display('avanzados/grillaComentario.tpl');
	    }

	    public function listarAdjuntos(){
	    	$adjuntos = $this->request->getParametros();
	    	$adjuntos=$this->_DAODetalleSolicitud->getAdjuntosTicket($adjuntos[0]);
	    	//print_r($comentario);die();
	    	$this->smarty->assign('adjuntos',$adjuntos);
			$this->smarty->display('avanzados/grillaAdjuntos.tpl');
	    }

	    public function eliminarAdjunto(){
	    	$adjunto=$this->request->getParametros();
	    	$eliminarAdjunto=$this->_DAODetalleSolicitud->eliminarAdjunto($adjunto[0]);
	    }

	}
?>