<?php

class Home extends Controller{

    /**
     * Constructor
     */
    function __construct(){
        parent::__construct();
        Acceso::set("ALL");
        $this->smarty->addPluginsDir(APP_PATH . "views/templates/home/plugins/");
        $this->_DAOUsuarios 	= $this->load->model("DAOUsuarios");
        $this->_DAOComuna 		= $this->load->model("DAOComuna");
        $this->_DAOInstalacion 	= $this->load->model("DAOInstalacion");
		$this->_DAODatosRemoto  = $this->load->model("DAODatosRemotos");
    }

    /**
     * 
     */
    public function index(){
        
    }

    /**
     * 
     */
    public function dashboard(){
        if ($_SESSION['perfil'] == 1) {
            $this->load->lib('Fechas',false);
            $_DAODocumento = $this->load->model('DAODocumento');

            # GRAFICO DE ESTADOS DE TICKETS PERFIL ADM
            $creadas = $_DAODocumento->getTicketsPorEstado(1)->numRows;
            $recibidas = $_DAODocumento->getTicketsPorEstado(2)->numRows;
            $desarrollo = $_DAODocumento->getTicketsPorEstado(3)->numRows;
            $finalizadas = $_DAODocumento->getTicketsPorEstado(7)->numRows;
            $derivadas = $_DAODocumento->getTicketsPorEstado(5)->numRows;
            $archivadas = $_DAODocumento->getTicketsPorEstado(6)->numRows;
            $completado = $_DAODocumento->getTicketsPorEstado(4)->numRows;

            $cero_ocho=$_DAODocumento->diferenciaDiasFechas(0,-8)->numRows;
            $nueve_quince=$_DAODocumento->diferenciaDiasFechas(-9,-15)->numRows;
            $dieciseis_treinta=$_DAODocumento->diferenciaDiasFechas(-16,-30)->numRows;
            $treinta_mas=$_DAODocumento->diferenciaDiasFechas(-30,-1000)->numRows;
            //$arr = $_DAODocumento->getDocumentosAsignados();
            /*if($cero_ocho==0 && $nueve_quince==0 && $dieciseis_treinta==0 && $treinta_mas==0){

            }*/
            $estados = array(
                'creadas' => $creadas,
                'recibidas' => $recibidas,
                'desarrollo' => $desarrollo,
                'finalizadas' => $finalizadas,
                'derivadas' => $derivadas,
                'archivadas' => $archivadas,
                'completado'=>$completado
            );

            # GRAFICOS PARA RANGO DE ATRASOS PERFIL ADM
            $atrasos = array(
                'cero_ocho' => $cero_ocho,
                'nueve_quince' => $nueve_quince,
                'dieciseis_treinta' => $dieciseis_treinta,
                'treinta_mas' => $treinta_mas
            );

            $this->smarty->assign("FOLDER",'acceso');   
            $this->_display('home/dashboard.tpl');
            $this->load->javascript(STATIC_FILES.'js/plugins/Chart.js/Chart.js');
            $this->load->javascript(STATIC_FILES.'js/templates/home/home.js');
            $this->load->javascript('Home.graficoEstados('.json_encode($estados).');');
            $this->load->javascript('Home.graficoEstados1('.json_encode($atrasos).');');
        }
        # usuarios no administradores
        else{

            $_DAODocumento = $this->load->model('DAODocumento');
            $session = New Zend_Session_Namespace("usuario_carpeta");
            
            $this->load->lib('Fechas',false);

            # GRAFICO DE ESTADOS DE TICKETS PERFIL NO ADM
            $creadas = $_DAODocumento->getTicketsPorUsuario($session->id,1)->numRows;
            $recibidas = $_DAODocumento->getTicketsPorUsuario($session->id,2)->numRows;
            $desarrollo = $_DAODocumento->getTicketsPorUsuario($session->id,3)->numRows;
            $finalizadas = $_DAODocumento->getTicketsPorUsuario($session->id,7)->numRows;
            $derivadas = $_DAODocumento->getTicketsPorUsuario($session->id,5)->numRows;
            $archivadas = $_DAODocumento->getTicketsPorUsuario($session->id,6)->numRows;
            $completado = $_DAODocumento->getTicketsPorUsuario($session->id,4)->numRows;

            # GRAFICOS PARA RANGO DE ATRASOS PERFIL NO ADM
            $cero_ocho=$_DAODocumento->diferenciaDiasFechasUsuario($session->id,0,-8)->numRows;
            $nueve_quince=$_DAODocumento->diferenciaDiasFechasUsuario($session->id,-9,-15)->numRows;
            $dieciseis_treinta=$_DAODocumento->diferenciaDiasFechasUsuario($session->id,-16,-30)->numRows;
            $treinta_mas=$_DAODocumento->diferenciaDiasFechasUsuario($session->id,-30,-1000)->numRows;
            $estados = array(
                'creadas' => $creadas,
                'recibidas' => $recibidas,
                'desarrollo' => $desarrollo,
                'finalizadas' => $finalizadas,
                'derivadas' => $derivadas,
                'archivadas' => $archivadas,
                'completado'=>$completado
            );

            $atrasos = array(
                'cero_ocho' => $cero_ocho,
                'nueve_quince' => $nueve_quince,
                'dieciseis_treinta' => $dieciseis_treinta,
                'treinta_mas' => $treinta_mas
            );

            $this->smarty->assign("FOLDER",'acceso');   
            $this->_display('home/dashboard.tpl');
            $this->load->javascript(STATIC_FILES.'js/plugins/Chart.js/Chart.js');
            $this->load->javascript(STATIC_FILES.'js/templates/home/home.js');
            $this->load->javascript('Home.graficoEstados('.json_encode($estados).');');
            $this->load->javascript('Home.graficoEstados1('.json_encode($atrasos).');');
        }
		
    }
    public function ocultarTour(){
		$this->_DAOUsuarios->update(array("gl_ocultar_tour" => "1"), $_SESSION['usuario']['id']);
		$_SESSION['usuario']['gl_ocultar_tour'] = 1;
    }	
    public function ocultarTourSession(){
		$_SESSION['usuario']['gl_ocultar_tour'] = 1;
    }		
	
    public function buscarRut(){
	
		$arrComunicacion[0] = "OK";
		$arrComunicacion['rut']	= trim($_POST['rut']);	
		//echo "http://localhost/sipresa/jsonp/BuscarRut.php?rut=".$arrComunicacion['rut'];
		$arrBusqueda = get_data("http://200.55.194.54/programacion/jsonp/BuscarRut.php?rut=".$arrComunicacion['rut']);		
		$arrBusqueda = json_decode($arrBusqueda);	
		$this->smarty->assign("arrResultado",$arrBusqueda);	
        $this->smarty->display('home/tabla_buscar_rut.tpl');		
    }			
	
    public function buscarDireccion(){
			
		/*Obtener parametros desde */
		
		//print_r($_POST);
		
		$arr[0] = "OK";
		$arr['region'] 			= trim($_POST['region']); 
		$arr['comuna'] 			= trim($_POST['id_comuna']);
		$arr['calle'] 			= trim($_POST['gl_calle']);
		$arr['numero'] 			= trim($_POST['nr_numero']);
		$arr['rut']				= trim($_POST['gl_rut']);	
		$arr['nombre_fantasia']	= trim($_POST['gl_nombre_fantasia']);	
		$arr['razon_social']	= trim($_POST['gl_razon']);	
		
		
		//error_reporting(E_ALL);
		//ini_set('display_errors', '1');
	

		
		//Obtener datos desde SIPRESA
		$arrBusqueda = get_data_encrypt("http://200.55.194.54/programacion/jsonp/BuscarDireccion.php",$arr);			
	
	
		//print_r($arrBusqueda);
		
		//Obtener registros de BD local
		//$arrBusqueda = $this->_DAOInstalacion->queryBusqueda($arr);
		
		$this->smarty->assign("arrResultado",$arrBusqueda);	
		$this->smarty->display('home/tabla_buscar_rut.tpl');		

    }				


}

	
?>	