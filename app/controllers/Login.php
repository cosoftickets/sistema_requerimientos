<?php


class Login extends Controller{

    /**
     *
     * @var DAOUsuarios 
     */
    protected $_DAOUsuarios;
    
    /**
     * Constructor
     */
    function __construct(){
        parent::__construct();
        $this->_DAOUsuarios = $this->load->model("DAOUsuarios");
    }

    /**
     * Formulario de logeo
     */
    public function index(){
        $session = New Zend_Session_Namespace("usuario_carpeta");
        if(isset($session->id)){
            $usuario = $this->_DAOUsuarios->getById($session->id);
            if(!is_null($usuario)){
                header("location: index.php/Home/dashboard");
                die();
            }
        }
        $this->smarty->assign("hidden","hidden");
        $this->smarty->display('login/login.tpl');
    }
    
    /**
     * 
     */
    public function recuperar_password(){
        $this->_addJavascript(STATIC_FILES.'js/templates/login/recuperar_password.js');
        $this->_display('login/recuperar_password.tpl', false);
    }
    
    /**
     * 
     */
    public function recuperar_password_email(){
        header('Content-type: application/json');
        $email = "";
        $correcto = false;
        if(trim($this->_request->getParam("rut"))!=""){
            $usuario = $this->_DAOUsuarios->getByRut($this->_request->getParam("rut"));
            if(!is_null($usuario)){
                $correcto = true;
                $string = $this->load->lib("Helpers/String", true, "String");
                $cadena = $string->cadenaAleatoria();

                $this->smarty->assign("nombre", $usuario->nombres . " " . $usuario->apellidos);
                $this->smarty->assign('pass',$cadena);
                $this->smarty->assign("url", HOST . "/index.php/Usuario/modificar_password/" . $cadena);

                $this->_DAOUsuarios->update(
                                            array("password" => $cadena),
                                            $usuario->id
                                           );

                $this->load->lib('Email', false);
                $remitente = "midas@minsal.cl";
                $nombre_remitente =  "Facturacion";
                $destinatario = $usuario->email;

                $asunto = "FACTURACIÓN - Recuperar contraseña";
                $mensaje = $this->smarty->fetch("login/recuperar_password_email.tpl");
                Email::sendEmail($destinatario, $remitente, $nombre_remitente, $asunto, $mensaje);

            } else {
                $correcto = false;
            }
        }
        
        $salida = array("email"    => $email,
                        "correcto" => $correcto);
        
        $json = Zend_Json::encode($salida);
        echo $json;
    }

    /**
     * 
     */
    public function procesar(){
        $rut      = trim($this->_request->getParam("rut"));
        $password = trim($this->_request->getParam("password"));
        $recordar = trim($this->_request->getParam("recordar"));
        
        $usuario = $this->_DAOUsuarios->getByRut($rut);
        
        $valido = false;
        if(!is_null($usuario)){
            if($usuario->password == sha1($password)){
                $valido = true;
            }
        }
        
        if($valido and $rut!="" and $password != ""){
            $session 			= New Zend_Session_Namespace("usuario_carpeta");
            $session->id 		= $usuario->id;
            $session->gl_ocultar_tour 	= $usuario->gl_ocultar_tour;
            $session->rut 		= $usuario->rut;
            $session->usuario 	= $usuario->nombres." ".$usuario->apellidos;
            $_SESSION['id'] = $usuario->id;
            $_SESSION['perfil'] = $usuario->id_perfil;
            if($recordar == 1){
                setcookie('datos_usuario_carpeta', $usuario->id, time() + 365 * 24 * 60 * 60);
            }
            
            if($usuario->bo_password==1){
                header('Location: '.BASE_URI.'/Home/dashboard');
            }else{
                header('Location: '.BASE_URI.'/Login/actualizar');
            }
        }else{
            $this->smarty->assign("hidden","");
            $this->smarty->display('login/login.tpl');
        }
    }
    
    /**
     * Actualiza el password
     */
    public function actualizar(){
        $this->_addJavascript(STATIC_FILES.'js/templates/login/actualizar_password.js');
        $this->_display('login/actualizar.tpl');
    }

    /**
     * Guarda el nuevo password
     */
    public function ajax_guardar_nuevo_password(){
        header('Content-type: application/json');
        
        $session = New Zend_Session_Namespace("usuario_carpeta");
        
        $validar = $this->load->lib("Helpers/Validar/ActualizarPassword", true, "Validar_ActualizarPassword", $this->_request->getParams());
        if($validar->isValid()){
            $data = array("password" => sha1($this->_request->getParam("password")),
                          "codigo_cambiar_password" => "",
                          "bo_cambiar_password" => 0,
                          "bo_password" => 1);
            $this->_DAOUsuarios->update($data, $session->id);
        }
        $salida = array("error"    => $validar->getErrores(),
                        "correcto" => $validar->getCorrecto());
        
        $json = Zend_Json::encode($salida);
        echo $json;
    }

    /**
     * 
     */
    public function logoutUsuario(){
        
        if(isset($_COOKIE['datos_usuario_carpeta'])) {
            unset($_COOKIE['datos_usuario_carpeta']);
            setcookie('datos_usuario_carpeta', '', time() - 1); // empty value and old timestamp
        }
        unset($_SESSION['usuario_carpeta']);
        unset($_SESSION['adjuntos']);
        //session_destroy();
        header('Location:'.BASE_URI);
    }
    
    public function validaRutMidas(){
		$parametros   = $this->request->getParametros();
		$json         = array();
        
		print_r($parametros);
		die();
		
		if(isset($parametros[0])){
			$url = trim($parametros[0],'?');
			$url = explode("=",$url);
			$rut = trim($url[1]);
            
			if(is_null($rut) or empty($rut)){
				die();
			}
            
			$usuario = $this->_DAOUsuarios->getByRut($rut);
            
			if(count($usuario) > 0){
                $json['rut']       = $usuario->rut;
                $json['nombres']   = $usuario->nombres;
                $json['apellidos'] = $usuario->apellidos;
                $json['email']     = $usuario->email;

                echo json_encode($json);
            }
        }else{
            echo false;
        }
	}
    
    function loginRemoto(){
        $rut   = $this->_request->getParam('rut');
		$json         = array();
        
		if(isset($rut)){
            
			$usuario = $this->_DAOUsuarios->getByRut($rut);
			//print_r($usuario);die;
            $session 			        = New Zend_Session_Namespace("usuario_carpeta");
            $session->id 		        = $usuario->id;
            $session->region 		    = $usuario->id_region;
            $session->gl_ocultar_tour   = $usuario->gl_ocultar_tour;
            $session->rut 		        = $usuario->rut;
            $session->usuario 	        = $usuario->nombres." ".$usuario->apellidos;

            setcookie('datos_usuario_carpeta', $usuario->id, time() + 365 * 24 * 60 * 60);
            if($usuario->bo_password==1){
                header('Location: '.BASE_URI.'/Home/dashboard');
            }else{
                header('Location: '.BASE_URI.'/Login/actualizar');
            }
        }else{
			$this->smarty->assign("hidden","");
            $this->smarty->display('login/login.tpl');
		}
    }
}