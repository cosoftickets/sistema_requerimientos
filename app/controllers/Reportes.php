<?php

class Reportes extends Controller
{

    /**
     *
     * @var DAOUsuarios
     */
    protected $_DAOSolicitudes;
    protected $_DAOTareas;
    protected $campo_id = "id_reporte";

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->_DAOUsuarios    = $this->load->model("DAOUsuarios");
        $this->_DAOAdjuntos    = $this->load->model("DAOAdjuntos");
        $this->_DAOSolicitudes = $this->load->model('DAOSolicitudes');
        $this->_DAOHistorico   = $this->load->model('DAOHistorico');
        $this->_DAOTareas      = $this->load->model('DAOTareas');
        //$this->smarty->addPluginsDir(APP_PATH . "views/templates/mantenedor_avanzados/grilla_historico_user/plugins/");
    }

    /**
     * Index
     */
    public function index()
    {
        $reportes = $this->_DAOSolicitudes->Reportes();
        $this->smarty->assign("arrReportes", $reportes);
        $this->_display('Solicitudes/Nuevo/reportes.tpl');
    }

    public function diario()
    {
        $reporteDiario = date("Y-m-d");
        $reportes = $this->_DAOSolicitudes->Reportes($reporteDiario);
        $this->smarty->assign("arrReportes", $reportes);
        $this->_display('Solicitudes/Nuevo/reportes.tpl');
    }

    function _data_last_month_day(){ 
     $month = date('m');
     $year = date('Y');
     $day = date("d", mktime(0,0,0, $month+1, 0, $year)); 
     return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
     } 

     /** Actual mes primer dia **/
    function _data_first_month_day(){
         $month = date('m');
         $year = date('Y');
         return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
    } 


    function _data_last_month_day_filtro($mes){ 
     $month = $mes;
     $year = date('Y');
     $day = date("d", mktime(0,0,0, $month+1, 0, $year)); 
     return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
    } 

     /** Actual mes ultimo dia **/
    function _data_first_month_day_filtro($mes){
         $month = $mes;
         $year = date('Y');
         return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
    } 

    function recargarTablaReportes(){
        $mes = $_POST["filtroFecha"];
        $primer = $this->_data_first_month_day_filtro($mes);
        $ultimo = $this->_data_last_month_day_filtro($mes);
        $reportes = $this->_DAOSolicitudes->Reportes($primer, $ultimo);
        $this->smarty->assign("arrReportes", $reportes);
        $template = $this->smarty->fetch('Solicitudes/Grillas/tablaReportes.tpl');
        
        echo $template;
    } 

    // Filtro de tickets mensuales
    public function mensual()
    {   
        $tipo_reporte = 1;
        $mes_actual = date('m');
        $primer = $this->_data_first_month_day();
        $ultimo = $this->_data_last_month_day();
        $reportes = $this->_DAOSolicitudes->Reportes($primer, $ultimo);
        $this->smarty->assign("arrReportes", $reportes);
        $this->smarty->assign("mes_actual",$mes_actual);
        $this->smarty->assign("tipo_reporte",$tipo_reporte);
        $this->_display('Solicitudes/Nuevo/reportes.tpl');
        $this->load->javascript(STATIC_FILES . 'js/templates/solicitudes/solicitudes.js');
        $this->load->javascript(STATIC_FILES . 'template/plugins/datepicker/bootstrap-datepicker.js');
        $this->load->javascript(STATIC_FILES . 'template/plugins/datepicker/locales/bootstrap-datepicker.es.js');
        $this->load->javascript(STATIC_FILES . 'js/plugins/typeahead/js/bootstrap-typeahead.min.js');
    }

    public function anual()
    {   
        $tipo_reporte = 2;
        $primer_mes = 01;
        $ultimo_mes = 12;
        $primer = $this->_data_first_month_day_filtro($primer_mes);
        $ultimo = $this->_data_last_month_day_filtro($ultimo_mes);
        $reportes = $this->_DAOSolicitudes->Reportes($primer, $ultimo);
        $anio = date('Y');
        $this->smarty->assign("arrReportes", $reportes);
        $this->smarty->assign("anio",$anio);
        $this->smarty->assign("tipo_reporte",$tipo_reporte);
        $this->_display('Solicitudes/Nuevo/reportes.tpl');
        $this->load->javascript(STATIC_FILES . 'js/templates/solicitudes/solicitudes.js');
        $this->load->javascript(STATIC_FILES . 'template/plugins/datepicker/bootstrap-datepicker.js');
        $this->load->javascript(STATIC_FILES . 'template/plugins/datepicker/locales/bootstrap-datepicker.es.js');
        $this->load->javascript(STATIC_FILES . 'js/plugins/typeahead/js/bootstrap-typeahead.min.js');
    }

}
