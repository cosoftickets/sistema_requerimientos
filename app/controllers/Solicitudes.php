<?php

class Solicitudes extends Controller
{

    /**
     *
     * @var DAOUsuarios
     */
    protected $_DAOSolicitudes;
    protected $_DAOTareas;
    protected $campo_id = "id_ticket";

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->_DAOUsuarios    = $this->load->model("DAOUsuarios");
        $this->_DAOAdjuntos    = $this->load->model("DAOAdjuntos");
        $this->_DAOSolicitudes = $this->load->model('DAOSolicitudes');
        $this->_DAOHistorico   = $this->load->model('DAOHistorico');
        $this->_DAOTareas      = $this->load->model('DAOTareas');
        $this->smarty->addPluginsDir(APP_PATH . "views/templates/mantenedor_avanzados/grilla_historico_user/plugins/");
    }

    /**
     * Index
     */
    public function index()
    {
        die("nada que mostrar");
    }

    public function Nuevo(){

        if (isset($_SESSION['adjuntos'])) {
            unset($_SESSION['adjuntos']);
        }

        if (isset($_SESSION['tarea'])) {
            unset($_SESSION['tarea']);
        }

        if (isset($_SESSION['regularizar'])) {
            unset($_SESSION['regularizar']);
        }
        $DAOSolicitudes = $this->_DAOSolicitudes;

        $prioridad          = $DAOSolicitudes->getPrioridad();
        $lista_trabajadores = $DAOSolicitudes->getListaTrabajadores();
        $lista_proyectos    = $DAOSolicitudes->getListaProyectos();

        //Fechas
        $fecha_creacion = date("Y-m-d");

        //Variables de asignacion al template
        $this->smarty->assign('fecha_creacion_controller', $fecha_creacion);
        $this->smarty->assign('trabajadores', $lista_trabajadores);
        $this->smarty->assign('prioridad', $prioridad);
        $this->smarty->assign('lista_proyectos', $lista_proyectos);

        //llamado al template
        $this->_display('Solicitudes/Nuevo/nuevo.tpl');

        //Js asignados al template
        $this->load->javascript(STATIC_FILES . 'template/plugins/datepicker/bootstrap-datepicker.js');
        $this->load->javascript(STATIC_FILES . 'template/plugins/datepicker/locales/bootstrap-datepicker.es.js');
        $this->load->javascript(STATIC_FILES . 'js/plugins/typeahead/js/bootstrap-typeahead.min.js');
        $this->load->javascript(STATIC_FILES . 'js/templates/solicitudes/solicitudes.js');
        $this->load->javascript('$(".datepicker").datepicker();');
    }


    public function Asignados()
    {
        $session = New Zend_Session_Namespace("usuario_carpeta");

        $arr = $this->_DAOSolicitudes->getSolicitudesAsignadas($session->id, 0);
        $select_tareas = $this->_DAOSolicitudes->getTareasAsignadas($session->id, 0);
        //print_r(count($select_tareas));die();
        $fechaHoy = date("Y-m-d");

        $this->smarty->assign('arrResultado', $arr);
        $this->smarty->assign('fechaHoy', $fechaHoy );
        $this->smarty->assign('Fechas', $this->load->lib('Fechas', false));
        $this->_display('Solicitudes/Grillas/asignados.tpl');
        //$this->load->javascript(STATIC_FILES . 'js/templates/documento/documentos.js');

    }

    public function Asignados_editar()
    {
        if (isset($_SESSION['tarea'])) {
            unset($_SESSION['tarea']);
        }
        if (isset($_SESSION['adjuntos'])) {
            unset($_SESSION['adjuntos']);
        }
        $session = New Zend_Session_Namespace("usuario_carpeta"); 
        $this->smarty->assign('Fechas', $this->load->lib('Fechas', false));
        $parametros = $this->request->getParametros();
        $id_solicitud = $parametros[0];
        $solicitud = $this->_DAOSolicitudes->getSolicitudById($id_solicitud);
        if($_SESSION["perfil"] != 1){
            if($solicitud->id_estado ==1){
                $estadoUpdate["cd_id_estado"] = 2;
                $this->_DAOSolicitudes->update($estadoUpdate, $id_solicitud, $this->campo_id);
            }
        }
        $tareas    = $this->_DAOTareas->getTareasById($solicitud->id_solicitud);

        $fecha_entrega  = $solicitud->fecha_entrega;
        $fecha_creacion = $solicitud->fc_fecha_creacion;
        $fecha_termino  = $solicitud->fc_fecha_termino;
        $fc_fecha_entrega  = date("d/m/Y", strtotime($fecha_entrega));
        $fc_fecha_creacion = date("d/m/Y", strtotime($fecha_creacion));
        if($fecha_termino != "0000-00-00"){
            $fc_fecha_termino  = date("d/m/Y", strtotime($fecha_termino));
        }else{
            $fc_fecha_termino = "";
        }
        $cantidad_tareas = count($tareas);
        $fechaHoy = date("Y-m-d");
        $_DAOArchivos = $this->load->model('DAOArchivos');

        $adjuntos = $_DAOArchivos->getArchivosSolicitud($id_solicitud);

        $arr_adjuntos = array();
        if ($adjuntos) {
            $i = 0;
            foreach ($adjuntos as $item) {
                $_SESSION['adjuntos'][$i]['name'] = $item->gl_nombre_archivo;
                $_SESSION['adjuntos'][$i]['indice'] = $i;
                $_SESSION['adjuntos'][$i]['fecha'] = Fechas::formatearHtml($item->fc_fecha_archivo);
                $_SESSION['adjuntos'][$i]['usuario'] = $item->nombres . ' ' . $item->apellidos;
                $_SESSION['adjuntos'][$i]['usuario_id'] = $item->cd_usuario_fk_archivo;
                $_SESSION['adjuntos'][$i]['type'] = $item->gl_mime_archivo;
                $_SESSION['adjuntos'][$i]['sha'] = $item->gl_sha_archivo;
                $_SESSION['adjuntos'][$i]['contenido'] = base64_encode(file_get_contents($item->gl_ruta_archivo . '/' . $item->gl_nombre_archivo));
                $i++;
            }
            $arr_adjuntos = $_SESSION['adjuntos'];
        }

        $this->_addJavascript(STATIC_FILES.'js/templates/solicitudes/solicitudes.js');
        $this->smarty->assign("tareas_asignadas",$tareas);
        $this->smarty->assign("cantidad_tareas", $cantidad_tareas);
        $this->smarty->assign("solicitud", $solicitud);
        $this->smarty->assign("id_solicitud", $id_solicitud);
        $this->smarty->assign("fc_fecha_entrega",$fc_fecha_entrega);
        $this->smarty->assign("fc_fecha_creacion",$fc_fecha_creacion);
        $this->smarty->assign("fc_fecha_termino",$fc_fecha_termino);
        $this->smarty->assign("perfil", $_SESSION["perfil"]);
        $this->smarty->assign("adjuntos", $arr_adjuntos);
        $this->smarty->assign("estado_finalizar",$solicitud->id_estado);
        $this->_display('Solicitudes/Grillas/detalle_asignado.tpl');
        $this->load->javascript('parent.Solicitudes.cargarGrillaAdjuntos();');
        $this->load->javascript("parent.Solicitudes.barraProcessCheck(1,1,-1,".$cantidad_tareas.")");
        $this->load->javascript(STATIC_FILES . 'template/plugins/datepicker/bootstrap-datepicker.js');
        $this->load->javascript(STATIC_FILES . 'template/plugins/datepicker/locales/bootstrap-datepicker.es.js');
        $this->load->javascript(STATIC_FILES . 'js/plugins/typeahead/js/bootstrap-typeahead.min.js');
        $this->load->javascript('$(".datepicker").datepicker();');
        $this->load->javascript(STATIC_FILES . 'template/plugins/alertify/alertify.js');

    }

    public function ActualizarCamposSolicitudes(){
        $id_ticket = $_POST["idTicket"];
        $fecha_entrega = explode('/', $_POST["fechaEntrega"]);
        $comentario =  $_POST["comentario"];
        $detalle =  $_POST["detalle"];
        $horasEs =  $_POST["horasEstimadas"];
        $horasUt =  $_POST["horasUtilizadas"];
        $fecha_termino =  explode('/', $_POST["fechaFinalizar"]);

        $data = array();
        $data["id_ticket"] = $id_ticket;
        $data["fc_plazo"] = $fecha_entrega[2] . '-' . $fecha_entrega[1] . '-' . $fecha_entrega[0];
        $data["fc_fecha_termino"] = $fecha_termino[2] . '-' . $fecha_termino[1] . '-' . $fecha_termino[0];

        if($comentario != 1){
            $data["gl_comentario"] = $comentario;
            $data["gl_detalle_finalizado"] = $detalle;
            $data["nr_horas_estimadas"] = $horasEs;
            $data["nr_horas_utilizadas"] = $horasUt;
            
        }
        $update = $this->_DAOSolicitudes->update($data, $id_ticket, $this->campo_id);
        $json = array();
        if($update){
            $json["estado"] = true;
        }else{
            $json["estado"] = false;
        }

        echo json_encode($json);
    }

    public function Todo()
    {
        $this->load->lib('Fechas', false);
        $this->load->lib('Constantes', false);

        if ($_SESSION['perfil'] == 1) {
            $arr = $this->_DAODocumento->getDocumentosAsignados();
        } else {
            $session = New Zend_Session_Namespace("usuario_carpeta");
            $arr = $this->_DAODocumento->getDocumentosAsignados($session->id);
        }

        $tmp_arr = array();

        foreach ($arr as $item) {
            if ($item->cd_estado_solicitud == 0) {
                $item->dias_bandeja = Fechas::diffDias(date('Y-m-d'), $item->fc_fecha_ingreso_partes_solicitud, true) - $item->total_dias_feriados;
            } else {
                $evento = $this->_DAOHistorial->obtHistorialVisacionDocumento($item->id_solicitud, Constantes::DOCUMENTO_APROBADO, Constantes::DOCUMENTO_RECHAZADO);
                if ($evento) {
                    $item->dias_bandeja = Fechas::diffDias($evento->fc_fecha_historial, $item->fc_fecha_ingreso_partes_solicitud, true) - $item->total_dias_feriados;
                    $item->fecha_visacion = Fechas::formatearHtml($evento->fc_fecha_historial);
                } else {
                    $item->dias_bandeja = 'Sin fechas';
                    $item->fecha_visacion = 'Sin fecha';
                }
            }
            $tmp_arr[] = $item;
        }

        $this->smarty->assign('arrResultado', $tmp_arr);
        $this->smarty->assign('Fechas', $this->load->lib('Fechas', false));
        $this->_display('Documentos/Grillas/Todo.tpl');
        $this->load->javascript(STATIC_FILES . 'js/templates/documento/documentos.js');
    }


    public function Revision()
    {

        $arr = array();
        $documentos = $this->_DAODocumento->getDocumentosRevision(1);
        if ($documentos->numRows > 0) {
            $arr = $documentos->rows;
        }

        $this->smarty->assign('arrResultado', $arr);
        $this->smarty->assign('Fechas', $this->load->lib('Fechas', false));
        $this->_display('Documentos/Grillas/Revision.tpl');
        $this->load->javascript(STATIC_FILES . 'js/templates/documento/documentos.js');

    }


    public function regularizarIngreso()
    {

        /*$arr = array();
        $documentos = $this->_DAODocumento->getDocumentosRevision(1);
        if ($documentos->numRows > 0) {
            $arr = $documentos->rows;
        }*/

        //$this->smarty->assign('arrResultado', $arr);
        //$this->smarty->assign('Fechas', $this->load->lib('Fechas', false));
        $this->smarty->display('Solicitudes/Grillas/regularizarIngreso.tpl');

        //$this->load->javascript(STATIC_FILES . 'js/templates/documento/documentos.js');

    }

    /**
     * Resultados de búsqueda
     */
    public function buscar()
    {
        header('Content-type: application/json');

        $limit = array("comienzo" => $this->_request->getParam("page") - 1,
            "resultados" => 20);

        $lista = $this->_DAOUsuarios->listarBusqueda($this->_request->getParams(), $limit);
        $resultados = array();
        $cantidad = 0;
        if (!is_null($lista)) {
            foreach ($lista as $usuario) {
                $imagen = "images/personas/" . substr($usuario->rut, 0, -2) . ".jpg";
                if (file_exists("static/" . $imagen)) {
                    $imagen = "/images/personas/" . substr($usuario->rut, 0, -2) . ".jpg";
                } else {
                    $imagen = "/images/no-image.png";
                }
                $resultados["result"][] = array("image" => STATIC_FILES . $imagen,
                    "width" => "150",
                    "id" => $usuario->id,
                    "rut" => $usuario->rut,
                    "nombre" => $usuario->nombres . " " . $usuario->apellidos);
                $cantidad++;
            }
        }
        $resultados["total"] = $cantidad;

        echo Zend_Json_Encoder::encode($resultados);
    }

    public function detalleInstalacion()
    {

        $parametros = $this->request->getParametros();
        $idInstalacion = $parametros[0];

        //$arrSipresaId = $this->_DAOInstalacion->getDetalleInstalacion($idInstalacion)	;
        $arrSipresaId = $this->_DAODatosRemoto->getOrigenesRemotos("sipresa_id", "?id=" . $idInstalacion, false);
        $arrSumanet = $this->_DAODatosRemoto->getOrigenesRemotos("sumanet_3_sumarios", "?id=" . $idInstalacion, false);
        //$arrSumanet 	= $this->_DAODatosRemoto->getOrigenesRemotos("sumanet_3_sumarios","?id=12903",false)	;


        $arrSumanet = json_decode($arrSumanet);
        $arrSipresaId = json_decode($arrSipresaId);

        $resoluciones = array();


        foreach ($arrSipresaId->ambitos as $item) {
            $resoluciones[$item->gl_resolucion] = $item;
        }

        $arrSipresaId->arrResoluciones = $resoluciones;


        //print_r($arrSipresaId->resoluciones);

        //print_r($arrSipresaId->resoluciones);

        $e = str_replace(",", ".", $arrSipresaId->datos_generales->ins_c_coordenada_e);
        $n = str_replace(",", ".", $arrSipresaId->datos_generales->ins_c_coordenada_n);

        //Convertir coordenadas de UTM a LatLon
        $arrLatLong = ToLL($n, $e, 19);

        //Setear nuevas coordenadas	en datos nuevos
        $arrSipresaId->datos_generales->lat = $arrLatLong['lat'];
        $arrSipresaId->datos_generales->lon = $arrLatLong['lon'];

        $arrAmbitosResumen = array();
        foreach ($arrSipresaId->ambitos as $itm) {
            if (isset($arrAmbitosResumen[$itm->id_ambito])) {
                $arrAmbitosResumen[$itm->id_ambito] = $arrAmbitosResumen[$itm->id_ambito] + 1;
            } else {
                $arrAmbitosResumen[$itm->id_ambito] = 1;
            }
        }

        //print_r($arrAmbitosResumen);

        //print_r($arrSipresaId->datos_generales);

        /*
        if($idInstalacion == 207314){

            $arrSipresaId->datos_generales->gl_resolucion = 160540661;
        }

        if($idInstalacion == 206864){
            $arrSipresaId->datos_generales->gl_resolucion = 160550327;
        }
        */
        //echo "::".$arrSipresaId->datos_generales->gl_resolucion."::";


        //print_r($arrSipresaId->resoluciones);


        foreach ($arrSipresaId->resoluciones as $itemResolucion) {
            $strASD = $this->_DAODatosRemoto->getOrigenesRemotos("asd_antecedentes", "?gl_codigo=" . $itemResolucion->resolucion, false);
            $arrASD[$itemResolucion->resolucion] = json_decode($strASD);

            //$this->_DAOAdjuntos->exportarDesdeASD($idInstalacion,$arrASD);

        }


        //if(trim($arrSipresaId->datos_generales->gl_resolucion) != ""){


        //}

        //print_r($arrASD);
        //die();

        $arrAdjuntos = $this->_DAOAdjuntos->getAdjuntosInstalacion($idInstalacion);


        $arrAdjuntosTipo = array();
        foreach ($this->_DAOAdjuntos->getAdjuntosInstalacion($idInstalacion) as $itemAdjunto) {
            $arrAdjuntosTipo[$itemAdjunto->id_ambito][$itemAdjunto->id_tipo][] = $itemAdjunto;
        }


        $arrAdjuntosResumen = array();
        foreach ($arrAdjuntos as $itm) {
            if (isset($arrAdjuntosResumen[$itm->id_tipo])) {
                $arrAdjuntosResumen[$itm->id_tipo] = $arrAdjuntosResumen[$itm->id_tipo] + 1;
            } else {
                $arrAdjuntosResumen[$itm->id_tipo] = 1;
            }
        }

        foreach ($arrSipresaId->ambitos as $item) {

            $arrAmbitos[$item->id_ambito] = $item;

        }

        //Obtener adjuntos desde ASD


        //print_r($arrSipresaId->resoluciones);

        //print_r($arrSipresaId->arrResoluciones);

        //Llenar los arreglos de datos para el template
        $this->smarty->assign('idInstalacion', $idInstalacion);
        $this->smarty->assign('arrSipresa', $arrSipresaId->datos_generales);
        $this->smarty->assign('arrAdjuntos', $this->_DAOAdjuntos->getAdjuntosInstalacion($idInstalacion));
        $this->smarty->assign('arrAdjuntosASD', $arrASD);
        $this->smarty->assign('arrAdjuntosResumen', $arrAdjuntosResumen);
        $this->smarty->assign("arrAdjuntosTipo", $arrAdjuntosTipo);
        $this->smarty->assign('arrTipos', $this->_DAOAdjuntos->getTipos());
        $this->smarty->assign('arrAmbitos', $arrAmbitos);
        $this->smarty->assign('arrAmbitosBD', $this->_DAOAmbitos->getAmbitos());
        $this->smarty->assign('arrActividades', $arrSipresaId->actividades);
        $this->smarty->assign('arrCamposTipo', $this->_DAOAdjuntos->getTiposCampos());
        $this->smarty->assign('arrAmbitosResumen', $arrAmbitosResumen);
        $this->smarty->assign('arrSumarios', $arrSumanet->expedientes);
        $this->smarty->assign('arrSumariosArchivos', $arrSumanet->adjuntos);
        $this->smarty->assign('arrResoluciones', $arrSipresaId->arrResoluciones);

        $this->smarty->display('instalacion/detalleInstalacion.tpl');

    }

    //creada por BC
    public function guardarNuevaSolicitud(){  
        $session = New Zend_Session_Namespace("usuario_carpeta");
        $data = array();
        parse_str($_POST['data'], $data);
        $this->load->lib('Constantes', false);
        $json = array();
        $regularizar = 0;
        $datos = $data;

        if(isset($datos['fc_fecha_creacion_reg']) && $datos['fc_fecha_creacion_reg'] !=""){ // Validador campo no seteado
            $fecha_creacion_reg = explode('/', $datos['fc_fecha_creacion_reg']);
            $fecha_creacion_estado = true;
        }else{
            $fecha_creacion_estado = false;
        }

        if($fecha_creacion_estado == true){      
            $datos["fc_fecha_creacion"] = $fecha_creacion_reg[2] . '-' . $fecha_creacion_reg[0] . '-' . $fecha_creacion_reg[1];
        }else{
            $datos['fc_fecha_creacion'] = date('Y-m-d H:i:s');
        }

        if(isset($datos['fc_fecha_termino_reg']) && $datos['fc_fecha_termino_reg'] !=""){ // Validador campo no seteado
            $fecha_termino_reg = explode('/', $datos['fc_fecha_termino_reg']);
            $fecha_termino_estado = true;
        }else{
            $fecha_termino_estado = false;
        }

        if($fecha_termino_estado == true){
            $datos["fc_fecha_termino"] = $fecha_termino_reg[2] . '-' . $fecha_termino_reg[0] . '-' . $fecha_termino_reg[1];
        }

        $fecha_entrega = explode('/', $datos['fc_fecha_entrega']);   
        $datos['fc_fecha_entrega']  = $fecha_entrega[2] . '-' . $fecha_entrega[1] . '-' . $fecha_entrega[0];

        if(!isset($datos['gl_detalle_finalizado']) or $datos['gl_detalle_finalizado'] == null){
            $datos['gl_detalle_finalizado'] = '';
        }else{
            $regularizar = 1;
        }
        if(!isset($datos['nr_horas_estimadas']) or $datos['nr_horas_estimadas'] == null){
            $datos['nr_horas_estimadas'] = '';
        }
        if(!isset($datos['nr_horas_utilizadas']) or $datos['gl_detalle_finalizado'] == null){
            $datos['nr_horas_utilizadas'] = '';
        }

        //Diferencia de dia creacion con la de entrega
        $creacion = date_create($datos['fc_fecha_creacion']);
        $entrega = date_create($datos['fc_fecha_entrega']);
        $diferencia = date_diff($creacion, $entrega);
        $insertar    = $this->_DAOSolicitudes->insSolicitud($datos);

        if($insertar){  
            $id_solicitud = $insertar; 
            if($regularizar == 1){
                $data_estado["cd_id_estado"] = 7;
                $this->_DAOSolicitudes->update($data_estado,$id_solicitud, $this->campo_id);
            }
             if (isset($_SESSION['tarea']) and count($_SESSION['tarea'])) {
                    foreach ($_SESSION['tarea'] as $item) {
                        $dataT = array(
                            'gl_nombre_tarea' => $item['nombre'],
                            'gl_descripcion_tarea' => "",
                            'nr_estado' => 0,
                            'cd_id_ticket' => $insertar
                        );
                            $guardar = $this->_DAOTareas->insTarea($dataT);
                    }
             }
            if (isset($_SESSION['adjuntos']) and count($_SESSION['adjuntos'])) {
                $_DAOArchivos = $this->load->model('DAOArchivos');
                $ruta = 'solicitudes/' . $insertar;

                    if (!is_dir($ruta)) {
                        mkdir($ruta, 0777, true);
                    }

                    foreach ($_SESSION['adjuntos'] as $item) {
                        $data = array(
                            'solicitud' => $insertar,
                            'nombre' => $item['name'],
                            'ruta' => $ruta,
                            'sha' => $item['sha'],
                            'mime' => $item['type'],
                            'usuario_id' => $session->id,
                            'fecha' => $datos['fc_fecha_creacion'],
                            'comentario' => $item['gl_comentario_adjunto']
                        );

                        $adjunto = fopen($ruta . '/' . $item['name'], 'w+b');
                        fwrite($adjunto, base64_decode($item['contenido']));
                        fclose($adjunto);

                        if (is_file($ruta . '/' . $item['name']) and is_readable($ruta . '/' . $item['name'])) {
                            $guardar = $_DAOArchivos->insArchivo($data);
                        }

                    }   
            }

            $this->guardarHistorico($_POST["data"], $id_solicitud, true);

            if(!isset($datos['gl_detalle_finalizado']) or $datos['gl_detalle_finalizado'] ==''){
            }else{
                $evento ="El ticket se ha Finalizado";
                $estado = 7;
                $idUsuario = $session->id;
                $id_tarea = null;
                $this->procesarGuardarHistorico($id_solicitud, $idUsuario, $estado, $evento, $id_tarea);
             }
            $json['estado'] = true;
            $json['mensaje'] = 'Tikcet N°' . $id_solicitud . ' ingresado<br> Quedan '.$diferencia->days.' días para la finalización del proyecto';
        }

        echo json_encode($json);
    }

    //creada por BC
    public function guardarNuevoAdjunto(){
        $id_ticket=$_POST['id_ticket'];
        $archivos = $_FILES['archivo'];
        $comentario=$_POST['comentario_adjunto'];
        $archivos['contenido'] = base64_encode(file_get_contents($archivos['tmp_name']));

        $session = New Zend_Session_Namespace("usuario_carpeta");
        $data = array();
        //parse_str($_POST['data'], $data);
        $this->load->lib('Constantes', false);
        $json = array();
        $regularizar = 0;
        $datos = $data;
        $insertar=$id_ticket;
        if($insertar){
            $_DAOArchivos = $this->load->model('DAOArchivos');
            $ruta = 'solicitudes/' . $insertar;

            if (!is_dir($ruta)) {
                mkdir($ruta, 0777, true);
            }

            $data = array(
                'solicitud' => $insertar,
                'nombre' => $archivos['name'],
                'ruta' => $ruta,
                'sha' =>  sha1($archivos['name'] . uniqid()),
                'mime' => $archivos['type'],
                'usuario_id' => $session->id,
                'fecha' => date('y-m-d- H:i:s'),
                'comentario' => $comentario
            );
            $adjunto = fopen($ruta . '/' . $archivos['name'], 'w+b');
            fwrite($adjunto, base64_decode($archivos['contenido']));
            fclose($adjunto);

            if (is_file($ruta . '/' .$archivos['name']) and is_readable($ruta . '/' . $archivos['name'])) {
                $guardar = $_DAOArchivos->insArchivo($data);
            }

            $json['estado'] = true;
            $json['mensaje'] = 'exito';
        }
       
        //$modal="xModal.open('$smarty.const.BASE_URI/DetalleSolicitud/detalleSolicitud/62','Bitácora ticket número 62 ',85)";
        //echo json_encode($json);
        //header('Location:http://localhost/definitivo2/sistema_requerimientos/index.php/DetalleSolicitud/detalleSolicitud/'.$insertar);
        header('Location:'.BASE_URI.'/MantenedorSolicitudes');
        //header('Location:'.$modal);
    }

    public function guardarHistorico($data, $id_solicitud, $bool){
        $session = New Zend_Session_Namespace("usuario_carpeta");
        $dataHistorico = array();
        if($bool == true){
            parse_str($data, $dataHistorico);
            $datosHistorico = $dataHistorico;
            $datosHistorico['cd_id_usuario'] = $session->id;  
            $datosHistorico['cd_id_tarea'] = null;  
            $datosHistorico['fc_fecha_creacion'] = date('Y-m-d H:i:s');
            if(isset($_SESSION["regularizar"])){
                $datosHistorico['gl_evento_historial'] = "Se ha regularizado una nueva Solicitud";
            }
        }else{
            $datosHistorico = $data;
        }
        $datosHistorico['cd_id_solicitud'] = $id_solicitud;
            
        $insertarHistorico = $this->_DAOHistorico->insHistorico($datosHistorico);
    }


   public function revisarSolicitud()
   {

       $session = New Zend_Session_Namespace("usuario_carpeta");

       if (isset($_SESSION['adjuntos']))
           unset($_SESSION['adjuntos']);

       $params = $this->request->getParametros();
       $id_solicitud = $params[0];
       $fechaHoy = date("Y-m-d");
       $solicitud = $this->_DAOSolicitudes->getSolicitudById($id_solicitud);

       if($solicitud->id_estado == 1){
           $estado = 2;
           $evento = "La solicitud esta en Revisión";
           $id_usuario = $solicitud->id_usuario;
           $id_tarea = 0;
           $this->procesarGuardarHistorico($id_solicitud, $id_usuario, $estado, $evento,$id_tarea);
       }

       if (isset($_SESSION['adjuntos']))
            unset($_SESSION['adjuntos']);


        $params = $this->request->getParametros();
        $id_solicitud = $params[0];

        $solicitud = $this->_DAODocumento->getDocumentoPorId($id_solicitud);
        $_DAOArchivos = $this->load->model('DAOArchivos');

        $adjuntos = $_DAOArchivos->getArchivosSolicitud($id_solicitud);
        $this->load->lib('Fechas', false);

       $this->smarty->assign('Fechas', $this->load->lib('Fechas', false));
       $this->smarty->assign("solicitud", $solicitud);
       $this->smarty->assign("fechaHoy",$fechaHoy);
        $template = $this->smarty->fetch('Solicitudes/Nuevo/revisar.tpl');

       
       //$this->smarty->display('Solicitudes/Nuevo/revisar.tpl');

       $this->load->javascript(STATIC_FILES . 'js/templates/solicitudes/solicitudes.js');
       echo $template;
   }


    public function iniciarTicket()
    {
        $id_solicitud = $_POST['solicitud']; 
        $json = array();        
        $data = $this->_DAOSolicitudes->getSolicitudById($id_solicitud);
        $estado = 3;
        $id_usuario = $data->id_usuario;
        $id_tarea =0;
        $this->procesarGuardarHistorico($id_solicitud, $id_usuario, $estado, $id_tarea);
        $json['estado'] = true;
        $json['mensaje'] = 'Ticket #' . $id_solicitud . ' En Desarollo';
        echo json_encode($json);
    }

    public function agregarMensaje($mensaje = null, $visador = null){
        $params = $this->request->getParametros();
        $mensaje = '<div class="alert alert-success text-center">Ticket Finalizado con éxito!</div>';
        $this->smarty->assign("mensaje",$mensaje);
        $this->smarty->display('Solicitudes/Grillas/mensajeSuccess.tpl');
    }

    public function finalizarTicket()
   {
      
       $params = $this->request->getParametros();
       $id_solicitud = $params[0];
       $json = array();
       $data = $this->_DAOSolicitudes->getSolicitudById($id_solicitud);
       $arrDifFecha = array();
       $fechaHoy = date("Y-m-d");
       $termino = date_create($fechaHoy);
       $entrega = date_create($data->fecha_entrega);
       $diferencia = date_diff($termino, $entrega);
       $arrDifFecha["fc_fecha_termino"] = $fechaHoy;
       if($diferencia->invert == 1){
           $arrDifFecha["nr_fecha_diferencia"] = ($diferencia->days * -1);
       }else{
           $arrDifFecha["nr_fecha_diferencia"] = $diferencia->days;
       }
       
       //var_dump($diferencia);
       //$data['nr_fecha_diferencia'] = 4;
       $estado = 7;
       $evento = "Se ha Finalizado el Ticket";
       $id_usuario = $data->id_usuario;
       $id_tarea = null;
       $this->procesarGuardarHistorico($id_solicitud, $id_usuario, $estado, $evento, $id_tarea);
       $this->_DAOSolicitudes->update($arrDifFecha, $id_solicitud, $this->campo_id);
       $this->agregarMensaje();
   }

   

    public function procesarGuardarHistorico($id_solicitud, $id_usuario, $estado, $evento, $id_tarea){
       $fechaHoy = date("Y-m-d H:i:s");
       $dataTicket = array();
       $dataHistorico = array();
       $dataTicket["cd_id_estado"] = $estado;
       $dataHistorico["cd_id_estado"] = $estado;
       $dataHistorico["cd_id_usuario"] = $id_usuario;
       $dataHistorico["fc_fecha_creacion"] = $fechaHoy;
       $dataHistorico["gl_evento_historial"] = $evento;
       $dataHistorico["cd_id_tarea"] = $id_tarea;
       $this->_DAOSolicitudes->update($dataTicket, $id_solicitud, $this->campo_id);
       $this->guardarHistorico($dataHistorico, $id_solicitud, false);
   }


    public function guardarRechazo()
    {
        $comentario = $_POST['comentario'];
        $solicitud = $_POST['solicitud'];

        $this->load->lib('Fechas', false);
        $this->load->lib('Email', false);
        $this->load->lib('Constantes', false);

        $json = array();
        $parametros = array(
            'cd_estado_solicitud' => 2, /* estado rechazado */
            'gl_comentario_rechazo' => nl2br($comentario),
            'fc_fecha_cambio_estado' => date('Y-m-d H:i:s')
        );
        if ($this->_DAODocumento->updDocumento($solicitud, $parametros)) {
            $session = New Zend_Session_Namespace("usuario_carpeta");
            $json['estado'] = true;
            $json['mensaje'] = 'Documento #' . $solicitud . ' rechazado';

            $solicitud = $this->_DAODocumento->getDocumentoPorId($solicitud);

            $arr_historial = array(
                'fecha' => date('Y-m-d H:i:s'),
                'usuario' => $session->id,
                'solicitud' => $solicitud->id_solicitud,
                'evento' => Constantes::DOCUMENTO_RECHAZADO
            );
            $historial = $this->_DAOHistorial->insHistorial($arr_historial);

            /* correo a asantander@minsal.cl */
            $msg = '<h3>Documento RECHAZADO</h3>';
            $msg .= 'El documento que se detalla a continuación ha sido visado por: <strong>' . $session->usuario . '</strong><br/>';
            $msg .= 'Factura : ' . $solicitud->nr_numero_documento_solicitud . '<br/>';
            $msg .= 'Nombre Proveedor : ' . $solicitud->gl_nombre_emisor_solicitud . '<br/>';
            $msg .= 'Monto: $' . number_format($solicitud->nr_monto_solicitud,0,',','.') . '<br/>';
            $msg .= 'Correlativo : ' . $solicitud->id_solicitud.'<br/>';
            $msg .= 'Comentario rechazo : ' . $solicitud->gl_comentario_rechazo . '<br/>';
            $msg .= 'Fecha rechazo : ' . Fechas::formatearHtml($solicitud->fc_fecha_cambio_estado);
            $msg .= 'Para ver el detalle completo de esta visación y proceder a la devolución del documento, por favor conectarse al módulo de Facturación. Para acceder a la plataforma pinche el siguiente link: ';
            $msg .= '<a href="' . HOST . '/finanzas/" target="_blank">' . HOST . '/finanzas</a>';
            $msg .= '<p>Finanzas - '.date('d/m/Y').'</p>';

            $environment = '';
            if(ENVIROMENT != 'PROD')
                $environment = ENVIROMENT;

            $destinatario = 'asantander@minsal.cl';
            $remitente = 'Minsal - Finanzas';
            $nombre_remitente = 'Minsal - Finanzas';
            $asunto = 'Finanzas - Documento Rechazado '.$environment;
            $mensaje = $msg;
            Email::sendEmail($destinatario, $remitente, $nombre_remitente, $asunto, $mensaje);

        } else {
            $json['estado'] = false;
            $json['mensaje'] = 'Problemas al rechazar documento. Intente nuevamente';
        }

        echo json_encode($json);
    }


    public function getEmisores()
    {
        $rut = $_POST['search'];

        $_DAOProveedores = $this->load->model('DAOProveedores');

        $listado = $_DAOProveedores->getEmisorRut($rut);
        $json = array();
        $i = 0;
        foreach ($listado as $item) {
            $json['listado'][$i]['id'] = $item->id_proveedor;
            $json['listado'][$i]['rut'] = trim($item->gl_rut_proveedor);
            $json['listado'][$i]['nombre'] = trim($item->gl_nombre_proveedor);
            $i++;
        }

        echo json_encode($json);
    }


    public function grillaAsignados()
    {
        $session = New Zend_Session_Namespace("usuario_carpeta");

        $arr = $this->_DAOSolicitudes->getSolicitudesAsignadas($session->id, 0);
        $fechaHoy = date("Y-m-d");

        $this->smarty->assign('arrResultado', $arr);
        $this->smarty->assign("fechaHoy",$fechaHoy);
        $this->smarty->assign('Fechas', $this->load->lib('Fechas', false));
        $template = $this->smarty->fetch('Solicitudes/Grillas/grilla_asignados.tpl');

        echo $template;
    }


    public function cargarVisadores()
    {
        $centro = $_POST['centro'];

        $_DAOVisadores = $this->load->model('DAOVisadores');
        $visadores = $_DAOVisadores->getVisadoresPorCentro($centro);

        $json = array();
        if ($visadores->numRows > 0) {
            $i = 0;
            $visadores = $visadores->rows;
            foreach ($visadores as $item) {
                $json[$i]['id'] = $item->id;
                $json[$i]['nombre'] = $item->nombres . " " . $item->apellidos;
                $i++;
            }
        }

        echo json_encode($json);

    }


    public function adjuntarArchivo($mensaje = null, $visador = null){
        $params = $this->request->getParametros();
        if (isset($params[0]) and $params[0] > 0) {
            $this->smarty->assign('visador', $params[0]);
            $this->smarty->display('Solicitudes/Nuevo/adjuntar_archivo_visador.tpl');
        } else {
            if (!is_null($mensaje)) {
                $this->smarty->assign('mensaje', $mensaje);
            }
            if (!is_null($visador)) {
                $this->smarty->assign('visador', $visador);
                $this->smarty->display('Solicitudes/Nuevo/adjuntar_archivo_visador.tpl');
            } else {
                $this->smarty->display('Solicitudes/Nuevo/adjuntar_archivo.tpl');
            }

        }

    }

    //BC
    public function adjuntarArchivoBitacora($mensaje = null, $visador = null){

        $params = $this->request->getParametros();
        if (isset($params[0]) and $params[0] > 0) {
            $this->smarty->assign('visador', $params[0]);
            $this->smarty->display('Solicitudes/Nuevo/adjuntar_archivo_visador.tpl');
        } else {
            if (!is_null($mensaje)) {
                $this->smarty->assign('mensaje', $mensaje);
            }
            if (!is_null($visador)) {
                $this->smarty->assign('visador', $visador);
                $this->smarty->display('Solicitudes/Nuevo/adjuntar_archivo_visador.tpl');
            } else {
                $this->smarty->display('Solicitudes/Nuevo/adjunto_bitacora.tpl');
            }

        }
        $this->load->javascript(STATIC_FILES . 'js/templates/solicitudes/adjuntos.js');
    }

    public function subirArchivo()
    {
        $archivo = $_FILES['archivo'];
        $error = false;
        $archivo['contenido'] = base64_encode(file_get_contents($archivo['tmp_name']));
        if ($archivo['error'] > 0) {
            $error = true;
            $mensaje = '<div class="alert alert-danger text-center">Error al subir archivo</div>';
        }

        if ($archivo['size'] == 0) {
            $error = true;
            $mensaje = '<div class="alert alert-danger text-center">Archivo con peso 0</div>';
        }

        if (empty($archivo['contenido'])) {
            $error = true;
            $mensaje = '<div class="alert alert-danger text-center">Archivo sin contenido</div>';
        }


        if (!$this->validarArchivo($archivo['type'])) {
            $error = true;
            $mensaje = '<div class="alert alert-danger text-center">Tipo de Archivo no permitido</div>';
        }

        if ($error) {
            $this->adjuntarArchivo($mensaje);
        } else {
            $this->load->lib('Fechas', false);
            $session = New Zend_Session_Namespace("usuario_carpeta");

            $archivo['usuario'] = $session->usuario;
            $archivo['usuario_id'] = $session->id;
            $archivo['gl_comentario_adjunto'] = $_POST["gl_comentario_adjunto"];
            $archivo['fecha'] = date('d/m/Y H:i:s');
            $archivo['sha'] = sha1($archivo['name'] . uniqid());
            $_SESSION['adjuntos'][] = $archivo;
            $mensaje = '<div class="alert alert-success text-center">Archivo adjuntado</div>';

            if (isset($_POST['visador'])) {
                $_DAOArchivos = $this->load->model('DAOArchivos');
               
                $this->adjuntarArchivo($mensaje, $_POST['visador']);
                $this->load->javascript('parent.parent.Solicitudes.cargarGrillaAdjuntos("revisar",' . $_POST['visador'] . ');');
            } else {
                $this->adjuntarArchivo($mensaje);
                $this->load->javascript('parent.Solicitudes.cargarGrillaAdjuntos();');
            }
        }


    }
    public function agregarTarea($mensaje = null, $visador = null)
    {
        $params = $this->request->getParametros();
        if (isset($params[0]) and $params[0] > 0) {
            $this->smarty->assign('visador', $params[0]);
            $this->smarty->display('Solicitudes/Nuevo/adjuntar_archivo_visador.tpl');
        } else {
            if (!is_null($mensaje)) {
                $this->smarty->assign('mensaje', $mensaje);
            }
            if (!is_null($visador)) {
                $this->smarty->assign('visador', $visador);
                $this->smarty->display('Solicitudes/Nuevo/adjuntar_archivo_visador.tpl');
            } else {
                $this->smarty->display('Solicitudes/Nuevo/agregar_tarea.tpl');
            }

        }

    }

    public function agregarDatos($mensaje = null, $visador = null)
    {


        $params = $this->request->getParametros();
        if (isset($params[0]) and $params[0] > 0) {
            $this->smarty->assign('visador', $params[0]);
            $this->smarty->display('Solicitudes/Nuevo/adjuntar_archivo_visador.tpl');
        } else {
            if (!is_null($mensaje)) {
                $this->smarty->assign('mensaje', $mensaje);
            }
            if (!is_null($visador)) {
                $this->smarty->assign('visador', $visador);
                $this->smarty->display('Solicitudes/Nuevo/adjuntar_archivo_visador.tpl');
            } else {
                $this->smarty->display('Solicitudes/Grillas/regularizarIngreso.tpl');
            }

        }

    }

    public function cargarDatosRegularizacion(){
        $i=0;
        $arr = array();
        foreach($_SESSION['regularizar'] as $itm):
            $arr[$i]["gl_detalle_finalizado"] = $itm["gl_detalle_finalizado"];
            $arr[$i]["nr_horas_utilizadas"] = $itm["nr_horas_utilizadas"];
            $arr[$i]["fc_fecha_creacion_reg"] = $itm["fc_fecha_creacion_reg"];
            $arr[$i]["fc_fecha_termino_reg"] = $itm["fc_fecha_termino_reg"];
        endforeach;
        $this->smarty->assign('datos', $arr);
        $template = $this->smarty->fetch('Solicitudes/Grillas/grilla_regularizados.tpl');
        echo $template;
    }

    public function mostrarTarea(){
        $tarea = $_POST["tarea"];
        $tareas = array();
        $tareas["nombre"] = $_POST["tarea"];
        $session = New Zend_Session_Namespace("usuario_carpeta");
        $_SESSION['tarea_nombre'] = $tarea;
        $_SESSION['tarea'][] = $tareas;
        $mensaje = '';
        $this->agregarTarea($mensaje);
        $this->load->javascript('parent.Solicitudes.cargarGrillaTareas();');
    }


    public function barraProcessCheck(){
       
       if($_POST["id_tarea"] !=-1){
           $session = New Zend_Session_Namespace("usuario_carpeta");
           $id_tarea = $_POST["id_tarea"];

           $id_usuario = $session->id;
           $campo_id = "id_tarea";
           $id_solicitud = $_POST["idProyecto"];
           $estadoCheck  = $this->_DAOTareas->revisarEstadoCheck($id_tarea);
           $arrEstado = array();

           if($estadoCheck){
               if($estadoCheck->nr_estado == 0){
                   $arrEstado["nr_estado"] = 1;
                   $this->_DAOTareas->update($arrEstado, $id_tarea, $campo_id);
               }
               if($estadoCheck->nr_estado == 1){
                   $arrEstado["nr_estado"] = 0;
                   $this->_DAOTareas->update($arrEstado, $id_tarea, $campo_id);
               }
               if($estadoCheck->nr_estado == 2){
                   $arrEstado["nr_estado"] = 2;
                   $this->_DAOTareas->update($arrEstado, $id_tarea, $campo_id);
               }
            }
           
           $tareas_check = $this->_DAOTareas->getCheckById($id_solicitud);  

               if($tareas_check[0]->sumaCheck == $tareas_check[0]->cantCheck){
                   $estado = 4;
                   $evento = "La solicitud se ha Completado";
                   $id_tarea = null;
                   $this->procesarGuardarHistorico($id_solicitud, $id_usuario, $estado, $evento, $id_tarea);
               }
               if($tareas_check[0]->sumaCheck == 0){
                   $estado = 2;
                   $evento = "La solicitud esta en Revisión";
                   $id_tarea = null;
                   $this->procesarGuardarHistorico($id_solicitud, $id_usuario, $estado, $evento, $id_tarea);
               }
               if($tareas_check[0]->sumaCheck > 0 && $tareas_check[0]->sumaCheck < $tareas_check[0]->cantCheck){ 
                   $estado = 3;
                   $evento = "Se ha terminado esta Tarea";

                   $this->procesarGuardarHistorico($id_solicitud, $id_usuario, $estado, $evento, $id_tarea);
               }
       }
       $template = $this->smarty->fetch('Solicitudes/Nuevo/barra_procesCheck.tpl');
       echo $template;
   }

     public function cargarGrillaTareas(){
        $session    = New Zend_Session_Namespace("usuario_carpeta");
        $id_usuario = $session->id;
        $evento     = "Se Crea una nueva tarea";
        $i=1;
        $arr = array();
        if(isset($_POST["idProyecto"])){
            $id_solicitud = $_POST["idProyecto"];
            $data = array(
                        'gl_nombre_tarea' => $_SESSION["tarea_nombre"],
                        'gl_descripcion_tarea' => "",
                        'nr_estado' => 0, // Este campo hay q verlooo
                        'cd_id_ticket' => $id_solicitud,
            );

            $guardar = $this->_DAOTareas->insTarea($data);
            $id_tarea = $guardar;
            $estado_ticket = $this->_DAOSolicitudes->getSolicitudById($id_solicitud);
            $estado_ticket_actual = $estado_ticket->id_estado;
            $this->procesarGuardarHistorico($id_solicitud, $id_usuario, $estado_ticket_actual, $evento, $id_tarea);
            $select_tareas = $this->_DAOTareas->getTareasById($id_solicitud);

            foreach($select_tareas as $itm):
                 $arr[$i]["id_tarea"] = $itm->id_tarea;
                 $arr[$i]["gl_nombre_tarea"] = $itm->gl_nombre_tarea;
                 $arr[$i]["nr_estado"] = $itm->nr_estado;
                 $arr[$i]["cd_id_ticket"] = $itm->cd_id_ticket;
                 $i++;
            endforeach;

            $cantidad_tareas = count($select_tareas);
            $this->smarty->assign('cantidad_tareas', $cantidad_tareas);
            $this->smarty->assign('id_solicitud', $id_solicitud);
            $nuevoEditar = 1;
        }else{
            $i=0;
            foreach($_SESSION["tarea"] as $itm):
                $arr[$i]["id_tarea"] = $i;
                $arr[$i]["gl_nombre_tarea"] = $itm["nombre"];
                $arr[$i]["nr_estado"] = 0;
                $i++;
            endforeach;
            $nuevoEditar = 0;
            $this->smarty->assign('id_solicitud', "0");
        }

        $this->smarty->assign('tareas', $arr);
        $this->smarty->assign('nuevoEditar', $nuevoEditar);
        $template = $this->smarty->fetch('Solicitudes/Nuevo/grilla_tareas.tpl');
        echo $template;
    }

     public function subirDatos()
    {
        $session = New Zend_Session_Namespace("usuario_carpeta");
        $regularizar = array();
        $regularizar["gl_detalle_finalizado"] = $_POST["resumen"];
        $regularizar["nr_horas_utilizadas"] = $_POST["horas"];
        $regularizar["fc_fecha_creacion_reg"] = $_POST["fc_fecha_creacion_reg"];
        $regularizar["fc_fecha_termino_reg"] = $_POST["fc_fecha_termino_reg"];
        $_SESSION['regularizar'][] = $regularizar;

        $this->agregarDatos();
        $this->load->javascript('parent.Solicitudes.cargarDatosRegularizacion();');
    }

    public function RecargarGrillaTareas($id_proyecto, $id_tarea){
        $session    = New Zend_Session_Namespace("usuario_carpeta");
        $id_usuario = $session->id;
        $evento     = "Se Elimino una tarea";
        $i=1;
        $arr = array();
        if(isset($id_proyecto)){
            $id_solicitud = $id_proyecto;
            //$estado_ticket = $this->_DAOSolicitudes->getSolicitudById($id_solicitud);
            //$estado_ticket_actual = $estado_ticket->id_estado;
            //$this->procesarGuardarHistorico($id_solicitud, $id_usuario, $estado_ticket_actual, $evento);
            $select_tareas = $this->_DAOTareas->getTareasById($id_solicitud);
            $result = $this->_DAOSolicitudes->getEstadoSolicitud($id_solicitud);
            $estado = $result->cd_id_estado;
            $this->procesarGuardarHistorico($id_solicitud, $id_usuario, $estado, $evento, $id_tarea);
            if($select_tareas){
                foreach($select_tareas as $itm):
                     $arr[$i]["id_tarea"] = $itm->id_tarea;
                     $arr[$i]["gl_nombre_tarea"] = $itm->gl_nombre_tarea;
                     $arr[$i]["nr_estado"] = $itm->nr_estado;
                     $arr[$i]["cd_id_ticket"] = $itm->cd_id_ticket;
                     $i++;
                endforeach;
            }else{
                $arr = "";
            }

            $cantidad_tareas = count($select_tareas);
            $this->smarty->assign('cantidad_tareas', $cantidad_tareas);
            $nuevoEditar = 1;
        }
        $this->smarty->assign('tareas', $arr);
        $this->smarty->assign('id_solicitud', $id_proyecto);
        $this->smarty->assign('nuevoEditar', $nuevoEditar);
        $template = $this->smarty->fetch('Solicitudes/Nuevo/grilla_tareas.tpl');
        echo $template;
    }




    protected function validarArchivo($tipo)
    {
        $tipos = array(
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/msword',
            'image/png',
            'image/jpg',
            'image/jpeg'

        );

        if (in_array(trim($tipo), $tipos)) {
            return true;
        } else {
            return false;
        }
    }


    public function cargarGrillaAdjuntos($visador = null){
        $arr = array();
        $i = 0;
        if (isset($_SESSION['adjuntos']) and count($_SESSION['adjuntos']) > 0) {
            foreach ($_SESSION['adjuntos'] as $item) {
                $arr[$i]['name'] = $item['name'];
                $arr[$i]['indice'] = $i;
                $arr[$i]['fecha'] = $item['fecha'];
                $arr[$i]['usuario'] = $item['usuario'];
                $arr[$i]['usuario_id'] = $item['usuario_id'];
                $arr[$i]['visador'] = $visador;
                if (isset($_POST['visador']) and $_POST['visador'] > 0) {
                    $arr[$i]['visador'] = true;
                }
                $i++;
            }
        }

        $this->smarty->assign('adjuntos', $arr);
        $template = $this->smarty->fetch('Solicitudes/Nuevo/grilla_adjuntos.tpl');

        echo $template;
    }

    public function cargarGrillaComentarios($visador = null){
        $arr = array();
        $i = 0;
        if (isset($_SESSION['adjuntos']) and count($_SESSION['adjuntos']) > 0) {
            foreach ($_SESSION['adjuntos'] as $item) {
                $arr[$i]['name'] = $item['name'];
                $arr[$i]['indice'] = $i;
                $arr[$i]['fecha'] = $item['fecha'];
                $arr[$i]['usuario'] = $item['usuario'];
                $arr[$i]['usuario_id'] = $item['usuario_id'];
                $arr[$i]['visador'] = $visador;
                if (isset($_POST['visador']) and $_POST['visador'] > 0) {
                    $arr[$i]['visador'] = true;
                }
                $i++;
            }
        }

        $this->smarty->assign('adjuntos', $arr);
        $template = $this->smarty->fetch('Solicitudes/Nuevo/grilla_adjuntos.tpl');

        echo $template;
    }

    public function grillaRevision()
    {
        $arr = array();
        $documentos = $this->_DAODocumento->getDocumentosRevision(1);
        if ($documentos->numRows > 0) {
            $arr = $documentos->rows;
        }

        $this->smarty->assign('arrResultado', $arr);
        $this->smarty->assign('Fechas', $this->load->lib('Fechas', false));
        $template = $this->smarty->fetch('Documentos/Grillas/grilla_revision.tpl');

        echo $template;
    }


    public function borrarAdjunto()
    {
        $indice = $_POST['indice'];
        $visador = $_POST['visador'];

        $i = 0;
        unset($_SESSION['adjuntos'][$indice]);

        $arr = $_SESSION['adjuntos'];
        unset($_SESSION['adjuntos']);
        foreach ($arr as $item) {
            $_SESSION['adjuntos'][] = $item;
        }

        $this->cargarGrillaAdjuntos($visador);

        /*$this->smarty->assign('adjuntos', $_SESSION['adjuntos']);
        $template = $this->smarty->fetch('Documentos/Nuevo/grilla_adjuntos.tpl');

        echo $template;*/

    }

    public function borrarTarea()
    {
        if($_POST["idSolicitud"] == 0){
            $indice = $_POST['indice'];
            $i = 0;
            unset($_SESSION['tarea'][$indice]);   
            $arr = $_SESSION['tarea'];
            unset($_SESSION['tarea']);

            foreach ($arr as $item) {
                $_SESSION['tarea'][] = $item;
            }
            $this->cargarGrillaTareas();
        }else{
            $indice = $_POST['indice'];
            $id_proyecto = $_POST['idSolicitud'];
            $column_id = "id_tarea";
            $update_estado["nr_estado"] = 2; 

            $this->_DAOTareas->update($update_estado, $indice, $column_id);
            $this->RecargarGrillaTareas($id_proyecto, $indice);    
        }



    }


    public function verAdjunto()
    {
        $params = $this->request->getParametros();
        if (isset($_SESSION['adjuntos'])) {

            $adjunto = $_SESSION['adjuntos'][$params[0]];

            header('Content-Type: ' . $adjunto['type']);
            header('Content-Disposition: inline; filename="' . $adjunto['name'] . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            echo base64_decode($adjunto['contenido']);
            exit();


        } else {
            $sha = $params[0];
            $_DAOArchivos = $this->load->model('DAOArchivos');
            $adjunto = $_DAOArchivos->getArchivoPorSha($sha);
            $ruta = $adjunto->gl_ruta_archivo . '/' . $adjunto->gl_nombre_archivo;
            header('Content-Type: ' . $adjunto->gl_mime_archivo);
            header('Content-Disposition: inline; filename="' . $adjunto->gl_nombre_archivo . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            readfile($ruta);
            exit();
        }

    }


    public function devengarSolicitud()
    {
        $numero = $_POST['numero'];
        $solicitud = $_POST['solicitud'];

        $json = array();
        if (!is_numeric($numero)) {
            $json['estado'] = false;
            $json['mensaje'] = 'Número de folio no válido';
        } else {

            $parametros = array(
                'cd_estado_solicitud' => 4, /* estado devengado */
                'nr_folio_sigfe_solicitud' => $numero
            );
            $update = $this->_DAODocumento->updDocumento($solicitud, $parametros);
            if ($update) {


                $json['estado'] = true;
                $json['mensaje'] = 'Documento #' . $solicitud . ' Devengado';

                $this->load->lib('Constantes', false);
                $session = New Zend_Session_Namespace("usuario_carpeta");
                $arr_historial = array(
                    'fecha' => date('Y-m-d H:i:s'),
                    'usuario' => $session->id,
                    'solicitud' => $solicitud,
                    'evento' => Constantes::DOCUMENTO_DEVENGADO
                );
                $historial = $this->_DAOHistorial->insHistorial($arr_historial);
            } else {
                $json['estado'] = false;
                $json['mensaje'] = 'Problemas al devengar documento. Intente nuevamente';
            }
        }

        echo json_encode($json);
    }


    public function devolverProveedor()
    {
        $solicitud = $_POST['solicitud'];

        $json = array();
        $parametros = array(
            'cd_estado_solicitud' => 3 /* estado devolver proveedor */
        );
        $update = $this->_DAODocumento->updDocumento($solicitud, $parametros);
        if ($update) {
            $json['estado'] = true;
            $json['mensaje'] = 'Documento #' . $solicitud . ' marcado como Devuelto Proveedor';

            $this->load->lib('Constantes', false);
            $session = New Zend_Session_Namespace("usuario_carpeta");
            $arr_historial = array(
                'fecha' => date('Y-m-d H:i:s'),
                'usuario' => $session->id,
                'solicitud' => $solicitud,
                'evento' => Constantes::DOCUMENTO_DEVUELTO
            );
            $historial = $this->_DAOHistorial->insHistorial($arr_historial);
        } else {
            $json['estado'] = false;
            $json['mensaje'] = 'Problemas al cambiar estado del documento. Intente nuevamente';
        }

        echo json_encode($json);
    }


    public function filtrarDocumentos()
    {
        $this->load->lib('Constantes', false);
        $dias = $_POST['dias'];
        $condicion = $_POST['condicion'];

        if ($_SESSION['perfil'] == 1) {
            $arr = $this->_DAODocumento->getDocumentosPorEstados(Constantes::DOCUMENTO_NUEVO);
        } else {
            $session = New Zend_Session_Namespace("usuario_carpeta");
            $arr = $this->_DAODocumento->getDocumentosPorEstados(Constantes::DOCUMENTO_NUEVO, $session->id);
        }

        $documentos = array();
        if ($arr) {
            $this->load->lib('Fechas', false);
            foreach ($arr as $item) {
                $dias_habiles = Fechas::diffDias(date('Y-m-d'), $item->fc_fecha_ingreso_partes_solicitud, true) - $item->total_dias_feriados;
                if ($condicion == 1) {
                    if ($dias_habiles == $dias) {
                        $documentos[] = $item;
                    }
                } elseif ($condicion == 2) {
                    if ($dias_habiles > $dias) {
                        $documentos[] = $item;
                    }
                } elseif ($condicion == 3) {
                    if ($dias_habiles >= $dias) {
                        $documentos[] = $item;
                    }
                }

            }
        }

        $tmp_arr = array();

        foreach ($documentos as $item) {
            if ($item->cd_estado_solicitud == 0) {
                $item->dias_bandeja = Fechas::diffDias(date('Y-m-d'), $item->fc_fecha_ingreso_partes_solicitud, true) - $item->total_dias_feriados;
            } else {
                $evento = $this->_DAOHistorial->obtHistorialVisacionDocumento($item->id_solicitud, Constantes::DOCUMENTO_APROBADO, Constantes::DOCUMENTO_RECHAZADO);
                if ($evento) {
                    $item->dias_bandeja = Fechas::diffDias($evento->fc_fecha_historial, $item->fc_fecha_ingreso_partes_solicitud, true) - $item->total_dias_feriados;
                    $item->fecha_visacion = Fechas::formatearHtml($evento->fc_fecha_historial);
                } else {
                    $item->dias_bandeja = 'Sin fechas';
                    $item->fecha_visacion = 'Sin fecha';
                }
            }
            $tmp_arr[] = $item;
        }

        $this->smarty->assign('arrResultado', $tmp_arr);
        $this->smarty->assign('Fechas', $this->load->lib('Fechas', false));
        $template = $this->smarty->fetch('Documentos/Grillas/grilla_todos.tpl');

        echo $template;
    }


    public function cambiarVisador(){
        $visador = $_POST['visador'];
        $doc = $_POST['doc'];

        $data = array(
            'cd_asignacion_visador_solicitud' => $visador
            );

        $json = array();
        if($this->_DAODocumento->update($data,$doc)){
            if (ENVIROMENT == 'PROD') {
                $this->load->lib('Email', false);
                $documento = $this->_DAODocumento->getDocumentoPorId($doc);
                $datosVisador = $this->_DAOUsuarios->getUsuarioPorId($visador);
                /* correo a visador */
                $msg = '<h3>Documento asignado</h3>';
                $msg .= 'Estimado/a <strong>' . $datosVisador->nombres . ' ' . $datosVisador->apellidos . '</strong>:<br/><br/>';
                $msg .= 'Ud. tiene un documento pendiente de visación, correspondiente a:<br/>';
                $msg .= 'Factura : '.trim($documento->nr_numero_documento_solicitud).'<br/>';
                $msg .= 'Nombre Proveedor : '.trim($documento->gl_nombre_emisor_solicitud).'<br/>';
                $msg .= 'Monto : $'.number_format($documento->nr_monto_solicitud,0,',','.').'<br/>';
                $msg .= 'Correlativo : '.$doc.'<br/><br/>';
                $msg .= 'Para ver el detalle completo de esta solicitud, por favor conectarse al módulo de Facturación. Para acceder a la plataforma pinche el siguiente link:';
                $msg .= '<a href="' . HOST . '/finanzas/" target="_blank">' . HOST . '/finanzas</a>';
                $msg .= '<p>Finanzas - ' . date('d/m/Y') . '</p>';

                $destinatario = trim($datosVisador->email);
                $remitente = 'Minsal - Finanzas';
                $nombre_remitente = 'Minsal - Finanzas';
                $asunto = 'Finanzas - Documento Asignado';
                $mensaje = $msg;
                Email::sendEmail($destinatario, $remitente, $nombre_remitente, $asunto, $mensaje);

            }
            $json['estado'] = true;
        }else{
            $json['estado'] = false;
            $json['mensaje'] = 'Problemas al cambiar visador. Intente nuevamente';
        }

        echo json_encode($json);
    }


    public function grillaTodos(){
        $this->load->lib('Fechas', false);
        $this->load->lib('Constantes', false);

        if ($_SESSION['perfil'] == 1) {
            $arr = $this->_DAODocumento->getDocumentosAsignados();
        } else {
            $session = New Zend_Session_Namespace("usuario_carpeta");
            $arr = $this->_DAODocumento->getDocumentosAsignados($session->id);
        }

        $tmp_arr = array();

        foreach ($arr as $item) {
            if ($item->cd_estado_solicitud == 0) {
                $item->dias_bandeja = Fechas::diffDias(date('Y-m-d'), $item->fc_fecha_ingreso_partes_solicitud, true) - $item->total_dias_feriados;
            } else {
                $evento = $this->_DAOHistorial->obtHistorialVisacionDocumento($item->id_solicitud, Constantes::DOCUMENTO_APROBADO, Constantes::DOCUMENTO_RECHAZADO);
                if ($evento) {
                    $item->dias_bandeja = Fechas::diffDias($evento->fc_fecha_historial, $item->fc_fecha_ingreso_partes_solicitud, true) - $item->total_dias_feriados;
                    $item->fecha_visacion = Fechas::formatearHtml($evento->fc_fecha_historial);
                } else {
                    $item->dias_bandeja = 'Sin fechas';
                    $item->fecha_visacion = 'Sin fecha';
                }
            }
            $tmp_arr[] = $item;
        }

        $this->smarty->assign('arrResultado', $tmp_arr);
        $this->smarty->assign('Fechas', $this->load->lib('Fechas', false));
        $template = $this->smarty->fetch('Documentos/Grillas/grilla_todos.tpl');

        echo $template;
    }

    public function verSolicitudes(){
        $this->load->lib('Fechas', false);
        $this->load->lib('Constantes', false);

        if ($_SESSION['perfil'] == 1) {
            $arr = $this->_DAOSolicitudes->getSolicitudesAsignadas();
        } else {
            $session = New Zend_Session_Namespace("usuario_carpeta");
            $arr = $this->_DAOSolicitudes->getSolicitudesAsignadas($session->id);
        }

       
        $template = $this->smarty->fetch('Solicitudes/Mantenedores/index.tpl');

        echo $template;
    }

    public function historicoPorUser(){
        $this->_display('avanzados/historicoUser.tpl');
    }

     public function historicoUsuario(){
        $DAOSolicitudes = $this->_DAOSolicitudes;
        $session = New Zend_Session_Namespace("usuario_carpeta");
        $historicoUser= $DAOSolicitudes->ticketPorUsuario($session->id);
        //print_r($historicoUser);die();
        $this->smarty->assign('historicoUser',$historicoUser);
        $this->_display('avanzados/historicoUser.tpl');
        
    }

    /*public function ActualizarDetalleFinalizado(){
        $id_ticket = $_POST["idTicket"];
        //$fecha_entrega = explode('/', $_POST["fechaEntrega"]);
        $comentario =  $_POST["comentario"];
        $data = array();
        $data["id_ticket"] = $id_ticket;
        //$data["fc_plazo"] = $fecha_entrega[2] . '-' . $fecha_entrega[1] . '-' . $fecha_entrega[0];
        if($comentario != 1){
            $data["gl_detalle_finalizado"] = $comentario;
        }
        $this->_DAOSolicitudes->update($data, $id_ticket, $this->campo_id);
    }*/

    /*public function ActualizarHoras(){
        $id_ticket = $_POST["idTicket"];
        //$fecha_entrega = explode('/', $_POST["fechaEntrega"]);
        $estimadas =  $_POST["estimadas"];
        $utilizadas =  $_POST["utilizadas"];
        $data = array();
        $data["id_ticket"] = $id_ticket;
        //$data["fc_plazo"] = $fecha_entrega[2] . '-' . $fecha_entrega[1] . '-' . $fecha_entrega[0];
        //if($comentario != 1){
            $data["nr_horas_estimadas"] = $estimadas;
            $data["nr_horas_utilizadas"] = $utilizadas;
        //}
        $this->_DAOSolicitudes->update($data, $id_ticket, $this->campo_id);
    } */
    
    public function guardarNuevoComentario(){
        $session = New Zend_Session_Namespace("usuario_carpeta");
        $data = array();
        parse_str($_POST['data'], $data);
        $this->load->lib('Constantes', false);
        $json = array();
        $datos = $data;
        //$datos['fc_fecha_creacion'] =date('Y-m-d H:i:s');
        $datos['id_usuario'] =$session->id;
        $insertar = $this->_DAOSolicitudes->insComentario($datos);
               
        if ($insertar) {
                //$id_solicitud = $insertar;
                $json['estado'] = true;
                $json['mensaje'] = 'Comentario ingresado correctamente';
        }
        echo json_encode($json);
    }
}
