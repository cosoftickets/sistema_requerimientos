
<legend>
        Mantenedor de sistemas
        <button class="btn btn-xs btn-success" onClick="location.href='{$base_url}/MantenedorSistemas/nuevo'" style="float: right">
            <i class="fa fa-plus"></i> Nuevo sistema
        </button>
</legend>
<ol class="breadcrumb">
    <li><i class="fa fa-angle-right"></i> <strong>Mantenedores</strong></li>
    <li class="active">Sistemas</li>
</ol>
<div id="div_tabla">
    {grilla}
</div>
