
<div class="row c-box">
    <div class="col-md-3">
    {if $tipo_reporte == 1}
    <h5 class="box-title">Filtro Mensual</h5>
    <select class="form-control select2" id="filtroFecha" onchange="Solicitudes.recargarTablaReportes()">
        <option value="01" {if $mes_actual == "01"} selected{/if}>Enero</option>
        <option value="02" {if $mes_actual == "02"} selected{/if}>Febrero</option>
        <option value="03" {if $mes_actual == "03"} selected{/if}>Marzo</option>
        <option value="04" {if $mes_actual == "04"} selected{/if}>Abril</option>
        <option value="05" {if $mes_actual == "05"} selected{/if}>Mayo</option>
        <option value="06" {if $mes_actual == "06"} selected{/if}>Junio</option>
        <option value="07" {if $mes_actual == "07"} selected{/if}>Julio</option>
        <option value="08" {if $mes_actual == "08"} selected{/if}>Agosto</option>
        <option value="09" {if $mes_actual == "09"} selected{/if}>Septiembre</option>
        <option value="10" {if $mes_actual == "10"} selected{/if}>Octubre</option>
        <option value="11" {if $mes_actual == "11"} selected{/if}>Noviembre</option>
        <option value="12" {if $mes_actual == "12"} selected{/if}>Diciembre</option>
    </select>
    {/if}
    {if $tipo_reporte == 2}
    <h5 class="box-title">Reporte Anual</h5>
    {/if}
    </div>
</div>
<div id="tablaReportes">
    {include file="Solicitudes/Grillas/tablaReportes.tpl"}
</div>
      