<table id="tablaPrincipal" class="table table-hover table-striped table-bordered  table-middle dataTable no-footer">
    <thead>
    <tr role="row">
        <th align="center">ID</th>
        <th align="center">Proyecto</th>
        <th align="center">Asunto</th>
        <th align="center">Tareas</th>
        <th align="center">Fecha Límite</th>
        <th align="center">Estado</th>
        <th align="center">Prioridad</th>
        <th align="center">Días restantes</th>
        <th width="1px" align="center">Acciones</th>
    </tr>
    </thead>
    <tbody>
{$todasSolicitudes|@print_r}
    {foreach $todasSolicitudes as $itm}
        <tr>
            <td nowrap align="center"> {$itm->id_ticket} </td>
            <td class="text-center">{$itm->nombre_proyecto}</td>
            <td class="text-center">{$itm->asunto|truncate:60:"...":true}</td>
            <td nowrap width="100px" align="center">{if $itm->sumaEstados == null}0{else} {$itm->sumaEstados} {/if}/{$itm->cantidadTareas}</td>
            <td class="text-center">{$itm->fecha_entrega}</td>
            <td class="text-center">{$itm->desc_estado}</td>
            <td nowrap class="text-center">{$itm->desc_prioridad}</td>
            <td align="center" nowrap>
                <div style="height: 25px; width: 50px;    margin-left: 5px;">
                    <div style="width:20px;float:left">{Fechas::diffDiasTickets({$fechaHoy},{$itm->fecha_entrega})}</div>
                    <div style="float:left; margin-top:-1px">
                        <img src ="../static/images/estado_entrega_ticket/{Fechas::diffDiasAlerta({$fechaHoy},{$itm->fecha_entrega})}" width="12px" height="12px"/>
                    </div>
                    </div>
            </td>
            <td align="center">
            <a href='{$base_url}/Solicitudes/Asignados_editar/{$itm->id_solicitud}' class="btn btn-sm btn-flat btn-success" title="Ver Detalle"><i class="fa fa-edit"></i>Revisar</a>
                <!--<button data-toggle="tooltip" type="button" class="btn btn-sm btn-success btn-flat" title="Ver Detalle" onClick="xModal.open('{$smarty.const.BASE_URI}/Solicitudes/revisarSolicitud/{$itm->id_solicitud}','Revisar Solicitud',85);">
                    <i class="fa fa-edit"></i>&nbsp;&nbsp;Revisar
                </button>-->
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>        