<link href="{$static}template/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css"/>

<form role="form" class="form-horizontal">
<section class="content-header">
    <h1>Nuevo Registro
        <small>Ingresar nueva solicitud</small>
    </h1>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Formulario de Registro</h3>
        </div>
        <div class="box-body">
		{include file="Solicitudes/Grillas/tareas.tpl"}


</div>
</div>
</section>
</form>


