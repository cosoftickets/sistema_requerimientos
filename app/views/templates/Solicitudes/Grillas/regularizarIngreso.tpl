<head>{include file="layout/css.tpl"}{include file="layout/css.tpl"}
<link href="{$static}template/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
</head>

<section class="content c-content">
    <div class="box box-success c-box-success">
        <div class="box-body">
            <form class="form" role="form" enctype="multipart/form-data" method="post"
                  action="{$smarty.const.BASE_URI}/Solicitudes/subirDatos">
                <div class="form-group">
                    <label class="control-label">Añadir Datos</label>
                    <div class="">
                        <div class="form-group">
							<label for="exampleInputEmail1" class="col-xs-2  col-md-2 control-label"> Resumen de requerimiento</label>
							<div class="col-lg-7">
			                        <textarea class="form-control form-control-textarea" name="resumen" id="resumen" rows="3" ></textarea>
							</div>	
						</div>				
						<div class="form-group c-form-group">
							<label for="exampleInputEmail1" class="col-xs-2  col-md-2 control-label"> Horas Utilizadas</label>
							<div class="col-lg-2">
								<input type="text" name="horas" id="horas" value="" class="form-control col-xs-2" /> 
							</div>
						</div> 

                          <div class="form-group">
                            <label for="exampleInputEmail1" class="col-lg-2 control-label">Fecha Creación</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" readonly
                                           style="border-radius: 0" id="fc_fecha_creacion_reg"
                                           name="fc_fecha_creacion_reg"
                                           placeholder="">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="exampleInputEmail1" class="col-lg-2 control-label">Fecha Término</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" readonly
                                           style="border-radius: 0" id="fc_fecha_termino_reg"
                                           name="fc_fecha_termino_reg"
                                           placeholder="">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="input-group-btn c-input-group-btn">
                        <button  type="button" class="btn btn-success btn-flat c-btn-flat" onclick="this.form.submit();">
                                Guardar Solicitud
                        </button>
                        <button type="button" class="btn btn-primary btn-flat" onclick="parent.xModal.close();">
                                Cerrar
                        </button>
                        <input  type="hidden" name="tipo_guardado" id="tipo_guardado" value="1"></input>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {if isset($mensaje)}
        {$mensaje}
    {/if}
</section>
<script type="text/javascript">
        $('.datepicker').datepicker();
</script>
