<table id="tablaPrincipal" class="table table-hover table-striped table-bordered  table-middle dataTable no-footer">
    <thead>
    <tr role="row">
        <th align="center">ID</th>
        <th align="center">Proyecto</th>
        <th align="center">Asunto</th>
        <th align="center">Tareas</th>
        <th align="center">Fecha Límite</th>
        <th align="center">Estado</th>
        <th align="center">Prioridad</th>
        <th align="center">Días restantes</th>
        <th width="1px" align="center">Acciones</th>
    </tr>
    </thead>
    <tbody>

    {foreach $arrResultado as $itm}
        <tr>
            <td nowrap align="center"> {$itm->id_solicitud} </td>
            <td class="text-center">{$itm->nombre_proyecto}</td>
            <td class="text-center">{$itm->asunto|truncate:60:"...":true}</td>
            <td nowrap width="100px" align="center">{if $itm->sumaEstados == null}0{else} {$itm->sumaEstados} {/if}/{$itm->cantidadTareas}</td>
            <td class="text-center">{$itm->fecha_entrega}</td>
            <td class="text-center">{$itm->desc_estado}</td>
            <td nowrap class="text-center">{$itm->desc_prioridad}</td>
            <td align="center" nowrap>
                <div class="c-fecha-1">
                    <div class="c-fecha-2">
                    {if $itm->id_estado != 7}
                        {Fechas::diffDiasTickets({$fechaHoy},{$itm->fecha_entrega})}
                            </div>
                            <div class="c-cont-fechas">
                            <img src ="../../static/images/estado_entrega_ticket/{Fechas::diffDiasAlerta({$fechaHoy},{$itm->fecha_entrega})}" width="12px" height="12px"/>
                            </div>
                        {else}
                            </div>
                            <div class="c-cont-fechas">
                                <span class="fa fa-check success c-success"></span>
                            </div>
                        {/if}   
                </div>
            </td>
            <td align="center">
                <a href='{$base_url}/Solicitudes/Asignados_editar/{$itm->id_solicitud}' class="btn btn-sm btn-flat btn-success" data-toggle="tooltip" title="Revisar"><i class="fa fa-edit"></i></a>

                <a href='javascript:void(0)' onClick="xModal.open('{$smarty.const.BASE_URI}/DetalleSolicitud/detalleSolicitud/{$itm->id_solicitud}','Bitácora ticket número {$itm->id_solicitud}: {$itm->nombre}',85);" class="btn btn-sm btn-flat btn-primary" data-toggle="tooltip" title="Bitácora"><i class="fa fa-search"></i></a>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>        