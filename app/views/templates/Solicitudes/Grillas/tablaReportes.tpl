<table id="tablaPrincipal" class="table table-hover table-striped table-bordered  table-middle dataTable no-footer">
    <thead>
    <tr role="row">
        <th align="center">ID</th>
        <th align="center">Proyecto</th>
        <th align="center">Responsable</th>   
        <th align="center">Horas Utilizadas</th>
        <th align="center">Detalle</th>
        <th align="center">Estado</th>
        <th align="center">Accion</th>
        
    </tr>
    </thead>
    <tbody>
    {foreach $arrReportes as $itm}
        <tr>
            <td nowrap align="center">{$itm->id_solicitud}</td>
             <td align="center">{$itm->nombre_proyecto}</td>
             <td align="center">{$itm->nombres} {$itm->apellidos}</td>
            <td class="text-center">{$itm->nr_horas_utilizadas}</td>
            <td >{$itm->gl_detalle_finalizado}</td>   
            <td class="text-center" >{$itm->desc_estado}</td> 
            <td width="40px" class="text-center" >
                <a href='{$base_url}/Solicitudes/Asignados_editar/{$itm->id_solicitud}' class="btn " data-toggle="tooltip" title="Revisar">
                <i class="fa fa-edit"></i></a>
            </td>   
        </tr>
    {/foreach}
    </tbody>
</table>  