<link href="{$static}template/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css"/>
<head>
    {include file="layout/css.tpl"}
    <link href="{$smarty.const.STATIC_FILES}template/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="{$smarty.const.STATIC_FILES}template/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
</head>


<div class="col-xs-9 " id="columna_izq">
    <div class="panel panel-success " style="border-color:#3C8D88;">
        <div class="panel-heading" style="background-color:#3c8dbc;color:#FFF">
            <h3 class="panel-title"><i class="fa fa-tag fa-fw"></i> Datos</h3>
        </div>
		<div class="panel-body">
			<div class="form-group">
				<label for="exampleInputEmail1" class="col-xs-2  col-md-2 control-label"> Asignado a:</label>
				<div class="col-lg-4">
					<img src="{$base_url}/../static/images/member.png" width="17" style="float:left">
                    <label style="margin-left:3px">{$solicitud->nombre} {$solicitud->apellido}</label>
				</div>	
			</div>						
			<div class="form-group">
				<label for="exampleInputEmail1" class="col-xs-2  col-md-2 control-label"> Asunto</label>
				<div class="col-lg-4">
					<input type="text" name="id_proyecto" id="id_proyecto" value="{$solicitud->asunto}" readonly class="form-control col-xs-6" /> 
				</div>	
			</div>	
			<div class="form-group">
				<label for="exampleInputEmail1" class="col-xs-2  col-md-2 control-label"> Proyecto</label>
				<div class="col-lg-4">
					<input type="text" name="id_proyecto" id="id_proyecto" value="{$solicitud->nombre_proyecto}" readonly class="form-control col-xs-6" /> 
				</div>	
			</div>				

			<div class="form-group">
				<label for="exampleInputEmail1" class="col-xs-2  col-md-2 control-label"> Resumen de requerimiento:</label>
				<div class="col-lg-7">
                        <textarea style="resize:none" class="form-control form-control-textarea" name="gl_comentario" id="gl_comentario" rows="3"  onblur="Solicitudes.ActualizarCamposSolicitudes(this.form,this,{$solicitud->id_solicitud},{$perfil})" 
                                  {if $perfil != 1} disabled {/if}>{$solicitud->comentario}</textarea>
				</div>	
			</div>				            
			
			<div class="form-group">
				<label for="exampleInputEmail1" class="col-xs-2  col-md-2 control-label"> Resumen de lo realizado:</label>
				<div class="col-lg-7">
                        <textarea style="resize:none" class="form-control form-control-textarea" name="gl_detalle_finalizado" id="gl_detalle_finalizado" rows="3" 
                        onblur="Solicitudes.ActualizarCamposSolicitudes(this.form,this,{$solicitud->id_solicitud},{$perfil})">{$solicitud->gl_detalle_finalizado}</textarea>
				</div>	
			</div>				            			

			<div class="form-group">
				<label for="exampleInputEmail1" class="col-xs-1  col-md-1 control-label"> <img src="{$base_url}/../static/images/check.png" width="17"> Tareas:</label>
				<div class="col-lg-10">
                <div style="float:left;font-size:12px;margin-top:1px" id="porcentaje"></div>
                <div style="background-color:#EEE; margin-top: 6px; margin-right:3px;border-radius: 4px; float: left; width: 100%;" id="barraProcesCheck" name="barraProcesCheck" >
                </div>
                <br>
                <div style="margin-top: 40px;" id="lista_tareas" name="lista_tareas">
                    {if $tareas_asignadas}
						{assign var="indTarea" value="1"}
                        {foreach $tareas_asignadas as $tareas_asign}
                            <div class=" clearfix" style="margin-bottom: 10px;border:3px">
								<div class="checkbox" style="float:left">
									<label>  
										<input type="checkbox" id="check" name="tareas[]" value="" onclick="Solicitudes.barraProcessCheck(this.form,this,{$tareas_asign->id_tarea},{$cantidad_tareas})" {if $tareas_asign->nr_estado == 1 } checked {else}{/if}/>
                                        <b>TAREA N° {$indTarea}:</b>
                                        {$tareas_asign->gl_nombre_tarea}
									</label>
								</div>
                                <img onclick="Solicitudes.borrarTarea({$tareas_asign->id_tarea},{$id_solicitud})" 
                                        src="{$base_url}/../static/images/borrar_barra.png" 
                                        style="width:15px; margin-left:10px; margin-top:10px; cursor:pointer">
                            </div>
						{assign var="indTarea" value=$indTarea + 1}							
                        {/foreach}
                    {/if}
                </div>
				</div>	
			</div>				            						         
        </div>
    </div>
    <input type="hidden" name="id_solicitud" id="id_solicitud" value="{$solicitud->id_solicitud}"/>

</div>
<div class="col-xs-3 ">
	{if $perfil !=1}
    	<div class="box box-success">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-gear"></i> Acciones</h3>
            </div>
            <div class="box-body">
    			<div class="form-group">
    				<div class="col-lg-12">
    				    <button  id="btn_finalizado" type="button" class="btn btn-success btn-flat" onClick="xModal.open('{$smarty.const.BASE_URI}/Solicitudes/finalizarTicket/{$solicitud->id_solicitud}','Ticket Finalizado',40,'ticket',true,40);" disabled>
    				    	<i class="fa fa-plus-circle">&nbsp Finalizar Ticket</i>
    				    </button>
    				</div>
    			</div>
    		</div>
    	</div>
	{/if}

    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-plus fa-fw"></i> Añadir</h3>
        </div>
        <div class="box-body">
			<button type="button" class="btn btn-success c-button" onClick="xModal.open('{$smarty.const.BASE_URI}/Solicitudes/agregarTarea','Añadir Tarea',40,'tarea',true,160);">
				<i class="fa fa-plus-circle">&nbsp; Tareas</i>
			</button>
			<button type="button" class="btn btn-success c-button" onClick="xModal.open('{$smarty.const.BASE_URI}/Solicitudes/adjuntarArchivo','Adjuntar archivo',50,'tarea',true,200);">
				<i class="fa fa-upload">&nbsp; Adjuntos</i>
			</button>
			<button type="button" class="btn btn-success c-button" onClick="xModal.open('{$smarty.const.BASE_URI}/Solicitudes/agregarTarea','Añadir Tarea',40,'tarea',true,160);" disabled>
				<i class="fa fa-plus-circle">&nbsp; Usuarios</i>
			</button>
		</div>
	</div>
    <div class="box box-success">
        <div class="box-header" >
            <h3 class="box-title"><i class="fa fa-calendar"></i> Fechas</h3>
        </div>
        <div class="box-body c-box-body" >
                <label  class="control-label required">Fecha Creación</label>
                <div class="input-group">
                    <input type="text" class="form-control datepicker" readonly style="border-radius: 0" id="fecha_creacion" name="fecha_creacion" value="{$fc_fecha_creacion}" disabled>
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i></span>
                </div>
                    <span class="help-block hidden"></span>     
        </div>

        <div class="box-body" >
                <label  class="control-label required">Fecha Termino(estimado)</label>
                <div class="input-group">
                    <input type="text" class="form-control datepicker" readonly style="border-radius: 0" id="fecha_entrega" name="fecha_entrega" value="{$fc_fecha_entrega}" onblur="Solicitudes.ActualizarCamposSolicitudes(this.form,this,{$solicitud->id_solicitud},{$perfil})">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i></span>
                </div>
                    <span class="help-block hidden"></span>      
        </div>
        
        {if $perfil ==1}
            <div class="box-body" >
                    <label  class="control-label required">Fecha Termino(confirmado)</label>
                    <div class="input-group">
                        <input type="text" class="form-control datepicker" readonly style="border-radius: 0" id="fecha_termino" name="fecha_termino" value="{$fc_fecha_termino}" onblur="Solicitudes.ActualizarCamposSolicitudes(this.form,this,{$solicitud->id_solicitud},{$perfil})">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i></span>
                    </div>
                        <span class="help-block hidden"></span>      
            </div>
        {/if}
    </div>

    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-clock-o fa-fw"></i> Control de horas</h3>
        </div>
        <div class="box-body">
			<div class="form-group">
				<div class="col-lg-12">
					Horas estimadas
				</div>					
				<div class="col-lg-12">
					<input type="text" name="horas_estimadas" id="horas_estimadas" class="form-control" onkeypress="return validar_texto(event)" value="{$solicitud->nr_horas_estimadas}" onblur="Solicitudes.ActualizarCamposSolicitudes(this.form,this,{$solicitud->id_solicitud},{$perfil})" >
				</div>	
			</div>			
			<div class="form-group">
				<div class="col-lg-12">
					Horas Utilizadas
				</div>					
				<div class="col-lg-12">
					<input type="text" name="horas_utilizadas" id="horas_utilizadas" class="form-control" onkeypress="return validar_texto(event)" value="{$solicitud->nr_horas_utilizadas}" onblur="Solicitudes.ActualizarCamposSolicitudes(this.form,this,{$solicitud->id_solicitud},{$perfil})">
				</div>	
			</div>					
        </div>
    </div>
</div>
<div class="col-xs-12">
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Documentos adjuntos</h3>
        </div>
        <div class="box-body">
			<div class="table-responsive" id="lista_adjuntos"></div>
		</div>	
	</div>	
</div>	

<script type="text/javascript">
	function validar_texto(e){
    	tecla = (document.all) ? e.keyCode : e.which;
	    //Tecla de retroceso para borrar, siempre la permite
    	if (tecla==8){
        	return true;
    	}
        // Patron de entrada, en este caso solo acepta numeros
    	patron =/[0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
	}
</script>