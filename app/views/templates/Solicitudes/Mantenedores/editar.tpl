<section class="content-header">
    <h1>Tickets <small>Administración</small></h1>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Editar <small>(*) Campos requeridos</small></h3>
        </div>
        <div class="box-body">
            {include file="Solicitudes/Mantenedores/form.tpl"}
        </div>
    </div>
</section>

