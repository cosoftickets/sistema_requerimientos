<link href="{$smarty.const.STATIC_FILES}template/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="{$smarty.const.STATIC_FILES}template/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Mantenedor de Solicitudes</h3>
            <button class="btn btn-sm btn-success btn-flat pull-right"
                    onClick="location.href='{$base_url}/Solicitudes/Nuevo'">
                <i class="fa fa-plus"></i> Nueva Solicitud
            </button>
        </div>
        <div class="box-body">
            <div id="div_tabla" class="table-responsive small">
                <table id="tablaPrincipal" class="table table-hover table-striped table-bordered  table-middle dataTable no-footer">
                    <thead>
                        <tr role="row">
                            <th align="center">ID</th>
                            <th align="center">Proyecto</th>
                            <th align="center">Asunto</th>
                            <th align="center">Tareas</th>
                            <th align="center">Fecha Límite</th>
                            <th align="center">Estado</th>
                            <th align="center">Prioridad</th>
                            <th align="center">Días restantes</th>
                            <th width="1px" align="center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                        {foreach $todasSolicitudes as $itm}
                        <tr>
                            <td nowrap align="center"> {$itm->id_solicitud} </td>
                            <td class="text-center">{$itm->nombre_proyecto}</td>
                            <td class="text-center">{$itm->asunto|truncate:60:"...":true}</td>
                            <td nowrap width="100px" align="center">{if $itm->sumaEstados == null}0{else} {$itm->sumaEstados} {/if}/{$itm->cantidadTareas}</td>
                            <td class="text-center">{$itm->fecha_entrega}</td>
                            <td class="text-center">{$itm->desc_estado}</td>
                            <td nowrap class="text-center">{$itm->desc_prioridad}</td>
                            <td align="center" nowrap>
                                <div style="height: 25px; width: 50px;    margin-left: 5px;">
                                    <div style="width:20px;float:left">
                                        {if $itm->id_estado != 7}
                                            {Fechas::diffDiasTickets({$fechaHoy},{$itm->fecha_entrega})}
                                            </div>
                                            <div style="float:left; margin-top:-1px">
                                                <img src ="../../static/images/estado_entrega_ticket/{Fechas::diffDiasAlerta({$fechaHoy},{$itm->fecha_entrega})}" width="12px" height="12px"/>
                                            </div>
                                        {else}
                                            </div>
                                            <div style="float:left; margin-top:-1px">
                                                <span class="fa fa-check success" style="margin-left:15px;margin-top:10px"></span>
                                            </div>
                                        {/if}   
                                </div>
                            </td>
                            <td align="center">
                                <a href='{$base_url}/Solicitudes/Asignados_editar/{$itm->id_solicitud}' class="btn btn-sm btn-flat btn-success" data-toggle="tooltip" title="Revisar"><i class="fa fa-edit"></i></a>
                                 <a href='javascript:void(0)' onClick="xModal.open('{$smarty.const.BASE_URI}/DetalleSolicitud/detalleSolicitud/{$itm->id_solicitud}','Bitácora ticket número {$itm->id_solicitud}: {$itm->nombre} ',85);" data-toggle="tooltip" title="Bitácora" class="btn btn-sm btn-flat btn-primary" title="Ver Detalle"><i class="fa fa-search"></i></a>
                            </td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>               
            </div>
        </div>
    </div>
</section>

