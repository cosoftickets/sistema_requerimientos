<head>{include file="layout/css.tpl"}</head>

<section class="content" style="min-height:100px">
    <div class="box box-success" style="margin:0px">
        <div class="box-body">
            <form class="form" role="form" enctype="multipart/form-data" method="post"
                  action="{$smarty.const.BASE_URI}/Solicitudes/mostrarTarea">
                <div class="form-group">
                    <label class="control-label">Añadir Tarea</label>
                    <div class="">
                        <div class="input-group" style="width: 100%;">
                            <input type="text" style="width:100%" name="tarea" id="tarea" class="form-control"/> 
                        </div>
                        <div class="input-group-btn" style="float:left; margin-left:2px; margin-top:5px">
                            <button type="button" class="btn btn-success btn-flat" onclick="this.form.submit();">
                                    Agregar
                            </button>
                            <button type="button" class="btn btn-primary btn-flat" onclick="parent.xModal.close();">
                                    Cerrar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {if isset($mensaje)}
        {$mensaje}
    {/if}
</section>
