<input type="hidden" name="cantidad_tareas" id="cantidad_tareas" value="{$cantidad_tareas}"/>
    {if $tareas}
        {assign var="indTarea" value="1"}
        {foreach $tareas as $item}  
            <div class=" clearfix" style="margin-bottom: 10px;border:3px">
                <div class="checkbox" style="float:left">
                    <label>
                        <input type="checkbox" id="check" name="tareas[]" value="" 
                               onclick="Solicitudes.barraProcessCheck(this.form,this,{$item['id_tarea']},{$cantidad_tareas})" {if $item["nr_estado"] == 1 } checked {else}{/if} 
                               {if $nuevoEditar == 0 } disabled {else}{/if}/>
                                    <b>TAREA N° {$indTarea}: </b>{$item["gl_nombre_tarea"]}
                    </label>
                </div>
                <img onclick="Solicitudes.borrarTarea({$item['id_tarea']},{$id_solicitud})" src="{$base_url}/../static/images/borrar_barra.png" 
                     style="width:15px; margin-left:10px;  margin-top:10px; cursor:pointer">
            </div>
        {assign var="indTarea" value=$indTarea + 1}
        {/foreach}
    {/if}

