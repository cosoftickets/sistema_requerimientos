<link href="{$smarty.const.STATIC_FILES}template/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="{$smarty.const.STATIC_FILES}template/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />


<section class="content-header">
    <h1>Tickets <small>Estadística</small></h1>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Rendimiento Desarrolladores</h3>      
        </div>
        <div class="box-body">
        	<div ><h5 style="text-decoration:underline">Equipo COSOF</h5></div>
		        	<div class="col-md-6">
			        	{foreach $data as $datos}
			        		<div style="border:1px solid; border-radius:4px; margin-bottom:10px; padding:5px; width:60%;" class="panel panel-primary">
			        			<i class="fa fa-pie-chart" style="cursor:pointer; float:right" onclick="Solicitudes.CargarGrafico({$datos->id})" 
			        			   data-toggle="tooltip" title="Cargar Gráfico"></i>
				        		<label>Desarrollador: </label>{$datos->nombres} {$datos->apellidos}<br>
				        		<label>Proyectos Asignados: </label> {$datos->cant_proyectos}<br>
				        		<label>Proyectos Terminados: </label> {$datos->cant_terminados}<br>
				        		<label>Proyectos Terminados Atrasados: </label> {$datos->pro_atrasados}<br>
				        		<label>Proyectos Terminados a Tiempo: </label> {$datos->pro_atiempo}<br>
				        		
			        		</div>	
			        	{/foreach}
	        	</div>
        		<div class="col-md-6" id="contenedor_grafico"></div>
		</div>
    </div>
</section>
