<link href="{$smarty.const.STATIC_FILES}template/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="{$smarty.const.STATIC_FILES}template/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<section class="content-header">
    <h1>LISTADO
        <small>solicitudes</small>
    </h1>

</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">SOLICITUDES</h3>
           
        </div>
        <div class="box-body">
            <div id="div_tabla" class="table-responsive small">
                {grilla} 
                <button type="button" class="btn btn-success btn-xs btn-flat" onClick="xModal.open('{$smarty.const.BASE_URI}/DetalleSolicitud/detalleSolicitud/79','Bitácora ',100,'tarea',true,1060);">
                    <i class="fa fa-plus-circle">&nbsp Añadir tarea</i>
                </button>            
            </div>
        </div>
    </div>
</section>
<div id="historicoFiltrado"></div>
