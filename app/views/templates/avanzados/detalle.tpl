<head>
    {include file="layout/css.tpl"}
    <link href="{$smarty.const.STATIC_FILES}template/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="{$smarty.const.STATIC_FILES}template/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
</head>
<section class="content-header">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-info-circle fa-fw"></i> Información del ticket </h3>
        </div>
        <br>
        <div class="row">
            {foreach $parameters as $p}
                <div class="col-md-3">
                    <div class="form-group">
                        {if $p->TotalTareas>0}
                            <label for="exampleInputPassword1" class=" control-label">Tareas asociadas:</label> <a href="#tareas_asociadas"> {$p->TotalTareas}</a>
                        {else}
                            <label for="exampleInputPassword1" class=" control-label">Tareas asociadas:</label> {$p->TotalTareas}

                        {/if}                          
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="control-label">Fecha de término:</label>
                            {$p->fc_fecha_termino}
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="control-label">Estado:</label>
                            {$p->gl_descripcion}
                    </div>
                </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {if $p->TotalTareas>0}
                        {assign var="porcentaje" value=($p->TareasPendientes/$p->TotalTareas)*100}
                        <label for="exampleInputPassword1" class="control-label">Tareas pendientes:</label> <a href="#tareas_asociadas">{$p->TareasPendientes} ({$porcentaje} % pendiente)</a>
                    {else}
                        <label for="exampleInputPassword1" class="control-label">Tareas pendientes:</label>
                            {$p->TareasPendientes}
                    {/if}
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleInputPassword1" class="control-label">Plazo:</label>
                        {$p->fc_plazo}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1" class="control-label">Comentario:</label>
                        {$p->gl_comentario}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {if $p->TotalTareas>0}
                        {assign var="porcentaje" value=($p->TareasTerminadas/$p->TotalTareas)*100}
                        <label for="exampleInputPassword1" class="control-label">Tareas terminadas:</label>
                            <a href="#tareas_asociadas"> {$p->TareasTerminadas} ({$porcentaje} % completado)</a>
                    {else}
                        <label for="exampleInputPassword1" class="control-label">Tareas terminadas:</label>
                           {$p->TareasTerminadas} 
                    {/if}
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleInputPassword1" class="control-label">Días de atraso: </label>
                        {if $p->cd_id_estado==7 }
                            El ticket ha sido finalizado
                        {else}
                            {if $p->DiasAtraso<0}
                                <font color="red">Tiene {$p->DiasAtraso*-1} días atrasados</font>
                            {/if}
                            {if $p->DiasAtraso > 0}
                                Aún quedan {$p->DiasAtraso} días
                            {/if}
                            {if $p->DiasAtraso==0}
                                El plazo vence hoy
                            {/if}
                        {/if}
                </div>
            </div>

             <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1" class="control-label">Detalle finalizado: </label>
                        {if $p->gl_detalle_finalizado==''}
                            (No se ha ingresado detalle)
                        {else}
                            {$p->gl_detalle_finalizado} 
                        {/if}
                </div>
            </div>
        </div> 

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                        <label for="exampleInputPassword1" class="control-label">Horas estimadas:</label>
                            {$p->nr_horas_estimadas} 
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="exampleInputPassword1" class="control-label">Horas utilizadas: </label>
                        {$p->nr_horas_utilizadas} 
                </div>
            </div>

             <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1" class="control-label">Diferencia días: </label>
                        {if $p->cd_id_estado==7}
                            {if $p->nr_fecha_diferencia<0}
                                El ticket fue finalizado con {$p->nr_fecha_diferencia*-1} días de atraso
                            {/if}
                            {if $p->nr_fecha_diferencia>0}
                                El ticket fue finalizado {$p->nr_fecha_diferencia} días antes del plazo
                            {/if}

                            {if $p->nr_fecha_diferencia==0}
                                El ticket fue finalizado al mismo día de plazo
                            {/if}
                        {else}
                            Este ticket aún no está finalizado
                        {/if}
                </div>
            </div>
        </div>              
            {/foreach}
    </div>
</section>

<hr>
<section class="content-header">
    <h3 class="panel-title"><i class="fa fa-list fa-fw"></i> Historial del ticket </h3>
    <div class="box box-primary">
        <div class="box-header">
           
        </div>
        <div class="box-body">
            <div id="div_tabla" class="table-responsive small"> 
                <table class="table table-hover table-striped table-bordered  dataTable no-footer">
                    <thead>
                        <th>Evento</th>
                        <th>Solicitud / Tarea</th>
                        <th>Descripcion</th>
                        <th>Estado</th>
                        <th>Prioridad</th>
                        <th>Realizada por</th>
                        <th>Asignada a</th>
                        <th>Fecha evento</th>
                    </thead>
                    <tbody>
                        {foreach $detalle as $item}
                            <tr>
                                <!--nombre de evento-->
                                {if $item->cd_id_tarea==null}
                                    <td>{$item->gl_evento_historial}: ({$item->nombre})</td>
                                {else}
                                    <td>{$item->gl_evento_historial}: ({$item->gl_nombre_tarea})</td>
                                {/if}
                                
                                <!--tipo de solicitud-->
                                {if $item->cd_id_tarea==null}
                                    <td>Solicitud</td>
                                {else}
                                    <td>Tarea</td>
                                {/if}
                                
                                <!--descripcion de solicitud-->
                                {if $item->cd_id_tarea==null}
                                    <td>{$item->gl_comentario}</td>
                                {else}
                                    <td>{$item->gl_descripcion_tarea}</td>
                                {/if}

                                <!--estado de solicitud-->
                                {if $item->cd_id_tarea==null}
                                    <td>{$item->estado}</td>
                                {else}
                                    {if $item->nr_estado==0}
                                        <td><font color="red">Tarea pendiente</font></td>
                                    {/if}
                                    {if $item->nr_estado==2}
                                        <td>Tarea eliminada</td>
                                    {/if}
                                    {if $item->nr_estado==1}
                                        <td>Tarea terminada</td>
                                    {/if}
                                {/if}

                                <!--prioridad-->
                                {if $item->cd_id_tarea==null}
                                    <td>{$item->prioridad}</td>
                                {else}
                                    <td></td>
                                {/if}
                                <td>{$item->usuario}</td>
                                <td>{$item->asignado}</td>
                                <td>{$item->fecha}</td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>            
            </div>
        </div>
    </div>
</section>
<hr>

<!--agregar un comentario desde la bitacora-->
<section class="content-header" >
    <h3 class="panel-title"><span class="fa fa-plus" onclick="habilitar()"> Agregar comentario</span></h3>
    <form id="form" name="form-inline" enctype="application/x-www-form-urlencoded" action="" method="post">
        <input value="{$p->id_ticket}" id="ticket" name="ticket" type="hidden">
        <div class="box box-primary" id="seccionComentario" style="display:none">
            <div class="box-header">
               <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                                <label for="exampleInputPassword1" class="control-label">Nuevo Comentario</label>
                                    <textarea class="form-control" name="nuevo_comentario" id="nuevo_comentario" style="resize:none" rows="10"></textarea> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <div id="div_tabla" class="table-responsive small"> 
                            <button type="button" class="form-control btn btn-primary" onclick="Comentario.guardarComentario(this.form,this)">Guardar comentario</button>     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

<hr>
<div id="div_grilla_comentario">
    {include file='avanzados/grillaComentario.tpl'}
</div>



<section class="content-header" id="tareas_asociadas">
    <h3 class="panel-title"><i class="fa fa-tasks fa-fw"></i> Tareas asociadas</h3>
    <div class="box box-primary">
       
        <div class="box-body">
            <div id="div_tabla" class="table-responsive small"> 
                <table class="table table-hover table-striped table-bordered  dataTable no-footer">
                    <thead>
                        <th>Nombre tarea</th>
                        <th>Estado</th>
                    </thead>
                    <tbody>
                        {foreach $tareas as $t}
                            <tr>
                                <td>{$t->gl_nombre_tarea}</td>
                                {if $t->nr_estado==0}
                                    <td><font color="red">Tarea pendiente</font></td>
                                {else}
                                    <td>Tarea terminada</td>
                                {/if}
                            </tr>
                        {/foreach}
                    </tbody>
                </table>            
            </div>
        </div>
    </div>
</section>
<hr>

<!--agregar un adjunto (! version) desde la bitacora-->
<section class="content-header" >
    <h3 class="panel-title"><span class="fa fa-plus" onclick="habilitarAdjunto()"> Agregar adjunto</span></h3>
    <form id="form" role="form" name="form-inline" enctype="multipart/form-data" action="{$smarty.const.BASE_URI}/Solicitudes/guardarNuevoAdjunto" method="post">
        <input value="{$p->id_ticket}" id="ticket" name="ticket" type="hidden">
        <div class="box box-primary" id="seccionAdjunto" style="display:none">
            <div class="top-spaced">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="box-header">
                                Adjuntar nuevo archivo
                                <input type="file" name="archivo" id="archivo" class="form-control"/>
                                <input type="hidden" value="{$p->id_ticket}" name="id_ticket" id="id_ticket">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="box-header">
                                Comentario (opcional)
                                <textarea style="resize:none" class="form-control" id="comentario_adjunto" name="comentario_adjunto"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="box-header">
                            <button class="btn btn-primary form-control" type="button" onclick="this.form.submit()">Guardar adjunto</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
<hr>
<div id="div_grilla_adjuntos">
    {include file='avanzados/grillaAdjuntos.tpl'}
</div>

<script type="text/javascript">
    function habilitar(){
        $('#seccionComentario').toggle()
    }

    function habilitarAdjunto(){
        $('#seccionAdjunto').toggle()
    }
</script>