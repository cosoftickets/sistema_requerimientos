<link href="{$smarty.const.STATIC_FILES}template/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="{$smarty.const.STATIC_FILES}template/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<section class="content-header">
    <h1>Histórico
        <small>solicitudes</small>
    </h1>

</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-body">
            <div id="div_tabla" class="table-responsive small">
                <!--{grilla}  -->
                <section class="content-header">
                    <div class="">
                        <div class="box-body">
                            <div id="div_tabla" class="table-responsive small"> 
                                <table class="table table-hover table-striped table-bordered  dataTable no-footer" border=1>
                                    <thead>
                                        <th>ID</th>
                                        <th>Evento</th>
                                        <th>Solicitud / Tarea</th>
                                        <th>Descripcion</th>
                                        <th>Estado</th>
                                        <th>Prioridad</th>
                                        <th>Realizada por</th>
                                        <th>Asignado a</th>
                                        <th>Fecha</th>
                                    </thead>
                                    
                                    
                                    <tbody>
                                    {foreach $historico as $hist}
                                        <tr>
                                            <td>{$hist->id_historial}</td>

                                            <!--nombre de evento-->
                                            {if $hist->cd_id_tarea==null}
                                                <td>{$hist->gl_evento_historial}: ({$hist->nombre})</td>
                                            {else}
                                                <td>{$hist->gl_evento_historial}: ({$hist->gl_nombre_tarea})</td>
                                            {/if}

                                            <!--tipo de solicitud-->
                                            {if $hist->cd_id_tarea==null}
                                                <td>Solicitud</td>
                                            {else}
                                                <td>Tarea</td>
                                            {/if}

                                            <!--descripcion de solicitud-->
                                            {if $hist->cd_id_tarea==null}
                                                <td>{$hist->gl_comentario}</td>
                                            {else}
                                                <td>{$hist->gl_descripcion_tarea}</td>
                                            {/if}

                                            <!--estado de solicitud-->
                                            {if $hist->cd_id_tarea==null}
                                                <td>{$hist->estado}</td>
                                            {else}
                                                {if $hist->nr_estado==0}
                                                    <td><font color="red">Tarea pendiente</font></td>
                                                {/if}
                                                {if $hist->nr_estado==1}
                                                    <td>Tarea terminada</td>
                                                {/if}
                                                {if $hist->nr_estado==2}
                                                    <td>Tarea eliminada</td>
                                                {/if}
                                            {/if}

                                            <!--prioridad-->
                                            {if $hist->cd_id_tarea==null}
                                                <td>{$hist->prioridad}</td>
                                            {else}
                                                <td></td>
                                            {/if}

                                            <td>{$hist->usuario}</td>
                                            <td>{$hist->asignado}</td>
                                            <td>{$hist->fecha}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                   
                                </table>            
                            </div>
                        </div>
                    </div>
                </section>  
            </div>
        </div>
    </div>
</section>

