<footer>
    <!-- jQuery 2.1.4 -->
    <script src="{$static}template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{$static}template/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{$static}template/dist/js/app.min.js"></script>

    {*<script type="text/javascript" src="{$static}js/plugins/jquery.livequery.min.js"></script>*}
    {*<script type="text/javascript" src="{$static}js/plugins/DataTables-1.10.5/media/js/jquery.dataTables.min.js"></script>*}
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
    <!--<script type="text/javascript" src="{$static}js/plugins/select2.full.min.js"></script>-->
    <script type="text/javascript" src="{$static}js/plugins/jquery.mask.js"></script>
	<script type="text/javascript" src="{$static}js/plugins/jquery.colorbox.js"></script>
    <script type="text/javascript" src="{$static}js/plugins/xmodal.js"></script>
    <script type="text/javascript" src="{$static}js/base.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    {js}
    {$js}
</footer>