									<div class="row">
										<legend>&nbsp;&nbsp;Datos de la instalaci&oacute;n</legend>
									</div>

									<div class="row">
										<div class="form-group top-spaced">
												<label for="selectCDAType" class="col-xs-3 control-label clabel">Rut de instalaci&oacute;n</label>
												<div class="col-xs-7">
													{$arrSipresa->ins_c_rut}
												</div>
										</div>
									</div>
									<div class="row">							
										<div class="form-group top-spaced">
												<label for="selectCDAType" class="col-xs-3 control-label clabel">Raz&oacute;n social</label>
												<div class="col-xs-9">
													{$arrSipresa->ins_c_razon_social}
												</div>
										</div>
									</div>									
									
									<div class="row">															
										<div class="form-group top-spaced">
												<label for="selectCDAType" class="col-xs-3 control-label clabel">Direcci&oacute;n</label>
												<div class="col-xs-7">
													{$arrSipresa->ins_c_nombre_direccion}
												</div>
										</div>
									</div>
									<div class="row">															
										<div class="form-group top-spaced">
												<label for="selectCDAType" class="col-xs-3 control-label clabel">N&uacute;mero</label>
												<div class="col-xs-7">
													{$arrSipresa->ins_c_numero_direccion}
												</div>
										</div>
									</div>								
									
									<div class="row">															
										<div class="form-group top-spaced">
												<label for="selectCDAType" class="col-xs-3 control-label clabel">Direcci&oacute;n resto</label>
												<div class="col-xs-7">
													{$arrSipresa->ins_c_resto_direccion} 
												</div>
										</div>
									</div>

									<div class="row">															
										<div class="form-group top-spaced">
												<label for="selectCDAType" class="col-xs-3 control-label clabel">Comuna</label>
												<div class="col-xs-7">
													{$arrSipresa->com_c_nombre}
												</div>
										</div>
									</div>									
									<div class="row">															
										<div class="form-group top-spaced">
												<label for="selectCDAType" class="col-xs-3 control-label clabel">Región</label>
												<div class="col-xs-7">
													{$arrSipresa->reg_ia_id}
												</div>
										</div>
									</div>																		
									




									<div class="row">
										<legend>&nbsp;&nbsp;Datos del representante</legend>
									</div>					
									<div class="row">															
										<div class="form-group top-spaced">
												<label for="selectCDAType" class="col-xs-3 control-label clabel">Rut</label>
												<div class="col-xs-7">
													{$arrSipresa->ins_c_rut_representante}
												</div>
										</div>								
									</div>							
									<div class="row">															
										<div class="form-group top-spaced">
												<label for="selectCDAType" class="col-xs-3 control-label clabel">Nombre</label>
												<div class="col-xs-7">
													{$arrSipresa->ins_c_nombre_representante} 
												</div>
										</div>
									</div>			