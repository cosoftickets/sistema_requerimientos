<section class="content-header">
    <h1>Usuarios <small>Administración</small></h1>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Editar usuario <small>(*) Campos requeridos</small></h3>
        </div>
        <div class="box-body">
            {include file="mantenedor_usuarios/form.tpl"}
        </div>
    </div>
</section>

