<section class="content-header">
    <h1>Usuarios <small>Administración</small></h1>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Nuevo usuario <small>(*) Campos requeridos</small></h3>
            <button class="btn btn-xs btn-link pull-right" onClick="location.href='{$base_url}/MantenedorUsuarios/index'" style="float: right">
                <i class="fa fa-backward"></i> Ir Atras
            </button>
        </div>
        <div class="box-body">
            {include file="mantenedor_usuarios/form.tpl"}
        </div>
    </div>
</section>


 
