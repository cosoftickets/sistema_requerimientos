<section class="content-header">
    <h1>Estados <small>Administración</small></h1>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Editar estado <small>(*) Campos requeridos</small></h3>
        </div>
        <div class="box-body">
            {include file="mantenedor_avanzados/formEstados.tpl"}
        </div>
    </div>
</section>

