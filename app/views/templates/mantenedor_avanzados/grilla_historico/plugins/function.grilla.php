<?php

require_once(APP_PATH . "libs/Helpers/View/Grid.php");

/**
 * 
 * @param array $params
 * @param Smarty $smarty
 * @return string html
 */
function smarty_function_grilla($params, &$smarty) {
    $grid = New View_Grid("lista_historico");
    $grid->setModel("DAOHistorico");
    $grid->addHelperPath(__DIR__, "");
    $grid->setQuery("queryBusquedaHistorico", array("id_historial" => "id_historial",
                                           "cd_id_solicitud" => "cd_id_solicitud",
                                           "cd_id_estado"=>"cd_id_estado",
                                           "cd_id_usuario"=>"cd_id_usuario",
                                           "fc_fecha_movimiento"=>"fc_fecha_movimiento",
                                           "cd_id_proyecto"=>"cd_id_proyecto",
                                           "gl_comentario"=>"gl_comentario",
                                           "gl_evento_historial"=>"gl_evento_historial"), 
                                   array());
    //coloca los nombres de la cabecera de la tabla y permite ordenar los registros
    $grid->setColumns(array(
           
            array("column_name" => "Asunto",
                "column_table" => array("cd_id_solicitud"),
                "column_type" => "method",
                "sortable" => array("active" => true,
                                    "sortable_field" => "t.cd_id_solicitud"),
                "width" => "20%"),
           
            array("column_name" => "Proyecto",
                "column_table" => array("cd_id_proyecto"),
                "column_type" => "method",
                "sortable" => array("active" => true,
                                    "sortable_field" => "t.cd_id_proyecto"),
                "width" => "20%"),
           array("column_name" => "Estado ticket",
                "column_table" => array("cd_id_estado"),
                "column_type" => "method",
                "sortable" => array("active" => true,
                                    "sortable_field" => "t.cd_id_estado"),
                "width" => "20%"),
           array("column_name" => "Asignado a",
                "column_table" => array("cd_id_usuario"),
                "column_type" => "method",
                "sortable" => array("active" => true,
                                    "sortable_field" => "t.cd_id_usuario"),
                "width" => "20%"),

           array("column_name" => "Evento",
                "column_table" => array("gl_evento_historial"),
                "column_type" => "method",
                "sortable" => array("active" => true,
                                    "sortable_field" => "t.gl_evento_historial"),
                "width" => "20%"),

           array("column_name" => "Fecha movimiento",
                "column_table" => array("fc_fecha_movimiento"),
                "column_type" => "method",
                "sortable" => array("active" => true,
                                    "sortable_field" => "t.fc_fecha_movimiento"),
                "width" => "20%")
        ));
    
    return $grid->getGrid();
}