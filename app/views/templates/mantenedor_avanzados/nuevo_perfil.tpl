<section class="content-header">
    <h1>Perfiles <small>Administración</small></h1>
</section>

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Nuevo perfil <small>(*) Campos requeridos</small></h3>
           
        </div>
        <div class="box-body">
            {include file="mantenedor_avanzados/form_perfil.tpl"}
        </div>
    </div>
</section>