<?php

require_once(APP_PATH . "libs/Helpers/View/Grid.php");

/**
 * 
 * @param array $params
 * @param Smarty $smarty
 * @return string html
 */
function smarty_function_grilla($params, &$smarty) {
    $grid = New View_Grid("lista_detalle_solicitudes");
    $grid->setModel("DAOSolicitudes");
    $grid->addHelperPath(__DIR__, "");
    $grid->setQuery("queryBusquedaDetalleSolicitudes", array("id_ticket" => "id_ticket",
                                           "nombre" => "nombre",
                                           "gl_comentario"=>"gl_comentario"), 
                                   array());
    //coloca los nombres de la cabecera de la tabla y permite ordenar los registros
    $grid->setColumns(array(
            array("column_name" => "Id",
                "column_table" => array("id_ticket"),
                "column_type" => "method",
                "sortable" => array("active" => true,
                                    "sortable_field" => "t.id_ticket"),
                "width" => "5%"),
            array("column_name" => "Descripción",
                "column_table" => array("nombre"),
                "column_type" => "method",
                "sortable" => array("active" => true,
                                    "sortable_field" => "t.nombre"),
                "width" => "20%"),
           array("column_name" => "Fecha creación",
                "column_table" => array("gl_comentario"),
                "column_type" => "method",
                "sortable" => array("active" => true,
                                    "sortable_field" => "t.gl_comentario"),
                "width" => "20%")
        ));
    
    return $grid->getGrid();
}


