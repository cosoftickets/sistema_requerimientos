<!DOCTYPE html>
<html lang="es">
<head>
    {include file="layout/css.tpl"}
</head>
    <body class="body-login">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-banner text-left">
                        <h1 class="text-center"><img src="{$static}images/logo.png" width="150" height="50" /><br/>&nbsp;&nbsp;&nbsp;TICKETS</h1>
                    </div>
                    <div class="portlet portlet-green">
                        <div class="portlet-body">
                            <div id="form-success" class="hidden">
                                <div id="mensaje-modificacion" class="alert alert-success">
                                    
                                </div>
                                <button onclick="location.href='{$base_url}'" class="btn btn-lg btn-primary btn-block" type="button">Continuar <i class="fa fa-forward"></i></button>

                            </div>
                            <div id="form-contenedor">
                                <form id="form" name="form" role="form" method="post">
                                    <fieldset>
                                        <div class="alert alert-warning">
                                            Para recuperar su contraseña, se enviará un email al correo asociado a su número de RUT.
                                        </div>

                                        <div class="form-group">
                                            <input class="form-control rut" name="rut" id="rut" type="text" placeholder="Escriba su RUT"/>
                                        </div>

                                        <div id="form-error" class="alert alert-danger hidden">
                                            <i class="fa fa-warning fa-2x"></i> &nbsp; No se encontro un usuario para el rut ingresado.
                                        </div>

                                        <br>
                                        <button id="enviar" class="btn btn-lg btn-primary btn-block" type="button">Enviar email</button>
                                        <a href="javascript:history.back();" class="pull-right">Volver</a>

                                    </fieldset>
                                    <br>                                
                                </form>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {include file="layout/js.tpl"}
    </body>
</html>