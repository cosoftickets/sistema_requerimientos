<!DOCTYPE html>
<html lang="es">
<head>
    {include file="layout/css.tpl"}

</head>
    <body class="body-login">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-banner text-left">
                        <h1 class="text-center"><img src="{$static}images/logo.png" width="150" height="50" /><br/>&nbsp;&nbsp;&nbsp;TICKETS</h1>
                    </div>
                    <div class="portlet portlet-green">
                        <div class="portlet-body">
                            
                            <form role="form" action="{$base_url}/Login/procesar" method="post">
                                <fieldset>

                                    <div class="form-group">
                                        <input class="form-control rut" name="rut" id="rut" type="text" placeholder="Escriba su RUT"/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="password" id="password" type="password" placeholder="Password"/>
                                    </div>
                                    <div class="form-group hide">
                                        <input type="checkbox" name="recordar" id="recordar" value="1"/> Recordarme en este equipo
                                    </div>
                                    <div id="form-error" class="alert alert-danger {$hidden}">
                                        <i class="fa fa-warning fa-2x"></i> &nbsp; Los datos ingresados no son válidos.
                                    </div>
                                    <br>
                                    <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
           
                                </fieldset>
                                <br>

                                <p class="small">
                                    <a href="{$base_url}/Login/recuperar_password">¿Olvidaste tu password?</a>
                                </p>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {include file="layout/js.tpl"}
    </body>
</html>
