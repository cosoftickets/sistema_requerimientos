<div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">{$arrDatos->nombres} {$arrDatos->apellidos}</h3>
            </div>
            <div class="panel-body">
              <div class="row">
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs" role="tablist">
					  <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Informaci&oacute;n general</a></li>
					  <li role="presentation" class=""><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Ámbitos</a></li>
					  <li role="presentation" class=""><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Sumarios</a></li>
					  <li role="presentation" class=""><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Eventos</a></li>
					  <li role="presentation" class=""><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Documentos adjuntos</a></li>
					</ul>
					<div id="myTabContent" class="tab-content">
					  <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
						<div class="panel panel-default" id="panel_mapa" >
							<div class="panel-body">
							<div class="row">
								<div class="form-group top-spaced">
										<label for="selectCDAType" class="col-xs-2 control-label clabel">Rut de instalaci&oacute;n</label>
										<div class="col-xs-3">
											14.498.574-5
										</div>
								</div>
							</div>
							<div class="row">							
								<div class="form-group top-spaced">
										<label for="selectCDAType" class="col-xs-2 control-label clabel">Razón social</label>
										<div class="col-xs-3">
											Domingo Cortez Jara
										</div>
								</div>
							</div>
							<div class="row">															
								<div class="form-group top-spaced">
										<label for="selectCDAType" class="col-xs-2 control-label clabel">Dirección</label>
										<div class="col-xs-3">
											Isla picton 280
										</div>
								</div>
							</div>
							<div class="row">															
								<div class="form-group top-spaced">
										<label for="selectCDAType" class="col-xs-2 control-label clabel">Datos de contacto</label>
										<div class="col-xs-3">
											8787818181 / domingo.cortez@gmail.com
										</div>
								</div>								
							</div>

							</div>
						</div>	
					  </div>
					  <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
					  <legend>Sumarios sanitarios</legend>
								<table class="table table-hover table-striped" data-toggle="table" data-show-columns="true">
		<thead>
			<tr>
				<th>
					Expediente
				</th>			
				<th>
					Resolución
				</th>				
				<th>
					Rut
				</th>
				<th>
					Razón social
				</th>
				<th>
					Dirección
				</th>
				<th>
					Responsable
				</th>				
				<th>
					Ubicación carpeta
				</th>
				<th>
					Ver detalle
				</th>				
			</tr>
		</thead>	
		<tbody>
			<tr>
				<td>
					EXP1200
				</td>
				<td>
					5S100
				</td>				
				<td>
					14.498.574-5
				</td>
				<td>
					COSOF EIRL
				</td>
				<td>
				Errazuriz #123456
				</td>
				<th>
				Gloria alfaro
				</td>				
				<td>
				OFICINA VALPARAISO
				</td>
				<td>
				<button onClick="detalle(1)">ver</button>
				</td>				
			</tr>
			<tr>
				<td>
					EXP1200
				</td>
				<td>
					5S100
				</td>				
				<td>
					14.498.574-5
				</td>
				<td>
					COSOF EIRL
				</td>
				<td>
				Errazuriz #123456
				</td>
				<th>
				Gloria alfaro
				</td>				
				<td>
				OFICINA VALPARAISO
				</td>
				<td>
				<button onClick="detalle(1)">ver</button>
				</td>				
			</tr>						<tr>
				<td>
					EXP1200
				</td>
				<td>
					5S100
				</td>				
				<td>
					14.498.574-5
				</td>
				<td>
					COSOF EIRL
				</td>
				<td>
				Errazuriz #123456
				</td>
				<th>
				Gloria alfaro
				</td>				
				<td>
				OFICINA VALPARAISO
				</td>
				<td>
				<button onClick="detalle(1)">ver</button>
				</td>				
			</tr>						<tr>
				<td>
					EXP1200
				</td>
				<td>
					5S100
				</td>				
				<td>
					14.498.574-5
				</td>
				<td>
					COSOF EIRL
				</td>
				<td>
				Errazuriz #123456
				</td>
				<th>
				Gloria alfaro
				</td>				
				<td>
				OFICINA VALPARAISO
				</td>
				<td>
				<button onClick="detalle(1)">ver</button>
				</td>				
			</tr>						<tr>
				<td>
					EXP1200
				</td>
				<td>
					5S100
				</td>				
				<td>
					14.498.574-5
				</td>
				<td>
					COSOF EIRL
				</td>
				<td>
				Errazuriz #123456
				</td>
				<th>
				Gloria alfaro
				</td>				
				<td>
				OFICINA VALPARAISO
				</td>
				<td>
				<button onClick="detalle(1)">ver</button>
				</td>				
			</tr>						<tr>
				<td>
					EXP1200
				</td>
				<td>
					5S100
				</td>				
				<td>
					14.498.574-5
				</td>
				<td>
					COSOF EIRL
				</td>
				<td>
				Errazuriz #123456
				</td>
				<th>
				Gloria alfaro
				</td>				
				<td>
				OFICINA VALPARAISO
				</td>
				<td>
				<button onClick="detalle(1)">ver</button>
				</td>				
			</tr>						<tr>
				<td>
					EXP1200
				</td>
				<td>
					5S100
				</td>				
				<td>
					14.498.574-5
				</td>
				<td>
					COSOF EIRL
				</td>
				<td>
				Errazuriz #123456
				</td>
				<th>
				Gloria alfaro
				</td>				
				<td>
				OFICINA VALPARAISO
				</td>
				<td>
				<button onClick="detalle(1)">ver</button>
				</td>				
			</tr>						<tr>
				<td>
					EXP1200
				</td>
				<td>
					5S100
				</td>				
				<td>
					14.498.574-5
				</td>
				<td>
					COSOF EIRL
				</td>
				<td>
				Errazuriz #123456
				</td>
				<th>
				Gloria alfaro
				</td>				
				<td>
				OFICINA VALPARAISO
				</td>
				<td>
				<button onClick="detalle(1)">ver</button>
				</td>				
			</tr>						<tr>
				<td>
					EXP1200
				</td>
				<td>
					5S100
				</td>				
				<td>
					14.498.574-5
				</td>
				<td>
					COSOF EIRL
				</td>
				<td>
				Errazuriz #123456
				</td>
				<th>
				Gloria alfaro
				</td>				
				<td>
				OFICINA VALPARAISO
				</td>
				<td>
				<button onClick="detalle(1)">ver</button>
				</td>				
			</tr>						<tr>
				<td>
					EXP1200
				</td>
				<td>
					5S100
				</td>				
				<td>
					14.498.574-5
				</td>
				<td>
					COSOF EIRL
				</td>
				<td>
				Errazuriz #123456
				</td>
				<th>
				Gloria alfaro
				</td>				
				<td>
				OFICINA VALPARAISO
				</td>
				<td>
				<button onClick="detalle(1)">ver</button>
				</td>				
			</tr>						<tr>
				<td>
					EXP1200
				</td>
				<td>
					5S100
				</td>				
				<td>
					14.498.574-5
				</td>
				<td>
					COSOF EIRL
				</td>
				<td>
				Errazuriz #123456
				</td>
				<th>
				Gloria alfaro
				</td>				
				<td>
				OFICINA VALPARAISO
				</td>
				<td>
				<button onClick="detalle(1)">ver</button>
				</td>				
			</tr>			
		</tbody>			
	</table>
					  </div>
					  <div role="tabpanel" class="tab-pane fade" id="dropdown1" aria-labelledby="dropdown1-tab">
						<p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
					  </div>
					  <div role="tabpanel" class="tab-pane fade" id="dropdown2" aria-labelledby="dropdown2-tab">
						<p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
					  </div>
					</div>
				  </div>
              </div>
            </div>
            
          </div>