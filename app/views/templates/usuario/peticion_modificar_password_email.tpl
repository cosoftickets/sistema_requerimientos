Estimado {$nombre}, <br><br>

Para continuar el proceso de modificación de contraseña, debe seguir el siguiente enlace:<br><br>

<a href="{$url}">{$url}</a><br><br>

Si usted no inicio este proceso, ignore este mensaje.<br><br>

Saludos,<br><br>

SISTEMA MIDAS