<section class="content-header">
    <h1>Estadísticas
        <small>Dashboard</small>
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Documentos por estados</h3>
                </div>
                <div class="box-body">
                    <div class="col-xs-7">
                        <canvas id="graficoA"></canvas>
                    </div>
                    <div class="col-xs-5">
                        <div id="graficoA_legend"></div>
                        
                       
                    </div>
                </div>
            </div>


        </div>
        <div class="col-xs-12 col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Solicitudes atrasadas</h3>
                </div>
                <div class="box-body">
                    <div class="col-xs-7">
                        <canvas id="graficoB"></canvas>
                    </div>
                    <div class="col-xs-5">
                        <div id="graficoB_legend"></div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>

<!--
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Documentos Por Estados</h3>
                </div>
                <div class="box-body">
                    <div class="col-xs-7">
                        <canvas id="graficoD"></canvas>
                    </div>
                    <div class="col-xs-5">
                        <div id="graficoD_legend"></div>
                        
                       
                    </div>
                </div>
            </div>


        </div>
        <div class="col-xs-12 col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Solicitudes atrasadas</h3>
                </div>
                <div class="box-body">
                    <div class="col-xs-7">
                        <canvas id="graficoC"></canvas>
                    </div>
                    <div class="col-xs-5">
                        <div id="graficoC_legend"></div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>-->