<?php

require_once(APP_PATH . 'models/DAOComuna.php');

function smarty_function_selectComuna($params, &$smarty) {
    $DAORegion = New DAOComuna();
    $lista = $DAORegion->listar("name");
    
    $rel = "";
    if(!empty($params["rel"])){
        $rel = "rel=\"" . $params["rel"] . "\"";
    }
    
    $data_rel = "";
    if(!empty($params["data_rel"])){
        $data_rel = "data-rel=\"" . $params["data_rel"] . "\"";
    }
    
    $html = "<select style=\"width:100%\" name=\"" . $params["nombre"] . "\" id=\"" . $params["nombre"] . "\" " . $data_rel . " " . $rel . " class=\"" . $params["class"] . "\">";
    $html .= "<option value=\"\">Seleccione una comuna</option>";
    foreach($lista as $key => $itm){
        $selected = "";
        if($params["default"] == $itm->id){
            $selected = "selected";
        }
        $html .= "<option value=\"".$itm->id."\" " . $selected . ">"
                . $itm->name
               . "</option>";
    }
    $html .= "</select>";
    
    return $html;
}

