<li>
    <a href="{$base_url}/Home/dashboard">
        <i class="fa fa-home"></i> <span>Inicio</span></a>
</li>
{if $smarty.session.perfil == 2}
    <li>
        <a href="{$base_url}/Solicitudes/Asignados">
            <i class="fa fa-arrow-circle-down"></i> <span>Tickets asignados</span>
            {if $cantBadge !=0}<span style="margin-left:5px; margin-top:-1px" class="badge"> {$cantBadge} </span>{/if}</a>
    </li>

    <li>
        <a href="{$base_url}/Solicitudes/historicoUsuario">
            <i class="fa fa-th"></i> <span>Histórico tickets asignados</span></a>
    </li>
{/if}
{if $smarty.session.perfil == 1}<!--administrador-->
    <li>
        <!--<a href="{$base_url}/Documento/Nuevo">-->
        <a href="{$base_url}/Solicitudes/Nuevo">
            <i class="fa fa-plus-circle"></i> <span>Nueva Solicitud</span></a>
    </li>
    <li >
            <a href="{$base_url}/MantenedorSolicitudes/">
                <i class="fa fa-credit-card"></i> <span>Administrar Solicitudes</span></a>
    </li>
    <li class="treeview">
            <a href="">
                <i class="fa fa-th"></i> <span>Reporte</span></a>
            <ul class="treeview-menu">
               <!--<li>
                    <a href="{$base_url}/Reportes/diario">
                        <i class="fa fa-calendar"></i> <span>Diario</span></a>
                </li>
                    
                 <li>
                    <a href="{$base_url}/Reportes/semanal">
                        <i class="fa fa-calendar"></i> <span>Semanal</span></a>
                </li>-->
                 <li>
                    <a href="{$base_url}/Reportes/mensual">
                        <i class="fa fa-calendar"></i> <span>Mensual</span></a>
                </li>
                <li>
                    <a href="{$base_url}/Reportes/anual">
                        <i class="fa fa-calendar"></i> <span>Anual</span></a>
                </li>
            </ul>
        </li>
    <li>
        <a href="{$base_url}/Historico/historico">
            <i class="fa fa-th"></i> <span>Histórico Solicitudes</span></a>
    </li>
    <li>
        <a href="{$base_url}/Estadisticas/">
            <i class="fa fa-bar-chart"></i> <span>Estadisticas Desarrolladores</span></a>
    </li>
    <li>
        <a href="{$base_url}/MantenedorUsuarios">
            <i class="fa fa-users"></i> <span>Administrar usuarios</span></a>
    </li>

        <li class="treeview">
            <a href="">
                <i class="fa fa-gears"></i> <span>Opciones Avanzadas</span></a>
            <ul class="treeview-menu">
                <li>
                    <a href="{$base_url}/Estados">
                        <i class="fa fa-plus-circle"></i> <span>Estados</span></a>
                </li>
                    
                 <li>
                    <a href="{$base_url}/Prioridad">
                        <i class="fa fa-plus-circle"></i> <span>Prioridades</span></a>
                </li>
                 <li>
                    <a href="{$base_url}/Perfiles/NuevoPerfil">
                        <i class="fa fa-plus-circle"></i> <span>Perfiles</span></a>
                </li>
                <li>
                    <a href="{$base_url}/Proyecto">
                        <i class="fa fa-plus-circle"></i> <span>Proyectos</span></a>
                </li>
            </ul>
        </li>
{/if}
