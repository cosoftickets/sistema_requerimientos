<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-list"></i> Mantenedores <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
        <li>
            <a href="{$base_url}/MantenedorUsuarios/index">
                <i class="fa fa-users"></i> Usuarios
            </a>
        </li>
      <li>
          <a href="{$base_url}/MantenedorSistemas/index">
              <i class="fa fa-briefcase"></i> Sistemas
          </a>
      </li>

    </ul>
</li>
