<?php

require_once(APP_PATH . "models/DAOUsuarios.php");
require_once(APP_PATH . "models/DAOSolicitudes.php");

/**
 * 
 * @param array $params
 * @param Smarty $smarty
 * @return string html
 */
function smarty_function_htmlMenu($params, &$smarty) {
    
    $DAOUsuario = New DAOUsuarios();
    $DAOSolicitudes = New DAOSolicitudes();
    //$_DAOSolicitudes = $this->load->model("DAOSolicitudes");
    $sesion = New Zend_Session_Namespace("usuario_carpeta");
    $id_usuario = $sesion->id;
    $badge = $DAOSolicitudes->getBadge($id_usuario);
    $badgeProcesado =$badge->badge;
    $smarty->assign("cantBadge",$badgeProcesado);
    $usuario = $DAOUsuario->getById($id_usuario);
    if(!is_null($usuario)){
        return $smarty->fetch("plugins/view/menu.tpl");
    }
}

