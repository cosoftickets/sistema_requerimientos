<?php

require_once (APP_PATH . 'models/DAOUsuarios.php');
require_once (__DIR__ . '/../Validar.php');

/**
 * Validar formulario de ingreso de usuario
 */
Class Validar_Usuario extends Validar{
    
    /**
     * Si es correcto o no
     * @return boolean
     */
    public function isValid(){
        $this->_validarVacio("nombre");
        $this->_validarVacio("apellido");
        
        $this->_validarPassword();
        
        $this->_validarVacio("rut");
        $this->_validarVacio("perfil");
        
        $this->_validarVacio("email");
        $this->_validarEmail("email");
        
        $this->_validarRut("rut", 2);
        
        //$this->_validarVacio("region");
        
        $this->_verSiYaExiste();
        
        return $this->getCorrecto();
    }
    
    /**
     * Valida el password
     */
    protected function _validarPassword(){
        $DAOUsuario = New DAOUsuarios();
        $id = $this->_parametros["id"];
        $usuario = $DAOUsuario->getById($id);
        if(is_null($usuario)){
            $this->_validarVacio("password");
        }
    }
    
    /**
     * Ve que no se repita el rut
     */
    protected function _verSiYaExiste(){
        $DAOUsuario = New DAOUsuarios();
        
        $id = $this->_parametros["id"];
        $usuario = $DAOUsuario->getById($id);
        if(!is_null($usuario)){
            $not_in = array($id);
        } else {
            $not_in = array();
        }
        
        $existe = $DAOUsuario->getByRut($this->_parametros["rut"], $not_in);
        if(!is_null($existe)){
            $this->_correcto = false;
            $this->_error["rut"] = "El rut ya pertenece a un usuario registrado";
            return false;
        } else {
            return true;
        }
    }
}

