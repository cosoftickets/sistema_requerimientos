<?php

class Constantes{

	const PERFIL_SUPERADMIN = 1;
    const PERFIL_VISADOR = 2;
    const PERFIL_AUDITORIA = 3;


    /* eventos historia */
    const DOCUMENTO_NUEVO = 'Se crea documento';
    const DOCUMENTO_APROBADO = 'Documento es Aprobado';
    const DOCUMENTO_RECHAZADO = 'Documento es Rechazado';
    const DOCUMENTO_DEVUELTO = 'Documento es Devuelto a Proveedor';
    const DOCUMENTO_DEVENGADO = 'Documento es Devengado';
}